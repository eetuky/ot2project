package gui;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import java.util.Locale;

import org.json.simple.parser.ParseException;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.TemporaryFolder;

import domain.Analysis;
import domain.Customer;
import util.Stance;

public class TestEllipsisPanel {
	private EllipsisPanel ellipsisPanel;
	private Customer customer;
	private Analysis analysis1;
	private Analysis analysis2;
	private Analysis analysis3;
	private ArrayList<Double[]> coordinates;
	
	@Rule
	public final TemporaryFolder testFolder = new TemporaryFolder();
	
	@Before
	public void setUp() throws IOException, ParseException {
		Locale.setDefault(new Locale("fi", "FI"));
		//Creating test ellipsisPanel, customer and analysis
		ellipsisPanel = new EllipsisPanel();
		customer = new Customer(testFolder.newFolder("testCommentPanel"), "Test Customer");
		coordinates = new ArrayList<Double[]>();
		coordinates.add(new Double[] {0.1, 0.0, 0.0, 0.0});
		
		File analysis1Folder = new File(customer.getFolder()+File.pathSeparator+"analysis1");
		analysis1Folder.mkdir();
		File analysis2Folder = new File(customer.getFolder()+File.pathSeparator+"analysis2");
		analysis2Folder.mkdir();
		File analysis3Folder = new File(customer.getFolder()+File.pathSeparator+"analysis3");
		analysis3Folder.mkdir();
		
		analysis1 = new Analysis(analysis1Folder, new Date(), Stance.TANDEM, "2_eyes_closed", coordinates, "comment1", "measurer");
		analysis2 = new Analysis(analysis2Folder, new Date(), Stance.TANDEM, "2_eyes_closed", coordinates, "comment2", "measurer");
		analysis3 = new Analysis(analysis3Folder, new Date(), Stance.TANDEM, "2_eyes_closed", coordinates, "comment3", "measurer");
		
		analysis1.drawChartToFile();
		analysis2.drawChartToFile();
		analysis3.drawChartToFile();
		
		customer.addAnalysis(analysis1);
		customer.addAnalysis(analysis2);
		customer.addAnalysis(analysis3);
		
	}
	
	@Test
	public void testAddAnalysis() {
		ellipsisPanel.addAnalysisToReport(analysis1, customer);
		ellipsisPanel.addAnalysisToReport(analysis2, customer);
		ellipsisPanel.addAnalysisToReport(analysis3, customer);
		
		Assert.assertEquals("AnalysisBox with analysis1 excists", EllipsisPanel.getBoxes().get(0).getAnalysis(), analysis1);
		Assert.assertEquals("AnalysisBox with analysis2 excists", EllipsisPanel.getBoxes().get(1).getAnalysis(), analysis2);
		Assert.assertEquals("AnalysisBox with analysis3 excists", EllipsisPanel.getBoxes().get(2).getAnalysis(), analysis3);
	}
	
	@Test
	public void testRemoveAnalysis() {
		ellipsisPanel.addAnalysisToReport(customer.getAnalysisList().get(0), customer);
		ellipsisPanel.addAnalysisToReport(customer.getAnalysisList().get(1), customer);
		ellipsisPanel.addAnalysisToReport(customer.getAnalysisList().get(2), customer);
		
		ellipsisPanel.removeAnalysisFromReport(analysis2);
		Assert.assertEquals("Correct size list after adding three analysis and removing middle one", EllipsisPanel.getBoxes().size(), 2);
		
		boolean excists = false;
		for(AnalysisBox currentAnalysisBox : EllipsisPanel.getBoxes()) {
			if(currentAnalysisBox.getAnalysis().equals(analysis1)) {
				excists = true;
			}
		}
		Assert.assertTrue("First added still analysis Excists", excists);
		
		excists = false;
		for(AnalysisBox currentAnalysisBox : EllipsisPanel.getBoxes()) {
			if(currentAnalysisBox.getAnalysis().equals(analysis3)) {
				excists = true;
			}
		}
		Assert.assertTrue("First added still analysis Excists", excists);

		
		ellipsisPanel.removeAnalysisFromReport(analysis1);
		Assert.assertEquals("Correct size list after removing firstly added", EllipsisPanel.getBoxes().size(), 1);
		
		excists = false;
		for(AnalysisBox currentAnalysisBox : EllipsisPanel.getBoxes()) {
			if(currentAnalysisBox.getAnalysis().equals(analysis3)) {
				excists = true;
			}
		}
		Assert.assertTrue("First added still analysis Excists", excists);
		
		ellipsisPanel.removeAnalysisFromReport(analysis3);
		Assert.assertEquals("Correct size list after removing firstly added", EllipsisPanel.getBoxes().size(), 0);
	}
}
