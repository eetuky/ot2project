package gui;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import javax.swing.JTable;
import javax.swing.SwingConstants;
import javax.swing.table.DefaultTableCellRenderer;

import org.junit.*;
import org.junit.rules.TemporaryFolder;

import domain.Analysis;
import util.Stance;
import gui.StatsPanel.AutoSizeJTable;
import gui.StatsPanel.StatsTableModel;

/**
 * Tests for GUI class StatsPanel.
 * 
 * @author Maarit
 */
public class TestStatsPanel {
	private StatsPanel statsPanel;
	private AutoSizeJTable statsTable;
	private Analysis analysis1;
	private Analysis analysis2;

	@Rule
	public final TemporaryFolder testFolder = new TemporaryFolder();

	@Before
	public void setUp() throws IOException {
		Locale.setDefault(new Locale("fi", "FI"));

		statsPanel = new StatsPanel();
		statsTable = (AutoSizeJTable) TestUtils.getChildNamed(statsPanel, "statsTable");
		ArrayList<Double[]> coordList = new ArrayList<>();
		coordList.add(new Double[] { 0.0, 0.0, 0.0, 0.0 });
		coordList.add(new Double[] { 0.01, 0.0, 0.0, 0.0 });
		analysis1 = new Analysis(testFolder.newFolder("test analysis"), new Date(), Stance.SEMI_TANDEM, "1_eyes_open",
				coordList, "Hyvin meni", "Tiina Testaaja");
		analysis2 = new Analysis(testFolder.newFolder("test analysis 2"), new Date(), Stance.COMFORTABLE,
				"2_eyes_closed", coordList, "", "Tiina Testaaja");
	}

	@Test
	public void testStatsPanelHasTable() {
		Assert.assertNotNull("Should have JTable component for stats", statsTable);

		JTable rowHeaderTable = (JTable) TestUtils.getChildNamed(statsPanel, "rowHeaderTable");
		Assert.assertNotNull("Should have JTable component for stats row headers", rowHeaderTable);
	}

	@Test
	public void testAddAnalysisToTable() {
		Assert.assertEquals("At first there are no columns in the stats table", 0, statsTable.getColumnCount());

		boolean wasAdded = statsPanel.addAnalysisToTable(analysis1);
		Assert.assertTrue("StatsPanel says that the analysis was added", wasAdded);
		Assert.assertFalse("StatsPanel has stats columns after adding one analysis", statsPanel.isEmpty());
		Assert.assertEquals("After adding one analysis there is one column in the table", 1,
				statsTable.getColumnCount());
		Assert.assertTrue("StatsPanel contains added analysis",
				((StatsTableModel) statsTable.getModel()).contains(analysis1));

		int alignment = ((DefaultTableCellRenderer) statsTable.getDefaultRenderer(Analysis.class))
				.getHorizontalAlignment();
		Assert.assertEquals("Analysis stats are be aligned to center of cell", SwingConstants.CENTER, alignment);

		wasAdded = statsPanel.addAnalysisToTable(analysis1);
		Assert.assertFalse("StatsPanel doesn't add an analysis that is already in stats table", wasAdded);
	}

	@Test
	public void testRemoveStatsFromTable() {
		statsPanel.addAnalysisToTable(analysis1);
		Assert.assertFalse("StatsPanel has stats columns after adding one", statsPanel.isEmpty());
		Assert.assertEquals("After adding one column there is one in the table", 1, statsTable.getColumnCount());
		Assert.assertTrue("StatsTable contains added analysis",
				((StatsTableModel) statsTable.getModel()).contains(analysis1));

		boolean wasRemoved = statsPanel.removeStatsFromTable(analysis1);
		Assert.assertTrue("Stats column was removed from table", wasRemoved);
		Assert.assertTrue("StatsPanel has zero stats columns after removing all of them", statsPanel.isEmpty());
		Assert.assertEquals("After removing all stats columns there are no columns left", 0,
				statsTable.getColumnCount());
		Assert.assertFalse("StatsPanel does not contain removed analysis",
				((StatsTableModel) statsTable.getModel()).contains(analysis1));

		// What happens if we remove an analysis that is not on the table
		try {
			wasRemoved = statsPanel.removeStatsFromTable(analysis1);
			Assert.assertTrue("Handles it without exceptions when trying to remove a nonexisting column.", true);
			Assert.assertFalse("Stats column was not removed from table", wasRemoved);
		} catch (Exception e) {
			Assert.fail("Can't handle situation if trying to remove a nonexisting column.");
		}
	}

	@Test
	public void testRemoveListOfAnalysesFromTable() {
		statsPanel.addAnalysisToTable(analysis1);
		Assert.assertEquals("After adding one analysis there is one column in the table", 1,
				statsTable.getColumnCount());
		Assert.assertTrue("StatsPanel contains added analysis",
				((StatsTableModel) statsTable.getModel()).contains(analysis1));

		List<Analysis> analysisList = new ArrayList<>();
		analysisList.add(analysis1);
		analysisList.add(analysis2);

		statsPanel.removeStatsFromTable(analysisList);
		Assert.assertEquals("After removing a list of analysis there are no columns in the table", 0,
				statsTable.getColumnCount());
		Assert.assertFalse("StatsPanel doesn't contain the removed analysis",
				((StatsTableModel) statsTable.getModel()).contains(analysis1));
	}

	@Test
	public void testHeadersUpdateWhenRemovingMiddleColumn() throws IOException {
		statsPanel.addAnalysisToTable(analysis1);
		statsPanel.addAnalysisToTable(analysis2);

		int col = 1;
		String headerOfAnalysis2 = (String) statsTable.getColumnModel().getColumn(col).getHeaderValue();
		Assert.assertEquals("Header of analysis2 is now 2", "2", headerOfAnalysis2);
		statsPanel.removeStatsFromTable(analysis1);

		col = 0;
		headerOfAnalysis2 = (String) statsTable.getColumnModel().getColumn(col).getHeaderValue();
		Assert.assertEquals("Header of analysis2 is now 1", "1", headerOfAnalysis2);
	}

	@Test
	public void testSelectColumn() {
		statsPanel.addAnalysisToTable(analysis1);
		Assert.assertEquals("At first no column is selected", 0, statsTable.getColumnModel().getSelectedColumnCount());

		statsPanel.selectColumn(analysis1);
		Assert.assertEquals("After selecting an analysis one column is selected", 1,
				statsTable.getColumnModel().getSelectedColumnCount());

		int indexOfAnalysis = ((StatsTableModel) statsTable.getModel()).getColumnIndex(analysis1);
		int selectedIndex = statsTable.getColumnModel().getSelectedColumns()[0];
		Assert.assertEquals("The selected column is the one that was set selected", indexOfAnalysis, selectedIndex);

	}

	@Test
	public void testtatsTableModel() {
		StatsTableModel model = ((StatsTableModel) statsTable.getModel());

		Assert.assertNull("Returns null analysis for an illegal column index", model.getAnalysisByColumnIndex(0));
		Assert.assertEquals("Returns column index -1 for an analysis that is not there", -1,
				model.getColumnIndex(analysis1));

		statsPanel.addAnalysisToTable(analysis1);
		int i = model.getColumnIndex(analysis1);

		Assert.assertEquals("Finds right column index for the added analysis", 0, i);
		Assert.assertEquals("Finds right analysis in the first column", analysis1, model.getAnalysisByColumnIndex(0));
		Assert.assertFalse("Table cells are not be editable", model.isCellEditable(0, 1));
		Assert.assertFalse("Column reordering are not allowed", statsTable.getTableHeader().getReorderingAllowed());
		Assert.assertEquals("Column class is Analysis", Analysis.class, model.getColumnClass(0));

		double amplAPmax = (double) statsTable.getValueAt(0, statsTable.getColumnCount() - 1);
		double amplAPavg = (double) statsTable.getValueAt(1, statsTable.getColumnCount() - 1);
		double amplMLmax = (double) statsTable.getValueAt(2, statsTable.getColumnCount() - 1);
		double amplMLavg = (double) statsTable.getValueAt(3, statsTable.getColumnCount() - 1);
		double freqAPavg = (double) statsTable.getValueAt(4, statsTable.getColumnCount() - 1);
		double freqMLavg = (double) statsTable.getValueAt(5, statsTable.getColumnCount() - 1);
		double travel = (double) statsTable.getValueAt(6, statsTable.getColumnCount() - 1);
		double duration = (double) statsTable.getValueAt(7, statsTable.getColumnCount() - 1);

		Assert.assertEquals("Max AP amplitude shows on first row", analysis1.getAmplitudeAPmax(), amplAPmax, 0.0);
		Assert.assertEquals("Average AP amplitude shows on second row", analysis1.getAmplitudeAPavg(), amplAPavg, 0.0);
		Assert.assertEquals("Max ML amplitude shows on third row", analysis1.getAmplitudeMLmax(), amplMLmax, 0.0);
		Assert.assertEquals("Average ML amplitude shows on fourth row", analysis1.getAmplitudeMLavg(), amplMLavg, 0.0);
		Assert.assertEquals("Average AP frequency shows on fifth row", analysis1.getFrequencyAPavg(), freqAPavg, 0.0);
		Assert.assertEquals("Average ML frequency shows on sixth row", analysis1.getFrequencyMLavg(), freqMLavg, 0.0);
		Assert.assertEquals("Travel shows on seventh row", analysis1.getTravel(), travel, 0.0);
		Assert.assertEquals("Duration shows on eighth row", analysis1.getDuration(), duration, 0.0);
	}
}