package gui;

import java.awt.Component;
import java.awt.Container;

import javax.swing.JMenu;

/**
 * Test utils for gui unit testing. Source:
 * http://www.ece.uvic.ca/~shsaad/seng426/resources/Lab%20Slides/SEGG426-Lab6.pdf
 */
class TestUtils {
	public int counter;

	public static Component getChildNamed(Component parent, String name) {
		if (name.equals(parent.getName()))
			return parent;

		if (parent instanceof Container) {
			Component[] children = (parent instanceof JMenu) ? ((JMenu) parent).getMenuComponents()
					: ((Container) parent).getComponents();

			for (Component component : children) {
				Component child = getChildNamed(component, name);
				if (child != null)
					return child;
			}
		}

		return null;
	}
}
