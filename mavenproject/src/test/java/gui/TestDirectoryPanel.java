package gui;

import static org.junit.Assert.assertEquals;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.util.*;

import javax.swing.UIManager;
import javax.swing.tree.TreePath;

import org.junit.*;
import org.junit.rules.TemporaryFolder;

import domain.Analysis;
import domain.Customer;
import name.lecaroz.java.swing.jocheckboxtree.CheckboxTree;
import tree.CustomJOCheckboxTreeTable;
import tree.MutableObjectNode;
import util.Stance;

/**
 * Tests for GUI class DirectoryPanel.
 * 
 * @author Maarit Ryynänen
 */
public class TestDirectoryPanel {

	private DirectoryPanel dirPanel;
	private Customer newCustomer;
	private Analysis newAnalysis1;
	private Analysis newAnalysis2;
	private Object selectedObject;

	@Rule
	public final TemporaryFolder testFolder = new TemporaryFolder();

	@Before
	public void setUp() throws Exception {
		Locale.setDefault(new Locale("fi", "FI"));
		UIManager.put("Tree.rowHeight", 30);

		File testDir = testFolder.newFolder("test directory");
		dirPanel = new DirectoryPanel(testDir, new ArrayList<>());
		newCustomer = new Customer(testFolder.newFolder("test customer"));

		ArrayList<Double[]> coordList = new ArrayList<>();
		coordList.add(new Double[] { 0.0, 0.0, 0.0, 0.0 });
		coordList.add(new Double[] { 0.01, 0.0, 0.0, 0.0 });
		newAnalysis1 = new Analysis(testFolder.newFolder("test analysis"), new Date(), Stance.SEMI_TANDEM,
				"1_eyes_open", coordList, "Hyvin meni", "Tiina Testaaja");
		newAnalysis2 = new Analysis(testFolder.newFolder("test analysis 2"), new Date(), Stance.SEMI_TANDEM,
				"2_eyes_closed", coordList, "Hyvin meni", "Tiina Testaaja");
	}

	@SuppressWarnings("unchecked")
	@Test
	public void testAddCustomerToTree() throws FileNotFoundException, UnsupportedEncodingException {
		Customer activeCustomer = dirPanel.getActiveCustomer();
		Assert.assertNull("At first no customer is selected", activeCustomer);

		dirPanel.addCustomerToTree(newCustomer);

		// Find selected node
		CustomJOCheckboxTreeTable<Object> treetable = (CustomJOCheckboxTreeTable<Object>) TestUtils
				.getChildNamed(dirPanel, "treetable");
		int row = Objects.requireNonNull(treetable).getSelectionModel().getMaxSelectionIndex();
		TreePath path = treetable.getTree().getPathForRow(row);
		MutableObjectNode activeNode = (MutableObjectNode) path.getLastPathComponent();
		Object obj = activeNode.getObject();
		Assert.assertEquals("After adding customer it is set selected", newCustomer, obj);

		// Find active customer
		activeCustomer = dirPanel.getActiveCustomer();
		Analysis activeAnalysis = dirPanel.getActiveAnalysis();

		Assert.assertEquals("After adding customer it is active", newCustomer, activeCustomer);
		Assert.assertNull("After addinc customer no analysis is active", activeAnalysis);
	}

	@SuppressWarnings("unchecked")
	@Test
	public void testRemoveCustomerFromTree() throws FileNotFoundException, UnsupportedEncodingException {
		dirPanel.addCustomerToTree(newCustomer);
		Customer activeCust = dirPanel.getActiveCustomer();
		Assert.assertEquals("After adding customer it is active", newCustomer, activeCust);

		dirPanel.removeCustomerFromTree(activeCust);
		activeCust = dirPanel.getActiveCustomer();
		Analysis activeAnalysis = dirPanel.getActiveAnalysis();
		Assert.assertNull("After removing customer no customer is active", activeCust);
		Assert.assertNull("After removing customer no analysis is active", activeAnalysis);

		// Find selected node
		CustomJOCheckboxTreeTable<Object> treetable = (CustomJOCheckboxTreeTable<Object>) TestUtils
				.getChildNamed(dirPanel, "treetable");
		int row = Objects.requireNonNull(treetable).getSelectionModel().getMaxSelectionIndex();
		Assert.assertEquals("After removing customer no tree node is selected", -1, row);
	}

	@Test
	public void testAddAnalysisToTree() {
		Analysis activeAnalysis = dirPanel.getActiveAnalysis();
		Customer activeCust = dirPanel.getActiveCustomer();
		Assert.assertNull("At first no analysis is selected", activeAnalysis);
		Assert.assertNull("At first no customer selected", activeCust);

		dirPanel.addCustomerToTree(newCustomer);
		dirPanel.addAnalysisToTree(newCustomer, newAnalysis1);
		activeAnalysis = dirPanel.getActiveAnalysis();
		activeCust = dirPanel.getActiveCustomer();
		Assert.assertEquals("Added analysis was selected active", newAnalysis1, activeAnalysis);
		Assert.assertEquals("Adding analysis made also customer selected", newCustomer, activeCust);
	}

	@SuppressWarnings("unchecked")
	@Test
	public void testCustomerCheckboxWhenAddingNewAnalysis() throws IOException {
		dirPanel.addCustomerToTree(newCustomer);
		dirPanel.addAnalysisToTree(newCustomer, newAnalysis1);

		// Find the nodes
		CustomJOCheckboxTreeTable<Object> treetable = (CustomJOCheckboxTreeTable<Object>) TestUtils
				.getChildNamed(dirPanel, "treetable");
		CheckboxTree tree = (CheckboxTree) Objects.requireNonNull(treetable).getTree();
		MutableObjectNode root = (MutableObjectNode) tree.getModel().getRoot();
		MutableObjectNode customerNode = root.getChildNode(newCustomer);
		TreePath customerPath = new TreePath(customerNode.getPath());

		// Add new analysis to a customer that is checked
		tree.getCheckingModel().addCheckingPath(customerPath);
		Assert.assertTrue("Customer node is now checked", tree.getCheckingModel().isPathChecked(customerPath));

		dirPanel.addAnalysisToTree(newCustomer, newAnalysis2);
		Assert.assertFalse("Customer node is not checked after adding new analysis",
				tree.getCheckingModel().isPathChecked(customerPath));
		Assert.assertTrue("Customer node checkbox is greyed after adding new analysis",
				tree.getCheckingModel().isPathGreyed(customerPath));
	}

	@SuppressWarnings("unchecked")
	@Test
	public void testUncheckAnalysis() {
		try {
			dirPanel.uncheckAnalysis(newAnalysis1);
			Assert.assertTrue("UncheckAnalysis() can be called with (wrong) analysis that is not checked", true);
		} catch (Exception e) {
			Assert.fail("UncheckAnalysis() failed to handle (wrong) analysis that is not checked");
		}

		dirPanel.addCustomerToTree(newCustomer);
		dirPanel.addAnalysisToTree(newCustomer, newAnalysis1);

		// Find the nodes
		CustomJOCheckboxTreeTable<Object> treetable = (CustomJOCheckboxTreeTable<Object>) TestUtils
				.getChildNamed(dirPanel, "treetable");
		CheckboxTree tree = (CheckboxTree) Objects.requireNonNull(treetable).getTree();
		MutableObjectNode root = (MutableObjectNode) tree.getModel().getRoot();
		MutableObjectNode customerNode = root.getChildNode(newCustomer);
		TreePath customerPath = new TreePath(customerNode.getPath());
		MutableObjectNode analysis1Node = customerNode.getChildNode(newAnalysis1);
		TreePath analysis1Path = new TreePath(analysis1Node.getPath());

		// Check analysis
		tree.addCheckingPath(customerPath);
		Assert.assertTrue("At first analysis is checked", tree.getCheckingModel().isPathChecked(analysis1Path));

		dirPanel.uncheckAnalysis(newAnalysis1);
		Assert.assertFalse("After calling uncheckAnalysis the analysis is no longer checked",
				tree.getCheckingModel().isPathChecked(analysis1Path));
		dirPanel.uncheckAnalysis(newAnalysis2);
		Assert.assertFalse("After unchecking all its analyses the customer is no longer grey",
				tree.getCheckingModel().isPathGreyed(customerPath));
	}

	@SuppressWarnings("unchecked")
	@Test
	public void testUpdateAnalysis() {
		dirPanel.addCustomerToTree(newCustomer);
		dirPanel.addAnalysisToTree(newCustomer, newAnalysis1);
		dirPanel.addAnalysisToTree(newCustomer, newAnalysis2);

		// Find the nodes
		CustomJOCheckboxTreeTable<Analysis> treetable = (CustomJOCheckboxTreeTable<Analysis>) TestUtils
				.getChildNamed(dirPanel, "treetable");
		CheckboxTree tree = (CheckboxTree) Objects.requireNonNull(treetable).getTree();
		MutableObjectNode root = (MutableObjectNode) tree.getModel().getRoot();
		MutableObjectNode customerNode = root.getChildNode(newCustomer);
		MutableObjectNode analysis2Node = customerNode.getChildNode(newAnalysis2);

		System.out.println("child at 0: " + customerNode.getChildAt(0));
		System.out.println("child at 1: " + customerNode.getChildAt(1));
		System.out.println("analysis1: " + newAnalysis1);
		System.out.println("analysis2: " + newAnalysis2);

		assertEquals("At first the analysis node is the first child of customer node", 0,
				customerNode.getIndex(analysis2Node));
		// Note: This test works when not running the whole unit tests but only this class' tests or failed tests

		newAnalysis2.setStance(Stance.TANDEM);
		dirPanel.updateAnalysis(newCustomer, newAnalysis2);
		assertEquals("After changing stance the analysis node is the second child of customer node", 0,
				customerNode.getIndex(analysis2Node));
	}

	@Test
	public void testRemoveAnalysisFromTree() throws FileNotFoundException, UnsupportedEncodingException {
		dirPanel.addCustomerToTree(newCustomer);
		dirPanel.addAnalysisToTree(newCustomer, newAnalysis1);
		Analysis activeAnalysis = dirPanel.getActiveAnalysis();

		dirPanel.removeAnalysisFromTree(newCustomer, activeAnalysis);
		activeAnalysis = dirPanel.getActiveAnalysis();
		Customer activeCust = dirPanel.getActiveCustomer();

		Assert.assertNull("After removing analysis no analysis is selected", activeAnalysis);
		Assert.assertEquals("Customer stays selected when removing analysis", newCustomer, activeCust);
	}

	@SuppressWarnings("unchecked")
	@Test
	public void testCustomerCheckboxWhenRemovingCheckedAnalysisFromTree() throws IOException {
		dirPanel.addCustomerToTree(newCustomer);
		dirPanel.addAnalysisToTree(newCustomer, newAnalysis1);
		dirPanel.addAnalysisToTree(newCustomer, newAnalysis2);

		// Find the nodes
		CustomJOCheckboxTreeTable<Object> treetable = (CustomJOCheckboxTreeTable<Object>) TestUtils
				.getChildNamed(dirPanel, "treetable");
		CheckboxTree tree = (CheckboxTree) Objects.requireNonNull(treetable).getTree();
		MutableObjectNode root = (MutableObjectNode) tree.getModel().getRoot();
		MutableObjectNode customerNode = root.getChildNode(newCustomer);
		TreePath customerPath = new TreePath(customerNode.getPath());
		MutableObjectNode checkedAnalysisNode = customerNode.getChildNode(newAnalysis1);
		TreePath checkedAnalysisPath = new TreePath(checkedAnalysisNode.getPath());

		// Remove (the only) checked analysis from a customer
		tree.getCheckingModel().addCheckingPath(checkedAnalysisPath);
		Assert.assertTrue("At first Customer node checkbox is greyed",
				tree.getCheckingModel().isPathGreyed(customerPath));

		dirPanel.removeAnalysisFromTree(newCustomer, newAnalysis1);
		Assert.assertFalse("Customer node is not checked after removing (the only) checked analysis",
				tree.getCheckingModel().isPathChecked(customerPath));
		Assert.assertFalse("Customer node checkbox is not greyed after removing (the only) checked analysis",
				tree.getCheckingModel().isPathGreyed(customerPath));

		// Let's put the analysis back and check it again
		dirPanel.addAnalysisToTree(newCustomer, newAnalysis1);
		tree.getCheckingModel().addCheckingPath(checkedAnalysisPath);
		Assert.assertTrue("At first Customer node checkbox is greyed",
				tree.getCheckingModel().isPathGreyed(customerPath));

		// Remove (the only) unchecked analysis from a customer
		dirPanel.removeAnalysisFromTree(newCustomer, newAnalysis2);
		Assert.assertTrue("Customer node is checked after removing the only unchecked analysis",
				tree.getCheckingModel().isPathChecked(customerPath));
		Assert.assertFalse("Customer node checkbox is not greyed after removing the only checked analysis",
				tree.getCheckingModel().isPathGreyed(customerPath));
	}

	@SuppressWarnings("unchecked")
	@Test
	public void testSelectCustomer() throws IOException {
		dirPanel.addCustomerToTree(newCustomer);
		Customer newCustomer2 = new Customer(testFolder.newFolder("another test customer"));
		dirPanel.addCustomerToTree(newCustomer2);

		// Find the selected customer
		CustomJOCheckboxTreeTable<Object> treetable = (CustomJOCheckboxTreeTable<Object>) TestUtils
				.getChildNamed(dirPanel, "treetable");
		CheckboxTree tree = (CheckboxTree) Objects.requireNonNull(treetable).getTree();
		Customer activeCustomer = dirPanel.getActiveCustomer();
		Customer selectedCustomer = (Customer) ((MutableObjectNode) tree.getSelectionPath().getLastPathComponent())
				.getObject();

		Assert.assertEquals("At first newCustomer2 is set active", newCustomer2, activeCustomer);
		Assert.assertEquals("At first newCustomer2 is set selected", newCustomer2, selectedCustomer);

		// Change selection
		dirPanel.selectCustomer(newCustomer);

		activeCustomer = dirPanel.getActiveCustomer();
		selectedCustomer = (Customer) ((MutableObjectNode) tree.getSelectionPath().getLastPathComponent()).getObject();

		Assert.assertEquals("After selecting newCustomer it is set active", newCustomer, activeCustomer);
		Assert.assertEquals("After selecting newCustomer it is set selected", newCustomer, selectedCustomer);

	}

	@SuppressWarnings("unchecked")
	@Test
	public void testSelectCheckedAnalysis() {
		dirPanel.addCustomerToTree(newCustomer);
		dirPanel.addAnalysisToTree(newCustomer, newAnalysis1);

		// Find the nodes and check the customer
		CustomJOCheckboxTreeTable<Object> treetable = (CustomJOCheckboxTreeTable<Object>) TestUtils
				.getChildNamed(dirPanel, "treetable");
		CheckboxTree tree = (CheckboxTree) Objects.requireNonNull(treetable).getTree();
		MutableObjectNode root = (MutableObjectNode) tree.getModel().getRoot();
		MutableObjectNode customerNode = root.getChildNode(newCustomer);

		MutableObjectNode analysis1Node = customerNode.getChildNode(newAnalysis1);
		TreePath analysis1Path = new TreePath(analysis1Node.getPath());
		tree.addCheckingPath(analysis1Path);

		dirPanel.addAnalysisToTree(newCustomer, newAnalysis2);
		MutableObjectNode analysis2Node = customerNode.getChildNode(newAnalysis2);
		TreePath analysis2Path = new TreePath(analysis2Node.getPath());

		Assert.assertTrue("At first the analysis is checked", tree.getCheckingModel().isPathChecked(analysis1Path));
		Assert.assertTrue("At first the last added analysis is selected",
				tree.getSelectionModel().isPathSelected(analysis2Path));

		dirPanel.selectCheckedAnalysis(newAnalysis1);
		Analysis activeAnalysis = dirPanel.getActiveAnalysis();
		Analysis selectedAnalysis = (Analysis) ((MutableObjectNode) tree.getSelectionPath().getLastPathComponent())
				.getObject();
		Assert.assertEquals("After calling selectCheckedAnalysis() the first analysis is set active", newAnalysis1,
				activeAnalysis);
		Assert.assertEquals("After calling selectCheckedAnalysis() the analysis is set selected", newAnalysis1,
				selectedAnalysis);

		try {
			dirPanel.selectCheckedAnalysis(newAnalysis2);

			selectedAnalysis = (Analysis) ((MutableObjectNode) tree.getSelectionPath().getLastPathComponent())
					.getObject();
			activeAnalysis = dirPanel.getActiveAnalysis();
			Assert.assertEquals("After calling selectCheckedAnalysis() on false analysis the previous selection stays",
					newAnalysis1, selectedAnalysis);
			Assert.assertEquals(
					"After calling selectCheckedAnalysis() on false analysis the previous analysis stays active",
					newAnalysis1, activeAnalysis);

		} catch (Exception e) {
			Assert.fail("SelectCheckedAnalysis() failed to handle (wrong) analysis that is not checked");
		}
	}

	@Test
	public void testSelectionListener() throws FileNotFoundException, UnsupportedEncodingException {
		dirPanel.setSelectionListener(new SelectionListener() {
			@Override
			public void checkedBoxChanged(Object checkedObject, boolean isChecked) {
				// TODO Auto-generated method stub
			}

			@Override
			public void selectionChanged(Object selectedObj) {
				// TODO Auto-generated method stub
				selectedObject = selectedObj;
			}
		});

		Assert.assertNull("At first no Customer nor Analysis is selected active", selectedObject);
		dirPanel.addCustomerToTree(newCustomer);
		Assert.assertEquals("After adding customer the listener is notified with the added customer", newCustomer,
				selectedObject);
		dirPanel.addAnalysisToTree(newCustomer, newAnalysis1);
		Assert.assertEquals("After adding analysis the listener is notified with the added analysis", newAnalysis1,
				selectedObject);
	}
}
