package gui;

import java.io.File;
import java.io.IOException;
import java.util.*;
import java.util.concurrent.atomic.AtomicBoolean;

import javax.swing.JButton;
import org.junit.*;
import org.junit.rules.TemporaryFolder;

import domain.Analysis;
import domain.Customer;
import util.Stance;

/**
 * Tests for GUI class DirectoryPanel.
 * 
 * @author Maarit Ryynänen
 */
public class TestApplicationWindow {

	private ApplicationWindow applWindow;
	private JButton btnNewCustomer;
	private JButton btnNewAnalysis;
	private JButton btnEdit;
	private JButton btnDelete;
	private JButton btnPrintPdf;
	private DirectoryPanel dirPanel;
	private final AtomicBoolean showGui = new AtomicBoolean(true);

	@Rule
	public final TemporaryFolder testFolder = new TemporaryFolder();

	@Before
	public void setUp() throws IOException {
		Locale.setDefault(new Locale("fi", "FI"));

		File testDir = testFolder.newFile("test_dir");
		applWindow = new ApplicationWindow(testDir);

		applWindow.runGUI(new ArrayList<>(), showGui);
		
		btnNewCustomer= (JButton) TestUtils.getChildNamed(applWindow, "btnNewCustomer");
		btnNewAnalysis = (JButton) TestUtils.getChildNamed(applWindow, "btnNewAnalysis");
		btnEdit = (JButton) TestUtils.getChildNamed(applWindow, "btnEdit");
		btnDelete = (JButton) TestUtils.getChildNamed(applWindow, "btnDelete");
		btnPrintPdf = (JButton) TestUtils.getChildNamed(applWindow, "btnPrintPdf");
		
		dirPanel = (DirectoryPanel) TestUtils.getChildNamed(applWindow, "directoryPanel");
	}

	@Test
	public void testGUIFailsToRunOnNullCustomerList() {
		try {
			applWindow.runGUI(null, showGui);
		} catch (Exception e) {
			Assert.assertSame("NullPointerException thrown if trying to run ApplicationWindow with null customer list", e.getClass(), NullPointerException.class);
		}
	}

	@Test
	public void testHasToolbarAndPanels() {
		Object toolbar = TestUtils.getChildNamed(applWindow, "toolbar");
		Object commentPanel = TestUtils.getChildNamed(applWindow, "commentPanel");
		Object ellipsesPanel = TestUtils.getChildNamed(applWindow, "ellipsesPanel");
		Object statsPanel = TestUtils.getChildNamed(applWindow, "statsPanel");

		Assert.assertNotNull("Should have JToolbar component", toolbar);
		Assert.assertNotNull("Should have DirectoryPanel component", dirPanel);
		Assert.assertNotNull("Should have CommentPanel component", commentPanel);
		Assert.assertNotNull("Should have EllipsesPanel component", ellipsesPanel);
		Assert.assertNotNull("Should have StatsPanel component", statsPanel);
	}

	@Test
	public void testButtonsAreDisabled() {
		Assert.assertTrue("btnNewCustomer is enabled on start up", btnNewCustomer.isEnabled());
		Assert.assertFalse("btnNewAnalysis is disabled on start up", btnNewAnalysis.isEnabled());
		Assert.assertFalse("btnEdit is disabled on start up", btnEdit.isEnabled());
		Assert.assertFalse("btnDelete is disabled on start up", btnDelete.isEnabled());
		Assert.assertFalse("btnPrintPdf is disabled on start up", btnPrintPdf.isEnabled());
	}

	@Test
	public void testButtonsAreEnableOnTreeSelection() throws IOException {
		dirPanel.fireSelectionChanged(new Customer(testFolder.newFolder("test customer")));
		
		Assert.assertTrue("btnNewCustomer is enabled when customer is selected", btnNewCustomer.isEnabled());
		Assert.assertTrue("btnDelete is enabled when customer is selected", btnDelete.isEnabled());
		Assert.assertTrue("btnNewAnalysis is enabled when customer is selected", btnNewAnalysis.isEnabled());
		
		// TODO: Ei vielä toteutettu asiakkaan muokkausta
//		Assert.assertTrue("btnEdit is enabled when customer is selected", btnEdit.isEnabled());

		ArrayList<Double[]> coordList = new ArrayList<>();
		coordList.add(new Double[] {0.0, 0.0, 0.0, 0.0});
		coordList.add(new Double[] {0.01, 0.1, 0.2, 0.0});
		Analysis testAnalysis = new Analysis(testFolder.newFolder("test analysis"), new Date(), Stance.SEMI_TANDEM,
				"1_eyes_open", coordList, "Hyvin meni", "Tiina Testaaja");
		dirPanel.fireSelectionChanged(testAnalysis);
		
		Assert.assertTrue("btnNewCustomer is enabled when analysis is selected", btnNewCustomer.isEnabled());
		Assert.assertTrue("btnNewAnalysis is enabled when analysis is selected", btnNewAnalysis.isEnabled());
		Assert.assertTrue("btnEdit is enabled when analysis is selected", btnEdit.isEnabled());
		Assert.assertTrue("btnDelete is enabled when analysis is selected", btnDelete.isEnabled());
	}
}
