package tree;

import java.util.LinkedList;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import tree.MutableObjectNode;

public class TestMutableObjectNode {
	private MutableObjectNode node;

	@Before
	public void setUp() throws Exception {
		String obj = "Parent object";
		node = new MutableObjectNode(obj, true, true);
	}

	@Test
	public void testAddChildToObjectNode() {
		Assert.assertEquals("There are no child nodes before adding", 0, node.getChildCount());
		String child = "Child object";
		node.addChild(child, false);
		Assert.assertEquals("There is one child node after adding one child", 1, node.getChildCount());
	}

	@Test
	public void testAddChildListToObjectNode() {
		Assert.assertEquals("There are no child nodes before adding", 0, node.getChildCount());

		Object child1 = "Child object no 1";
		Object child2 = "Child object no 2";
		Object child3 = "Child object no 3";
		LinkedList<Object> childList = new LinkedList<>();
		childList.add(child1);
		childList.add(child2);
		childList.add(child3);
		node.addChildren(childList, false);
		
		Assert.assertEquals("There are three children nodes after adding three", 3, node.getChildCount());
		Object addedChild1 = ((MutableObjectNode) node.getChildAt(0)).getObject();
		Object addedChild2 = ((MutableObjectNode) node.getChildAt(1)).getObject();
		Object addedChild3 = ((MutableObjectNode) node.getChildAt(2)).getObject();
		Assert.assertEquals("First added child was returned", child1, addedChild1);
		Assert.assertEquals("Second added child was returned", child2, addedChild2);
		Assert.assertEquals("Third added child was returned", child3, addedChild3);
	}

	@Test
	public void testRemoveChildFromObjectNode() {
		String child = "Child to remove";
		node.addChild(child, false);
		Assert.assertEquals("There is one child node before removing it", 1, node.getChildCount());

		node.removeChild(child);
		Assert.assertEquals("There are no children nodes after removing", 0, node.getChildCount());
	}
}
