package domain;

import java.io.File;
import java.io.IOException;
import java.util.LinkedList;
import java.util.List;
import java.util.stream.Collectors;

import org.json.simple.parser.ParseException;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.TemporaryFolder;

public class TestFileController {

	private FileController fileController;
	private File customerFolder1;
	private File customerFolder2;
	private File customerFolder3;

	@Rule
	public final TemporaryFolder testFolder = new TemporaryFolder();

	@Before
	public void setUp() throws IOException {
		customerFolder1 = testFolder.newFolder("customer1");
		customerFolder3 = testFolder.newFolder("customer3");
		customerFolder2 = testFolder.newFolder("customer2");
		fileController = new FileController(testFolder.getRoot());
	}

	@Test
	public void testPullCustomers() throws IOException, ParseException {
		List<Customer> customers = fileController.pullCustomers();
		List<File> customerFolders = customers.stream().map(Customer::getFolder)
				.collect(Collectors.toCollection(LinkedList::new));

		Assert.assertTrue("Customer list from in working directory contains customer1",
				customerFolders.contains(customerFolder1));
		Assert.assertTrue("Customer list from in working directory contains customer2",
				customerFolders.contains(customerFolder2));
		Assert.assertTrue("Customer list from in working directory contains customer3",
				customerFolders.contains(customerFolder3));
	}
	
	@Test
	public void testCustomerListIsSorted() throws IOException, ParseException {
		List<Customer> customers = fileController.pullCustomers();
		List<File> customerFolders = customers.stream().map(Customer::getFolder)
				.collect(Collectors.toCollection(LinkedList::new));

		Assert.assertEquals(customerFolders.get(0), customerFolder1);
		Assert.assertEquals(customerFolders.get(1), customerFolder2);
		Assert.assertEquals(customerFolders.get(2), customerFolder3);
	}
}
