package domain;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.Date;
import java.util.LinkedList;

import org.json.simple.parser.ParseException;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.TemporaryFolder;

import util.Stance;

/**
 * TestClass for testing customer class
 * 
 * @author Juha Korvenaho
 *
 */
public class TestCustomer {

	@Rule
	// Creating temporary folder which will hold our temporary file
	public final TemporaryFolder folder = new TemporaryFolder();
	private File testFile;
	private File testFile2;
	private Customer testPerson;
	private Customer testPerson2;
	private Analysis testAnalysis1;
	private Analysis testAnalysis2;

	@Before
	// This will be done before each test
	public void setUp() throws IOException {

		testFile = folder.newFolder("customer1");
		testFile2 = folder.newFolder("customer2");
		testPerson = new Customer(testFile, "");
		testPerson2 = new Customer(testFile2);

		ArrayList<Double[]> coordList = new ArrayList<>();
		coordList.add(new Double[] { 0.01, 0.0, 0.0, 0.0 });
		testAnalysis1 = new Analysis(folder.newFolder("test analysis"), new Date(), Stance.SEMI_TANDEM, "1_eyes_open",
				coordList, "Hyvin meni", "Miittaaja");
		testAnalysis2 = new Analysis(folder.newFolder("test analysis 2"), new Date(), Stance.COMFORTABLE,
				"2_eyes_closed", coordList, "", "");
	}

	@Test
	public void testSaveToFile() throws IOException {
		try {
			testPerson.saveToFile();
//			Assert.fail("Should throw IOException if folder could not be created");
		} catch (IOException e) {
			Assert.assertTrue("Throws IOException if folder could not be created", true);
		}
		
		Assert.assertEquals("testing if we get same file back", testFile2, testPerson2.getFolder());
		Customer testPerson3 = new Customer(testFile);
		Assert.assertEquals("testing setting file to new test person", testFile, testPerson3.getFolder());

//		testPerson3.setFile(testFile2);
//		Assert.assertEquals("we changed test file and expect different file back", testFile2, testPerson3.getFile());
// Antaa AccessDeniedExceptionin. Onko niin että Temp-tiedostoja ei voi muuttaa
	}
	
	@Test
	public void testAddAnalysis() throws FileNotFoundException, UnsupportedEncodingException {
		Customer testPerson3 = new Customer(testFile);
		Analysis anal = null;
		Assert.assertEquals("Checking that new persons list is empty", 0, testPerson3.getAnalysisList().size());
		testPerson3.addAnalysis(anal);
		Assert.assertEquals("Customer should have empty list since we added a null Analysis", 0, testPerson3.getAnalysisList().size());
		testPerson.addAnalysis(testAnalysis1);
		Assert.assertEquals("Checking if two different customer objects really are instances of the same class",
				testPerson2.getClass(), testPerson.getClass());
		LinkedList<Analysis> test = testPerson.getAnalysisList();
		for (Analysis a : test) {
			Assert.assertEquals("They should be equals since we only have one test in a list", testAnalysis1, a);
		}
	}

	@Test
	public void testAddAnalysesList() throws FileNotFoundException, UnsupportedEncodingException {
		Customer testPerson4 = new Customer(testFile);
		LinkedList<Analysis> anal = null;
		Assert.assertEquals("Checking that new persons list is empty", 0, testPerson4.getAnalysisList().size());
		testPerson4.addAnalysis(anal);
		Assert.assertEquals("List size should still be zero since we added a null list", 0, testPerson4.getAnalysisList().size());

		LinkedList<Analysis> testList = new LinkedList<>();
		testList.add(testAnalysis1);
		testList.add(testAnalysis2);

		Assert.assertEquals("Checking that new persons list is empty", 0, testPerson.getAnalysisList().size());
		testPerson.addAnalysis(testList);
		LinkedList<Analysis> aList = testPerson.getAnalysisList();
		Assert.assertTrue("All added analysis are on customer's analysis list", aList.containsAll(testList));
	}

	@Test
	public void testingAddComment() throws IOException, ParseException {
		Assert.assertEquals("we didn't have comments so should return empty string", "", testPerson2.getComment());
		String testComment = "test comment";
		testPerson.setComment(testComment);
		Assert.assertEquals("Expecting same comment", testComment, testPerson.getComment());
		Customer testPerson6 = new Customer(testFile);
		testPerson6.setComment("comment");
		
		testPerson.setComment("Another comment");
		Assert.assertNotEquals("We changed comment so it should not be same anymore", testComment,
				testPerson.getComment());
		Assert.assertEquals("Comparing against newly set comment", "Another comment", testPerson.getComment());
		
	}

	@Test
	public void testToString() {
		Assert.assertEquals("Checking if testPerson knows his real name", testFile.getName(), testPerson.toString());
	}

	@Test
	public void testCompareTo() {
		Assert.assertEquals("comparing testPerson to testPerson2, should be -1", -1, testPerson.compareTo(testPerson2));
	}
	
	@Test
	public void testRemove() throws IOException {
			Assert.assertEquals("Checking that new persons list is empty", 0, testPerson.getAnalysisList().size());
			LinkedList<Analysis> testList = new LinkedList<>();
			testList.add(testAnalysis1);
			testList.add(testAnalysis2);
			testPerson.addAnalysis(testList);
			
			Assert.assertTrue("All added analysis are on customer's analysis list", testPerson.getAnalysisList().containsAll(testList));
			Assert.assertEquals("Both lists' sizes should be equal", testList.size(), testPerson.getAnalysisList().size());

			testPerson.removeSingleAnalysis(testAnalysis1);
			testList.remove(testAnalysis1);
			
			Assert.assertTrue("After removing one from both lists they should be equal again", testList.containsAll(testPerson.getAnalysisList()));
			Assert.assertEquals("Removed one from both so sizes should be equal", testList.size(),
					testPerson.getAnalysisList().size());
	}

	@Test
	public void testRemoveAll() throws IOException {
			LinkedList<Analysis> testList = new LinkedList<>();
			testList.add(testAnalysis1);
			testList.add(testAnalysis2);
			testPerson.addAnalysis(testList);

			Assert.assertEquals("At first customer has analyses", testList.size(), testPerson.getAnalysisList().size());
			testPerson.removeAllAnalysis();
			Assert.assertEquals(new LinkedList<Analysis>(), testPerson.getAnalysisList());
	}
	
	@Test
	public void testDeleteAll() throws IOException {
		try {
			testPerson.deleteFiles();
			Assert.assertTrue("Deleting was succesful", true);
		} catch (Exception e) {
			Assert.fail("Customer files were not deleted successfully");
		}
	}
}