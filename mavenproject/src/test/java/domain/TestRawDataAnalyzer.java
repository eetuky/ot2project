package domain;

import java.io.File;
import java.io.IOException;

import org.junit.Assert;
import org.junit.Test;

/**
 * Testaa RaakaMittaus-olion toimintaa 
 * lukee src/test/resources/exampleRawData.txt raakadatatiedostosta
 * @author Eetu
 *
 */
public class TestRawDataAnalyzer {
	
	
	
	
	/**
	 * Testataan, lukeeko tiedostosta oikein
	 */
	@Test
	public void testReading() {
		
		//Get raw-data file
		File rawData = new File("test/resources/exampleRawData.txt");
		//System.out.println(raakaData.getAbsolutePath());

		//Giving raw-data_analyzer the path to testfile
		System.out.println(rawData.getAbsolutePath());
		RawDataAnalyzer testi = null;
		try {
			testi = new RawDataAnalyzer(rawData.getAbsolutePath()); 
		} catch (IOException e) {
			Assert.fail("File was not validated correctly");
		}
		//System.out.println(raakaData.getAbsolutePath());
		//System.out.println(raakaData.getPath());
		//testi.getAnalysis();
		/*
		Time		Acc.			Gyro.		Roll	Pitch	Yaw
		t(s)	X(m/s2)	Y(m/s2)	Z(m/s2)	X(/s)	Y(/s)	Z(/s)	()	()	()
		0.010	9.393	-0.116	2.259	0.000	-0.366	-0.061	-4.6	-76.7	-124.8
		
		vika
		0.800	9.375	-0.139	2.217	-0.244	-1.526	-0.244	-4.6	-76.8	-124.6
		*/
		
		//test if reading first line successfull
		Assert.assertEquals(0.010, testi.getRawData().get(0)[0],0.001);
		Assert.assertEquals(9.393, testi.getRawData().get(0)[1],0.001);
		Assert.assertEquals(-0.116, testi.getRawData().get(0)[2],0.001);
		Assert.assertEquals(2.259, testi.getRawData().get(0)[3],0.001);
		Assert.assertEquals(0.000, testi.getRawData().get(0)[4],0.001);
		Assert.assertEquals(-0.366, testi.getRawData().get(0)[5],0.001);
		Assert.assertEquals(-0.061, testi.getRawData().get(0)[6],0.001);
		Assert.assertEquals(-4.6, testi.getRawData().get(0)[7],0.001);
		Assert.assertEquals(-76.7, testi.getRawData().get(0)[8],0.001);
		Assert.assertEquals(-124.8, testi.getRawData().get(0)[9],0.001);
		
		//Last row
		Assert.assertEquals(-4.6, testi.getRawData().get(79)[7],0.001);
		Assert.assertEquals(-76.8, testi.getRawData().get(79)[8],0.001);
		Assert.assertEquals(-124.6, testi.getRawData().get(79)[9],0.001);
		
		
		//This should throw exception
		Double asd = null;
		try {
			asd = testi.getCoordList().get(81)[0];
		} catch (Exception e) {
			asd = 0.5;
		}
		Assert.assertEquals(0.5,asd,0.001);
		
		
		//Test if coordlist has time		
		Assert.assertEquals(0.00, testi.getCoordList().get(0)[0],0.001);
		Assert.assertEquals(0.01, testi.getCoordList().get(1)[0],0.001);
		Assert.assertEquals(0.8, testi.getCoordList().get(80)[0],0.001);
		
	}
}
