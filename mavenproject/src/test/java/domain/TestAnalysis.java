package domain;


import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;

import org.junit.Assert;
import org.junit.Test;

import util.Stance;

import org.junit.Before;
import org.junit.Rule;
import org.junit.rules.TemporaryFolder;

public class TestAnalysis {
	@Rule
	// Creating temporary folder which will hold our temporary file
	public final TemporaryFolder tempF = new TemporaryFolder();
	private Analysis analysis;

	
	@Before
	public void setUp()throws IOException{
		File analysisF = tempF.newFolder("test analysis");
		ArrayList<Double[]> coordList = new ArrayList<>();
		coordList.add(new Double[] { 0.01, 0.0, 0.0, 0.0 });
		analysis = new Analysis(analysisF, new Date(), Stance.SEMI_TANDEM, "1_eyes_open",coordList, "Hyvin meni", "Tiina Testaaja");
		 
	}
	@Test
	public void testToString() {
		String vJSon = "3_semi-tandem_stance, 1_eyes_open";
		System.out.println(analysis.toString());
		Assert.assertEquals(vJSon, analysis.toString());
	}
	
	
	

	
}
