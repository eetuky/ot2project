package tree;

import java.awt.Component;
import java.awt.Dimension;
import java.util.Locale;
import java.util.ResourceBundle;

import javax.swing.BoxLayout;
import javax.swing.JTree;
import javax.swing.UIManager;
import javax.swing.tree.TreePath;

import domain.Analysis;
import name.lecaroz.java.swing.jocheckboxtree.CheckboxTree;
import name.lecaroz.java.swing.jocheckboxtree.DefaultCheckboxTreeCellRenderer;
import name.lecaroz.java.swing.jocheckboxtree.TreeCheckingModel;
import name.lecaroz.java.swing.jocheckboxtree.TreeNodeObject;
import name.lecaroz.java.swing.jocheckboxtree.QuadristateButtonModel.State;

public class CustomizedTreeCellRenderer extends DefaultCheckboxTreeCellRenderer {

	/** Attribute required by Serializable interface, which is extended by JPanel */
	private static final long serialVersionUID = -2703384182072860499L;
	private final ResourceBundle messages;

	public CustomizedTreeCellRenderer() {
		this.messages = ResourceBundle.getBundle("Messages", Locale.getDefault());

		setOpaque(false);
		setFont(UIManager.getFont("Table.font"));

		this.checkBox.setOpaque(false);

		setLayout(new BoxLayout(this, BoxLayout.LINE_AXIS));
		add(this.checkBox);
		add(this.label);
	}

	@Override
	public Component getTreeCellRendererComponent(JTree tree, Object object, boolean selected, boolean expanded,
			boolean leaf, int row, boolean hasFocus) {
		// Set label text
		MutableObjectNode node = (MutableObjectNode) object;
		Object obj = node.getObject();
		this.label.setText(obj.toString());
		this.label.setIcon(node.getIcon());

		// Customize Analysis nodes text
		if (obj.getClass() == Analysis.class) {
			Analysis a = (Analysis) obj;
			String stance = messages.getString(a.getStance().getStance());
			String eyes = messages.getString(a.getEyes());
			this.label.setText(stance + " / " + eyes);
		}

		/*
		 * Here's some copy code from DefaultTreeCellRenderer. The rest of rendering
		 * depends on the TreeCheckingModel.
		 */
		final Component component = (object instanceof TreeNodeObject)
				? ((TreeNodeObject) object).getTreeCellRendererComponent(
						tree, this, label, selected, expanded, leaf, row, hasFocus)
				: null;
		if (component == null) {
			if (!(object instanceof TreeNodeObject))
				this.label.getTreeCellRendererComponent(tree, object, selected, expanded, leaf, row, hasFocus);
			if (tree instanceof CheckboxTree) {
				TreePath path = tree.getPathForRow(row);
				TreeCheckingModel checkingModel = ((CheckboxTree) tree).getCheckingModel();
				if (object instanceof TreeNodeObject && !((TreeNodeObject) object).canBeChecked()) {
					this.checkBox.setVisible(false);
				} else {
					this.checkBox.setVisible(true);
					this.checkBox.setEnabled(checkingModel.isPathEnabled(path) && tree.isEnabled()
							&& (!(object instanceof TreeNodeObject) || ((TreeNodeObject) object).isEnabled()));
					boolean checked = checkingModel.isPathChecked(path);
					boolean greyed = checkingModel.isPathGreyed(path);
					if (checked && !greyed) {
						this.checkBox.setState(State.CHECKED);
					}
					if (!checked && greyed) {
						this.checkBox.setState(State.GREY_UNCHECKED);
					}
					if (checked && greyed) {
						this.checkBox.setState(State.GREY_CHECKED);
					}
					if (!checked && !greyed) {
						this.checkBox.setState(State.UNCHECKED);
					}
				}
			}
		}

		int rowHeight = UIManager.getInt("Tree.rowHeight");

		// Set location for checkbox
		Dimension d_check = this.checkBox.getPreferredSize();
		int w_check = 0;
		if (this.checkBox.isVisible()) {
			int y_check = (rowHeight - this.checkBox.getHeight()) / 2;
			w_check = d_check.width;
			this.checkBox.setLocation(0, y_check);
			this.checkBox.setBounds(0, y_check, w_check, d_check.height);
		}

		// Set location for label
		Dimension d_label = this.label.getPreferredSize();
		int y_label = (rowHeight - this.label.getHeight()) / 2;
		int hGap = 2;
		this.label.setLocation(w_check + hGap, y_label);
		this.label.setBounds(w_check + hGap, y_label, d_label.width, d_label.height);

		return component == null ? this : component;
	}
	
	@Override
	public boolean isOnHotspot(int x, int y) {
		return this.checkBox.getBounds().contains(x, y);
	}
}
