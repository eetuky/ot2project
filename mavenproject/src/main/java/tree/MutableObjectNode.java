package tree;

import java.awt.Component;
import java.io.IOException;
import java.io.InputStream;
import java.util.List;

import javax.imageio.ImageIO;
import javax.swing.*;
import javax.swing.tree.*;

import domain.Analysis;
import domain.Customer;
import name.lecaroz.java.swing.jocheckboxtree.TreeNodeObject;
import util.Stance;

public class MutableObjectNode extends DefaultMutableTreeNode implements TreeNodeObject, Comparable<MutableObjectNode> {

	/**
	 * Attribute required by Serializable interface, which is extended by
	 * DefaultMutableTreeNode
	 */
	private static final long serialVersionUID = 7771502885956431380L;
	final private boolean canBeChecked;
	private Icon icon; // jos solmulle halutaan oma kuvake

	public MutableObjectNode(Object object, boolean canBeChecked, boolean allowsChildren) {
		super(object, allowsChildren);
		this.canBeChecked = canBeChecked;

		if (object.getClass() == Analysis.class) {
			setIcon();
		} else if (object.getClass() != Customer.class) { // Object is neither Customer nor Analysis
			this.icon = UIManager.getIcon("FileView.directoryIcon");
		} else { // Customer nodes have no icon
			this.icon = null;
		}
	}

	@Override
	public Object getObject() {
		return super.getUserObject();
	}

	public Icon getIcon() {
		return icon;
	}

	public void updateIcon() {
		if (getObject().getClass() == Analysis.class) {
			setIcon();
		}
	}
	
	private void setIcon() {
		// Set unique icon showing Analysis stance and eyes (only to Analysis nodes)
		Analysis analysis = (Analysis) getObject();
		Stance stance = analysis.getStance();
		String eyes = analysis.getEyes();

		ImageIcon tmpIcon = null;
		try (InputStream stream = MutableObjectNode.class
				.getResourceAsStream("/images/" + stance + eyes + ".png")) {
			if (stream != null) {
				tmpIcon = new ImageIcon(ImageIO.read(stream));
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
		// Scale icon size and set icon
		int rowHeight = UIManager.getInt("Tree.rowHeight");
		this.icon = tmpIcon == null ? null
				: new ImageIcon(tmpIcon.getImage().getScaledInstance(-1, rowHeight,
						java.awt.Image.SCALE_SMOOTH));
	}

	@Override
	public boolean canBeChecked() {
		return this.canBeChecked;
	}

	@Override
	public boolean isEnabled() {
		return true;
	}

	@Override
	public Component getTreeCellRendererComponent(JTree tree, TreeCellRenderer treeCellRenderer,
			DefaultTreeCellRenderer label, boolean selected, boolean expanded, boolean leaf, int row,
			boolean hasFocus) {
		return null;
	}

	/**
	 * Add one child to the tree node.
	 * 
	 * @param child
	 *            Object to be added as child
	 * @param hasChilds
	 *            Whether the child is going to have children of its own
	 */
	@SuppressWarnings("rawtypes")
	public MutableObjectNode addChild(Comparable child, boolean hasChilds) {
		MutableObjectNode newNode = new MutableObjectNode(child, true, hasChilds);

		if (super.children == null) {
			super.add(newNode);

		} else {
			// Find the right index to add this child to
			int i;
			MutableObjectNode node;
			for (i = 0; i < super.children.size(); i++) {
				node = (MutableObjectNode) super.children.elementAt(i);
				if (node.compareTo(newNode) >= 0) {
					break;
				}
			}
			super.insert(newNode, i);
		}
		return newNode;
	}

	/**
	 * Add a listful of children to the tree node.
	 * 
	 * @param childs
	 *            List of children objects to be added
	 * @param hasChilds
	 *            Whether children are going to have children of their own
	 */
	public void addChildren(List<?> childs, boolean hasChilds) {
		for (Object child : childs) {
			MutableObjectNode newNode = new MutableObjectNode(child, true, hasChilds);
			super.add(newNode);
		}
	}

	/**
	 * Removes one child and its child nodes from the tree node.
	 * 
	 * @param child
	 *            A child object to be removed
	 */
	public void removeChild(Object child) {
		// Check that child belongs to this node
		if (getChildNode(child) == null)
			return;

		MutableObjectNode nodeToRemove = getChildNode(child);
		if (nodeToRemove.getChildCount() > 0)
			nodeToRemove.removeAllChildren();
		super.remove(nodeToRemove);
	}

	/** Find a child node that contains the given child object */
	public MutableObjectNode getChildNode(Object child) {
		if (super.children == null)
			return null;

		for (Object obj : super.children) {
			MutableObjectNode node = (MutableObjectNode) obj;
			Object object = node.getObject();
			if (object.equals(child))
				return node;
		}
		return null;
	}

	/**
	 * Updates a child object's place among children nodes.
	 * 
	 * @param child
	 *            The child object to move
	 */
	@SuppressWarnings("rawtypes")
	public void moveChild(Comparable child) {
		MutableObjectNode nodeToMove = getChildNode(child);
		super.remove(nodeToMove);

		// Find the right index to move this child to
		int i;
		MutableObjectNode node;
		for (i = 0; i < super.children.size(); i++) {
			node = (MutableObjectNode) super.children.elementAt(i);
			if (node == nodeToMove)
				continue;
			if (node.compareTo(nodeToMove) >= 0)
				break;
		}
		super.insert(nodeToMove, i);
	}

	@SuppressWarnings("rawtypes")
	@Override
	public int compareTo(MutableObjectNode o) {
		return ((Comparable) this.getObject()).compareTo(o.getObject());
	}
}
