package tree;

import javax.swing.JTree;
import javax.swing.tree.TreePath;

import name.lecaroz.java.swing.jocheckboxtree.ExtendedTreeTableModel;
import name.lecaroz.java.swing.sun.TreeTableModelAdapter;

public class CustomizedTreeTableModelAdapter<E> extends TreeTableModelAdapter<E> {

	/**
	 * Attribute required by Serializable interface, which is extended by
	 * TreeTableModelAdapter
	 */
	private static final long serialVersionUID = -6992066136464989350L;

	private final JTree tree;

	public CustomizedTreeTableModelAdapter(ExtendedTreeTableModel treeTableModel, JTree tree) {
		super(treeTableModel, tree);
		this.tree = tree;
		}

	@Override
	protected Object nodeForRow(int row) {
		TreePath treePath = tree.getPathForRow(row);
		if (treePath == null)
			return null;
		return treePath.getLastPathComponent();
	}
}
