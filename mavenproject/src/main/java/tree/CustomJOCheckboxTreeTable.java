package tree;

import name.lecaroz.java.swing.jocheckboxtree.ExtendedTreeTableModel;
import name.lecaroz.java.swing.sun.JOCheckboxTreeTable;

public class CustomJOCheckboxTreeTable<E> extends JOCheckboxTreeTable<E> {

	/** Attribute required by Serializable interface, which is extended by JOCheckboxTreeTable */
	private static final long serialVersionUID = 7457328719676117240L;

	public CustomJOCheckboxTreeTable(ExtendedTreeTableModel treeTableModel) {
		super(treeTableModel);
		super.setModel(new CustomizedTreeTableModelAdapter<>(treeTableModel, tree));
	}

}
