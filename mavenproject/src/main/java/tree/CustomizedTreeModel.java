package tree;

import java.awt.Component;
import java.awt.event.ActionListener;
import java.util.*;
import javax.swing.*;
import javax.swing.tree.TreeNode;

import domain.Analysis;
import domain.Customer;
import name.lecaroz.java.swing.jocheckboxtree.*;

public class CustomizedTreeModel extends ExtendedAbstractTreeTableModel implements ExtendedTreeTableModel {

	private final ResourceBundle messages;
	/** Names of the columns */
	private final String[] cNames;
	/** Types of the columns */
	private final Class<?>[] cTypes;
	private ActionListener popupMenuListener;

	public CustomizedTreeModel(Object root) {
		super(root);
		this.messages = ResourceBundle.getBundle("Messages", Locale.getDefault());
		this.cNames = new String[] { messages.getString("customer_tree_header"), messages.getString("date_tree_header") };
		this.cTypes = new Class[] { ExtendedTreeTableModel.class, Date.class };
	}

	@Override
	public int getColumnCount() {
		return cNames.length;
	}

	public String getColumnName(int column) {
		return cNames[column];
	}

	public Class<?> getColumnClass(int column) {
		return cTypes[column];
	}

	@Override
	public Object getValueAt(Object node, int column) {
		Object data = this.getObject(node);
        if (column == 1) {
            if (data == null)
                return "";
            if (data.getClass() == Analysis.class) {
                return ((Analysis) data).getDate();
            }
            return null;
        }
		return "";
	}

	@Override
	public Object getChild(Object parent, int index) {
		return ((TreeNode) parent).getChildAt(index);
	}

	@Override
	public int getChildCount(Object parent) {
		return ((TreeNode) parent).getChildCount();
	}

	@Override
	public int getIndexOfChild(Object parent, Object child) {
		TreeNode childNode = (TreeNode) child;
		return ((TreeNode) parent).getIndex(childNode);
	}

	@Override
	public String getTooltipAt(Object node, int column) {
		/*
		 * switch (column) { case 0: return "Tooltip for first column"; case 1:
		 * return "Tooltip for second column"; }
		 */
		return null;
	}

	@Override
	public Object getObject(Object object) {
		return ((TreeNodeObject) object).getObject();
	}

	@Override
	public void doubleClicked(Object object, int column) {
	}

	public void setPopupMenuListener(ActionListener listener) {
		this.popupMenuListener = listener;
	}

	@Override
	public void popupMenu(Object treeNodeObject, int column, Component component, int x, int y) {
		JPopupMenu menu = new JPopupMenu("Quick menu");

		// Define action for clicking on menu items
		ActionListener menuListener = event -> {
			// Notify listener
			if (popupMenuListener != null) {
				popupMenuListener.actionPerformed(event);
			}
		};

		if (treeNodeObject == null) {
			createBasicMenu(menu, menuListener);
		} else {
			MutableObjectNode node = (MutableObjectNode) treeNodeObject;

			// Create menu items depending on whether user clicks on customer or analysis
			Class<?> nodeClass = node.getObject().getClass();
			if (nodeClass == Customer.class) { // User clicked on Customer
				createCustomerMenu(menu, menuListener);
			} else if (nodeClass == Analysis.class) { // User clicked on Analysis
				createAnalysisMenu(menu, menuListener);
			} else {
				createBasicMenu(menu, menuListener);
			}
		}

		// menu.setBorder(new BevelBorder(BevelBorder.RAISED));
		menu.show(component, x, y);
	}

	/**
	 * Add only basic menu item to pop up menu.
	 * 
	 * @param menu
	 *            To which the items are added to
	 * @param listener
	 *            Actionlistener to every item
	 */
	private void createBasicMenu(JPopupMenu menu, ActionListener listener) {
		JMenuItem item = new JMenuItem(messages.getString("add_customer"));
		item.addActionListener(listener);
		menu.add(item);
	}

	/**
	 * Add customer related items to pop up menu.
	 * 
	 * @param menu
	 *            To which the items are added to
	 * @param listener
	 *            Actionlistener to every item
	 */
	private void createCustomerMenu(JPopupMenu menu, ActionListener listener) {
		JMenuItem item1 = new JMenuItem(messages.getString("add_analysis"));
		JMenuItem item2 = new JMenuItem(messages.getString("edit"));
		JMenuItem item3 = new JMenuItem(messages.getString("delete"));
		item1.addActionListener(listener);
		item2.addActionListener(listener);
		item3.addActionListener(listener);
		menu.add(item1);
		menu.add(item2);
		menu.add(item3);
	}

	/**
	 * Add analysis related items to pop up menu.
	 * 
	 * @param menu
	 *            To which the items are added to
	 * @param listener
	 *            Actionlistener to every item
	 */
	private void createAnalysisMenu(JPopupMenu menu, ActionListener listener) {
		JMenuItem item1 = new JMenuItem(messages.getString("edit"));
		JMenuItem item2 = new JMenuItem(messages.getString("delete"));
		item1.addActionListener(listener);
		item2.addActionListener(listener);
		menu.add(item1);
		menu.add(item2);
	}
}