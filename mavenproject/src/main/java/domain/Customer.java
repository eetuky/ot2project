package domain;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.StandardCopyOption;

import java.util.LinkedList;
import java.util.List;
import java.util.Collections;

import org.apache.commons.io.FileUtils;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

/**
 * Customer has a list of Analysis objects. User is able to add Analysis one by
 * one or as a list.
 * 
 * @author Juha Korvenaho
 */
public class Customer implements Comparable<Customer> {
	/** Name of file that data was read from */
	private File folder;
	private String strComment;
	private LinkedList<Analysis> analysisList;

	/**
	 * Constructor used when creating a new Customer.
	 * 
	 * @param folder
	 *            Customer Folder
	 * @param comment
	 */
	public Customer(File folder, String comment) {
		this.folder = folder;
		this.strComment = comment;
		this.analysisList = new LinkedList<>();
	}

	/**
	 * Saves customer folder and details to a json file in customer folder.
	 * 
	 * @throws IOException
	 */
	public void saveToFile() throws IOException {
		boolean folderCreated = this.folder.mkdir();
		if (folderCreated) {
			saveCustomerToJson(); // throws IOException
		} else {
			throw new IOException("Folder " + folder + " was not created");
		}
	}

	/**
	 * Constructor used when applications starts and reads the contents of working
	 * directory.
	 */
	public Customer(File file) {
		this.folder = file;
		this.strComment = null;
	}

	public File getFolder() {
		return this.folder;
	}

	/**
	 * Moves customer folder, in other words renames the folder.
	 * 
	 * @param newFolder
	 */
	public void setFolder(File newFolder) throws IOException {
		Files.move(this.folder.toPath(), newFolder.toPath(), StandardCopyOption.ATOMIC_MOVE);
		this.folder = newFolder;
		for (Analysis analysis : analysisList) {
			analysis.updateFolder(this.folder);
		}
	}

	/**
	 * Adds a single Analysis to Customer's analysis list. If parameter is null,
	 * nothing is added.
	 * 
	 * @param o
	 *            Analysis to be added
	 */
	public void addAnalysis(Analysis o) {
		if (this.analysisList == null) {
			this.analysisList = new LinkedList<>();
		}
		if (o != null) {
			this.analysisList.add(o);
		}
	}

	/**
	 * Adds a list of Analysis to Customer's analysis list. If parameter is null,
	 * nothing is added.
	 * 
	 * @param list
	 *            List of Analysis to be added
	 */
	public void addAnalysis(List<Analysis> list) {
		if (this.analysisList == null) {
			this.analysisList = new LinkedList<>();
		}
		if (list != null) {
			this.analysisList.addAll(list);
		}
	}

	/**
	 * @return list of all Analysis belonging to Customer
	 */
	public LinkedList<Analysis> getAnalysisList() {
		// If Analysis list was not yet created, read Analysis objects and add them
		if (this.analysisList == null) {
			this.analysisList = new LinkedList<>();
			for (File curDir : folder.listFiles()) {
				if (curDir.isDirectory()) {
					try {
						this.addAnalysis(new Analysis(curDir));
					} catch (IOException | ParseException e) {
						e.printStackTrace();
						System.out.println("Could not read analysis " + curDir);
					}
				}
			}
		}

		Collections.sort(analysisList);
		return this.analysisList;
	}

	/**
	 * @param comment
	 *            String to be set to comment
	 * @throws IOException
	 */
	public void setComment(String comment) throws IOException {
		if (comment == null)
			return;

		this.strComment = comment;
		saveCustomerToJson(); // throws IOException
	}

	/**
	 * Returns comment string. If it is null, tries to read it from json file.
	 * 
	 * @return Comment string
	 * @throws ParseException
	 * @throws IOException
	 */
	public String getComment() throws IOException, ParseException {
		if (this.strComment == null) {
			readComment();
		}
		return this.strComment;
	}

	/**
	 * Reads the Customer comment from the customer.json file to attribute.
	 */
	private void readComment() throws IOException, ParseException {
		String filePath = this.folder.getAbsolutePath() + File.separator + "customer.json";
		File customerJSON = new File(filePath);
		JSONParser parseJson = new JSONParser();

		try {
			FileReader readFile = new FileReader(customerJSON);
			Object parsedFile = parseJson.parse(readFile);
			JSONObject jsonFile = (JSONObject) parsedFile;
			this.strComment = (String) jsonFile.get("comment");
			readFile.close();
		} catch (FileNotFoundException e) {
			// If json file could not be found, create a new file
			System.out.println("File not found at path: " + filePath);
			saveCustomerToJson();
		} catch (IOException e) {
			e.printStackTrace();
		} catch (ParseException e) {
			e.printStackTrace();
		}
	}

	/**
	 * Returns name of this customer. It shows on the customer list on the directory
	 * view.
	 * @return
	 */
	public String toString() {
		return this.folder.getName();
	}

	/**
	 * Deletes the Customer directory and all files in it.
	 * 
	 * @throws IOException
	 */
	public void deleteFiles() throws IOException {
		System.out.println("Deleting Customer folder: " + this.folder.getAbsolutePath());
		FileUtils.deleteDirectory(this.folder);
	}

	/**
	 * Removes a single Analysis from Customer's Analysis list and deletes its files.
	 * 
	 * @param o
	 *            Analysis to be removed
	 * @throws IOException
	 */
	public void removeSingleAnalysis(Analysis o) throws IOException {
		o.deleteFiles();
		this.analysisList.remove(o);
	}

	/**
	 * Clears the whole Analysis list and deletes the files.
	 * 
	 * @throws IOException
	 */
	public void removeAllAnalysis() throws IOException {
		for (Analysis o : this.analysisList) {
			o.deleteFiles();
		}
		this.analysisList.clear();
	}

	/**
	 * Saves Customer to json file.
	 * 
	 * @throws IOException
	 */
	private void saveCustomerToJson() throws IOException {
		String filePath = this.folder.getAbsolutePath() + File.separator + "customer.json";
		System.out.println("Saving customer to file: " + filePath);

		FileWriter fileWriter = new FileWriter(filePath);
		BufferedWriter bufferedWriter = new BufferedWriter(fileWriter);

		try {
			String customerString = customerToJson();
			// Note that write() does not automatically.
			// append a newline character.
			bufferedWriter.write(customerString);
		} finally {
			// Always close files.
			bufferedWriter.close();
		}
	}

	/**
	 * Formats Customer attributes to json string.
	 * 
	 * @return Customer as json string
	 */
	@SuppressWarnings("unchecked")
	private String customerToJson() {
		if (this.strComment == null) {
			this.strComment = "";
		}
		JSONObject jsonCustomer = new JSONObject();
		jsonCustomer.put("comment", this.strComment);
		return jsonCustomer.toString();
	}

	@Override
	public int compareTo(Customer customer) {
		String customer1 = this.folder.getName().toLowerCase();
		String customer2 = customer.folder.getName().toLowerCase();
		return customer1.compareTo(customer2);
	}
}