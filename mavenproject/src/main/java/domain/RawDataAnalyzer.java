package domain;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Objects;
import java.util.Random;
import java.util.Scanner;
import java.util.regex.Pattern;



/**
 * Reads raw-data files and calculates positions
 * @author Eetu
 *
 */
public class RawDataAnalyzer {
	
	private final ArrayList<Double[]> coordList = new ArrayList<>();
	
	/**
	 * @return the coordList
	 */
	public ArrayList<Double[]> getCoordList() {
		return coordList;
	}

	/**
	 * List of rawDataLines
	 */
	private final ArrayList<Double[]> rawData = new ArrayList<>();
	
	/**
	 * @return raw data
	 */
	public ArrayList<Double[]> getRawData() {
		return rawData;
	}

	/**
	 * Create empty rawDataAnalyzer
	 */
	public RawDataAnalyzer() {
		
	}
	
	/**
	 * Reads the rawdata-file from a given path into the object
	 * Calculates positions (using random generator)
	 * @param filePath
	 */
	public RawDataAnalyzer(String filePath) throws IOException {
		if (!readFile(filePath)) throw new IOException("File " + filePath + " not supported by Syhero");
		calculatePosition();
	}

	/**
	 * TODO: Calculates position coordinates to coordList NOT IMPLEMENTED (uses randomGen)
	 * 
	 */
	private void calculatePosition() {
		Random randomGen= new Random();
		
		//The area of the created coordinates is affected by this (how large are the coordinates)
		Double RandomToCoordMultiplier = 0.25;
		
		//Gravity towards origo (multiplied by the previous coordinates and subtracted from lastRandom)
		Double toOrigo = 0.002;
		
		//The strength of the last Random when coordinates are calculated
		Double lastRandomStrength = 0.6;
		
		Double[] lastxyz = new Double[] {0.0, 0.0, 0.0, 0.0};
		Double[] lastRandom = new Double[] {0.0, 0.0, 0.0, 0.0};
		this.coordList.add(new Double[] {0.0, 0.0, 0.0, 0.0});	// first point
		for (Double[] rawDatum : this.rawData) {

			Double[] xyz = new Double[4];
			xyz[0] = rawDatum[0];

			// Move previous coordinate [-0.1, 0.1] mm
			Double currentRandomX = -0.5 + randomGen.nextDouble();
			Double currentRandomY = -0.5 + randomGen.nextDouble();
			//Double currentRandomZ = -0.5 + randomGen.nextDouble();

			xyz[1] = (currentRandomX + lastRandom[0]) * RandomToCoordMultiplier + lastxyz[1];
			xyz[2] = (currentRandomY + lastRandom[1]) * RandomToCoordMultiplier + lastxyz[2];
			xyz[3] = 0.0;

			lastRandom[0] = currentRandomX + lastRandom[0] * lastRandomStrength - toOrigo * xyz[1];
			lastRandom[1] = currentRandomY + lastRandom[1] * lastRandomStrength - toOrigo * xyz[2];
			lastxyz[1] = xyz[1];
			lastxyz[2] = xyz[2];
			//lastxyz[3] = xyz[3];

			this.coordList.add(xyz);
		}
	}
	/**
	 * Validates and reads file
	 * @param filePath
	 * returns List of rawdata lines. Null if file not acceptable
	 */
	private ArrayList<String> validateFile(String filePath) {
		
		// ATM Works with small strings but creates stackoverflow
		ArrayList<String> lineList = new ArrayList<>();
		ArrayList<String> returnList = new ArrayList<>();
		try {
			File file;
			if(Objects.equals(filePath, "mockup")) {
				file = new File("src/main/resources/lyhyt.txt");
			} else {
				file = new File(filePath);			
			}
			Scanner reader = new Scanner(file, "utf-8");
		
		
			//One line at a time
			while(reader.hasNextLine()) {
				lineList.add(reader.nextLine());
			}
			reader.close();		
			
			//Test headers
			/*
			System.out.println(Pattern.matches("BTS G-STUDIO File",lineList.get(0)));
			System.out.println(Pattern.matches("-+",lineList.get(2)));
			System.out.println(Pattern.matches("File Version:		\\d+",lineList.get(3)));
			System.out.println(Pattern.matches("Sensor type:		GS2",lineList.get(4)));
			System.out.println(Pattern.matches("Accelerometer \\(g\\):	±\\d",lineList.get(5)));
			System.out.println(Pattern.matches("Magnetometer \\(uT\\):	((OFF)|(ON))",lineList.get(6)));
			System.out.println(Pattern.matches("Gyroscope \\(°/s\\):	±\\d+",lineList.get(7)));
			System.out.println(Pattern.matches("Frequency \\(Hz\\):		\\d+",lineList.get(8)));
			System.out.println(Pattern.matches("GPS \\(Hz\\):		((OFF)|(ON))",lineList.get(9)));
			System.out.println(Pattern.matches("Information on sensor orientation available:	((OFF)|(ON))",lineList.get(10)));
			System.out.println(Pattern.matches("Frames:	\\d+",lineList.get(11)));
			System.out.println(Pattern.matches("-+",lineList.get(12)));
			System.out.println(Pattern.matches("",lineList.get(13)));
			System.out.println(Pattern.matches("Time		Acc.			Gyro.		Roll	Pitch	Yaw",lineList.get(14)));
			System.out.println(Pattern.matches("t\\(s\\)	X\\(m/s²\\)	Y\\(m/s²\\)	Z\\(m/s²\\)	X\\(°/s\\)	Y\\(°/s\\)	Z\\(°/s\\)	\\(°\\)	\\(°\\)	\\(°\\)",lineList.get(15)));
			*/
			
			//Find rawData starting point
			int start = 0;
			for (int i= 0; i<lineList.size();i++) {
				if (Pattern.matches("t\\(s\\)	X\\(m/s\u00B2\\)	Y\\(m/s\u00B2\\)	Z\\(m/s\u00B2\\)	X\\(\u00B0/s\\)	Y\\(\u00B0/s\\)	Z\\(\u00B0/s\\)	\\(\u00B0\\)	\\(\u00B0\\)	\\(\u00B0\\)",lineList.get(i))) {
					start = i+1;	
					break;
				}
			}
			if(start==0) return null;
			
			//Validate data-lines and put into another list for returning
			for(int i= start; i<lineList.size();i++) {
				if(!Pattern.matches("([+-]?\\d+(\\.\\d+)?	[+-]?\\d+(\\.\\d+)?	[+-]?\\d+(\\.\\d+)?	[+-]?\\d+(\\.\\d+)?	[+-]?\\d+(\\.\\d+)?	[+-]?\\d+(\\.\\d+)?	[+-]?\\d+(\\.\\d+)?	[+-]?\\d+(\\.\\d+)?	[+-]?\\d+(\\.\\d+)?	[+-]?\\d+(\\.\\d+)?)+",
						lineList.get(i))) {
					return null;
				} else {
					//System.out.println("Adding : "+lineList.get(i));
					returnList.add(lineList.get(i));
				}
			}		
			
		} catch(FileNotFoundException e) {
			System.out.println(e+"File not found");
		}
		
		return returnList;
	}

	
	/**
	 * Reads the given file into the this object
	 * @param pFilePath
	 * return true, if succesfull
	 */
	private boolean readFile(String pFilePath) {
		
		ArrayList<String> lineList = this.validateFile(pFilePath);
		if (lineList == null) return false;				
			//Iterate lines
		for (String s : lineList) {
			String[] lineArr = s.split("	");

			Double[] doubleArray = new Double[lineArr.length];

			//Parse String-array to Double-array
			for (int j = 0; j < lineArr.length; j++) {
				doubleArray[j] = Double.parseDouble(lineArr[j]);
			}

			//if last row is empty
			if (lineArr.length > 1) {
				this.rawData.add(doubleArray);
			}
		}
				
		return true;
	}
}
