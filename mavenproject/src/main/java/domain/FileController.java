package domain;

import java.io.File;
import java.io.IOException;
import java.util.Collections;
import java.util.LinkedList;

import org.json.simple.parser.ParseException;
/**
 * FileController is used to pull all the customers from the working directory, turn them into customer-objects and
 * return them as a LinkedList.
 * 
 * @author Teemu Savorinen
 */
public class FileController {
	private File workDir;
	
	/**
	 * Constructor.
	 * 
	 * @param workDir 
	 * 			Path to the working directory
	 */
	public FileController(File workDir) {
		this.workDir = workDir;
	}
	
	/**
	 * pullCustomers is used to pull the customers from the working directory, turn them into customer-objects and
	 * return them as a LinkedList.
	 * 
	 * @return LinkedList<Customer> A LinkedList which includes customers as a customer-objects
	 */
	public LinkedList<Customer> pullCustomers() throws IOException, ParseException {
		LinkedList<Customer> customers = new LinkedList<>();
		
		// Creating Customer objects and adding them into the LinkedList
		System.out.print("Creating customers from working directory... ");
		for(File curDir : workDir.listFiles()) {
			if(curDir.isDirectory()) {
				customers.add(new Customer(curDir));
			}
		}
		System.out.println("Created");

		System.out.print("Sorting customers... ");
		Collections.sort(customers);
		System.out.println("Sorted");

		return customers;
	}

	/**
	 * updateWorkDirPath is used to change the work directory inside FileController.
	 * 
	 * @param workDir
	 * 			New path
	 */
	public void updateWorkDirPath(File workDir) {
		this.workDir = workDir;
	}
}