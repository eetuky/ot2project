package domain;

import java.awt.*;
import java.awt.geom.Ellipse2D;
import java.awt.geom.Line2D;
import java.awt.image.BufferedImage;
import java.awt.image.RenderedImage;
import java.io.*;
import java.math.*;
import java.nio.file.Files;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.text.ParseException;
import javax.imageio.ImageIO;
import org.apache.commons.io.FileUtils;
import org.apache.commons.math3.stat.descriptive.moment.StandardDeviation;

import java.util.*;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;

import com.opencsv.CSVReader;

import util.Stance;

/**
 * @author Kimi, Eetu
 */
public class Analysis implements Comparable<Analysis> {

	private String eyes;
	private Stance stance;
	private String measurer;
	private Double medianX;
	private Double medianY;
	private String comments;
	private Date date;
	private Double amplitudeAPmax;
	private Double amplitudeAPavg;
	private Double amplitudeMLmax;
	private Double amplitudeMLavg;
	private Double frequencyAPavg;
	private Double frequencyMLavg;
	/** Travel = sum of the distances of consecutive points in coordinate list. */
	private Double travelStat;
	private Double duration;

	/** The folder that the object is saved into. */
	private File folder;

	/**
	 * List of coordinate points [time, x, y, z] in millimeters. x is medial lateral
	 * and y anterior posterior direction. Positive value = anterior / to the right.
	 */
	private ArrayList<Double[]> coordList;

	/**
	 * Reads data from the folder.
	 * 
	 * @param folder
	 *            Folder to read data from
	 * @throws IOException IOException
	 * @throws org.json.simple.parser.ParseException ParseException
	 */
	public Analysis(File folder) throws IOException, org.json.simple.parser.ParseException {
		this.folder = folder;
		this.coordList = new ArrayList<>();
		readFromJson(this.folder);
	}

	/**
	 *
	 * @param folder
	 * @param date
	 * @param stance
	 * @param eyes
	 * @param coordList
	 * @param comment
	 * @param measurer
	 * @throws IOException
	 */
	public Analysis(File folder, Date date, Stance stance, String eyes, ArrayList<Double[]> coordList, String comment,
			String measurer) throws IOException {
		this.folder = folder;
		this.date = date;
		this.measurer = measurer;
		this.setStance(stance);
		this.eyes = eyes;
		this.coordList = coordList;
		this.comments = comment;

		double lastCoordTime = this.coordList.get(this.coordList.size() - 1)[0];
		this.duration = Math.round(lastCoordTime * 10) / 10.0;

		calculateStats();
	}

	/**
	 * Moves analysis folder, in other words renames the folder.
	 * @param newParentFolder
	 * @throws IOException
	 */
	public void updateFolder(File newParentFolder) throws IOException {
		String folderName = this.folder.getName();
		this.folder = new File(newParentFolder.getAbsolutePath() + File.separator + folderName);
	}

	/**
	 * Calculates stats from values in coordinate list and sets them in private
	 * attributes.
	 */
	private void calculateStats() {
		int decimals = 1;

		// for travel
		Double sumOfDistances = 0.0;

		// for frequency
		boolean xPositive = false;
		boolean yPositive = false;
		int xCount = 0;
		int yCount = 0;

		// for amplitude
		calculateMedians();
		double[] arrStrDevX = new double[coordList.size()];
		double[] arrStrDevY = new double[coordList.size()];
		Double[] firstPoints = this.coordList.get(0);

		// variables for x axis
		Double minX = firstPoints[1];
		Double maxX = firstPoints[1];

		// variables for Y axis
		Double minY = firstPoints[2];
		Double maxY = firstPoints[2];

		for (int i = 1; i < this.coordList.size(); i++) {
			// travel
			Double[] currentPoint = this.coordList.get(i);
			Double[] previousPoint = this.coordList.get(i - 1);
			double currX = currentPoint[1];
			double currY = currentPoint[2];
			double prevX = previousPoint[1];
			double prevY = previousPoint[2];
			double distX = Math.abs(currX - prevX);
			double distY = Math.abs(currY - prevY);
			sumOfDistances += Math.hypot(distX, distY);

			// frequency
			if (xPositive) {
				if (previousPoint[1] > currentPoint[1]) {
					xPositive = false;
					xCount++;
				}
			} else {
				if (previousPoint[1] <= currentPoint[1]) {
					xPositive = true;
					xCount++;
				}
			}

			if (yPositive) {
				if (previousPoint[2] > currentPoint[2]) {
					yPositive = false;
					yCount++;
				}
			} else {
				if (previousPoint[2] <= currentPoint[2]) {
					yPositive = true;
					yCount++;
				}
			}

			// amplitude
			arrStrDevX[i - 1] = currentPoint[1];
			if (currentPoint[1] > maxX) {
				maxX = currentPoint[1];
			}
			if (currentPoint[1] < minX) {
				minX = currentPoint[1];
			}

			arrStrDevY[i - 1] = currentPoint[2];
			if (currentPoint[2] > maxY) {
				maxY = currentPoint[2];
			}
			if (currentPoint[2] < minY) {
				minY = currentPoint[2];
			}
		}

		// travel
		BigDecimal bd = new BigDecimal(Double.toString(sumOfDistances));
		bd = bd.setScale(decimals, RoundingMode.HALF_UP); // Round to one decimal
		this.travelStat = bd.doubleValue();

		// frequency
		double lastTime = this.coordList.get(this.coordList.size() - 1)[0];
		this.frequencyAPavg = new BigDecimal(Double.toString(xCount / lastTime))
				.setScale(decimals, RoundingMode.HALF_UP).doubleValue();
		this.frequencyMLavg = new BigDecimal(Double.toString(yCount / lastTime))
				.setScale(decimals, RoundingMode.HALF_UP).doubleValue();

		// amplitude max
		Double AP = maxY - minY;
		Double ML = maxX - minX;

		BigDecimal maxAP = new BigDecimal(Double.toString(AP));
		maxAP = maxAP.setScale(decimals, RoundingMode.HALF_UP);
		this.amplitudeAPmax = maxAP.doubleValue();

		BigDecimal maxML = new BigDecimal(Double.toString(ML));
		maxML = maxML.setScale(decimals, RoundingMode.HALF_UP);
		this.amplitudeMLmax = maxML.doubleValue();

		// amplitude average
		StandardDeviation sd = new StandardDeviation();
		double dblStrDevX = sd.evaluate(arrStrDevX, this.medianX);
		double dblStrDevY = sd.evaluate(arrStrDevY, this.medianY);

		BigDecimal APavg = new BigDecimal(Double.toString(dblStrDevY));
		APavg = APavg.setScale(decimals, RoundingMode.HALF_UP);
		this.amplitudeAPavg = APavg.doubleValue();

		BigDecimal MLavg = new BigDecimal(Double.toString(dblStrDevX));
		MLavg = MLavg.setScale(decimals, RoundingMode.HALF_UP);
		this.amplitudeMLavg = MLavg.doubleValue();
		System.out.println(APavg);
		System.out.println(MLavg);
	}

	/**
	 * Returns description of this analysis. It shows on the directory view.
	 */
	@Override
	public String toString() {
		return this.stance.getStance() + ", " + this.eyes;
	}

	/**
	 * Deletes old folder and saves the object into given folder
	 * @param folder
	 * @return
	 * @throws IOException
	 */
	public boolean moveTo(File folder) throws IOException {
		deleteFiles();
		this.folder = folder;
		saveToFile();
		return true;
	}

	/**
	 * Deletes the directory in folder/file-variable
	 * @return true
	 * @throws IOException
	 */
	public boolean deleteFiles() throws IOException {
		System.out.println("Deleting Analysis folder: " + this.folder);
		// System.out.println("Deleting Analysis folder: "+this.file.getAbsolutePath());
		FileUtils.deleteDirectory(this.folder);
		return true;
	}

	/**
	 * Calculate medians of the first 2 coordinates into medianX and medianY
	 */
	private void calculateMedians() {
		ArrayList<Double[]> listOfDoubleArrays = new ArrayList<>(this.coordList);

		int middle = coordList.size() / 2;

		listOfDoubleArrays.sort(Comparator.comparing(doubles -> doubles[1]));

		this.medianX = listOfDoubleArrays.get(middle)[1];
		// System.out.println("Median X = "+this.medianX);

		listOfDoubleArrays.sort(Comparator.comparing(doubles -> doubles[2]));

		this.medianY = listOfDoubleArrays.get(middle)[2];
		// System.out.println("Median Y = "+this.medianY);
	}

	/**
	 * Draws chart for the report showing the path of the sensor
	 * 
	 * @return BufferedImage
	 */
	private BufferedImage drawChart() {
		calculateMedians();
		System.out.println("medians: " + medianX + ", " + medianY);
		// maxX = 2cm
		// maxY = 2cm

		Double widthOfImage = 500.0;
		Double heightOfImage = 500.0;
		double widthMm = 20.0;
		double heightMm = 20.0;
		int sizeOfPoints = (int) Math.round(widthOfImage * 0.014);

		double margPercent = 0.25;
		int circleStroke = 4;
		int lineStroke = 2;

		BufferedImage buffImg = new BufferedImage(widthOfImage.intValue(), heightOfImage.intValue(),
				BufferedImage.TYPE_INT_RGB);

		Graphics2D g = buffImg.createGraphics();
		g.setColor(Color.WHITE);
		g.setBackground(Color.WHITE);

		g.setColor(Color.WHITE);
		Shape back = new Rectangle(0, 0, heightOfImage.intValue(), widthOfImage.intValue());
		g.fill(back);
		g.draw(back);

		// Draw coordinates
		g.setPaint(stance.getColor());

		for (Double[] point : coordList) {

			double x = point[1];
			double y = point[2];

			// Transform coordinates to image pixels. Origo is in (medianX, medianY)
			double xInPixels = (x - medianX + widthMm / 2) / widthMm * widthOfImage;
			double yInPixels = heightOfImage - (y - medianY + heightMm / 2) / heightMm * heightOfImage;

			int xLeft = (int) (xInPixels - sizeOfPoints / 2);
			int yTop = (int) (yInPixels - sizeOfPoints / 2);
			g.fillOval(xLeft, yTop, sizeOfPoints, sizeOfPoints);
		}

		// Draw Circle
		g.setStroke(new BasicStroke(circleStroke, BasicStroke.CAP_BUTT, BasicStroke.JOIN_BEVEL));
		g.setColor(Color.BLACK);

		Shape circle = new Ellipse2D.Double(heightOfImage * margPercent, widthOfImage * margPercent,
				heightOfImage - heightOfImage * margPercent * 2, widthOfImage - widthOfImage * margPercent * 2);
		g.draw(circle);

		// draw lines
		g.setStroke(new BasicStroke(lineStroke, BasicStroke.CAP_BUTT, BasicStroke.JOIN_BEVEL));
		Shape vertLine = new Line2D.Double(widthOfImage / 2, heightOfImage * margPercent, widthOfImage / 2,
				heightOfImage - heightOfImage * margPercent);
		g.draw(vertLine);

		Shape horLine = new Line2D.Double(widthOfImage * margPercent, heightOfImage / 2,
				widthOfImage - widthOfImage * margPercent, heightOfImage / 2);
		g.draw(horLine);

		g.dispose();

		return buffImg;
	}

	/**
	 * This will draw a chart for the report into a file. File will be put to the
	 * location specified by folder with filename /graph.png
	 */
	public void drawChartToFile() throws IOException {
		if (this.coordList.size() == 0) {
			coordList = new ArrayList<>();
			readFromCsv();
		}

		RenderedImage rendImage = drawChart();

		// Save to file
		File graphFile = new File(this.folder.getAbsolutePath() + File.separator + "graph.png");
		System.out.println("Saving graph to file : " + graphFile.getAbsolutePath());
		ImageIO.write(rendImage, "png", graphFile);
	}

	/**
	 * Reads the csv file into coordinates if required for example when drawing new
	 * graph.
	 * 
	 * @return true
	 * @throws IOException problem with reading
	 */
	private boolean readFromCsv() throws IOException {
		File csvFile = new File(this.folder.getAbsolutePath() + File.separator + "coordinates.csv");
		System.out.println("Reading coordinates from csv: " + csvFile.getAbsolutePath());
		try (Reader reader = Files.newBufferedReader(csvFile.toPath()); CSVReader csvReader = new CSVReader(reader);) {
			String[] nextRecord;
			Double[] array;
			csvReader.readNext();
			while ((nextRecord = csvReader.readNext()) != null) {
				array = new Double[4];
				array[0] = Double.parseDouble(nextRecord[0]);
				array[1] = Double.parseDouble(nextRecord[1]);
				array[2] = Double.parseDouble(nextRecord[2]);
				array[3] = Double.parseDouble(nextRecord[3]);
				this.coordList.add(array);
			}
		}
		return true;
	}

	/**
	 * Reads JSON file into this object.
	 * @param folder Directory of the details.json -file
	 * @throws IOException problem with reading
	 * @throws org.json.simple.parser.ParseException problem with parsing
	 */
	private void readFromJson(File folder) throws IOException, org.json.simple.parser.ParseException {
		
		File jsonFile = new File(folder.getAbsolutePath() + File.separator + "details.json");
		System.out.println("Trying to read file " + jsonFile);
		FileReader readFile = new FileReader(jsonFile);
		JSONObject jsonObj = (JSONObject) new JSONParser().parse(readFile);

		boolean valuesReadSuccessfully = true;

		// Read values given by user
		this.measurer = jsonObj.get("measurer") == null ? "" : jsonObj.get("measurer").toString();
		this.eyes = jsonObj.get("eyes") == null ? "" : jsonObj.get("eyes").toString();
		String staStr = jsonObj.get("stance").toString();
		this.setStance(new Stance(staStr));
		this.comments = jsonObj.get("comment") == null ? null : jsonObj.get("comment").toString();
		String dateStr = jsonObj.get("date") == null ? null : jsonObj.get("date").toString();

		// Parse date
		DateFormat df = new SimpleDateFormat("yyyy-MM-dd");
		try {
			this.date = df.parse(dateStr);
		} catch (ParseException | NullPointerException e) {
			System.out.println("Date in the .json was not valid : " + folder.getAbsolutePath());
			this.date = new Date();
		}

		if (this.comments == null) {
			valuesReadSuccessfully = false;
			this.comments = "";
		}
		if (this.measurer.length() == 0 || this.eyes.length() == 0 || staStr.length() == 0 || dateStr == null) {
			valuesReadSuccessfully = false;
		}

		// Read calculated stats values
		try {
			String travelStr = jsonObj.get("travel").toString();
			this.travelStat = Double.valueOf(travelStr);

			String durationStr = jsonObj.get("duration").toString();
			this.duration = Double.valueOf(durationStr);

			String frequency_AP = jsonObj.get("frequency_AP").toString();
			this.frequencyAPavg = Double.valueOf(frequency_AP);

			String frequency_ML = jsonObj.get("frequency_ML").toString();
			this.frequencyMLavg = Double.valueOf(frequency_ML);

			String amplitude_AP_max = jsonObj.get("amplitude_AP_max").toString();
			this.amplitudeAPmax = Double.valueOf(amplitude_AP_max);

			String amplitude_AP_avg = jsonObj.get("amplitude_AP_avg").toString();
			this.amplitudeAPavg = Double.valueOf(amplitude_AP_avg);

			String amplitude_ML_max = jsonObj.get("amplitude_ML_max").toString();
			this.amplitudeMLmax = Double.valueOf(amplitude_ML_max);

			String amplitude_ML_avg = jsonObj.get("amplitude_ML_avg").toString();
			this.amplitudeMLavg = Double.valueOf(amplitude_ML_avg);

		} catch (NullPointerException e) {
			valuesReadSuccessfully = false;
			readFromCsv();
			calculateStats();
		}

		readFile.close();

		if (!valuesReadSuccessfully) {
			saveDetailsJson();
		}
	}

	/**
	 * Saves coordinates to csv file
	 *
	 * @return
	 * @throws IOException
	 */
	private boolean saveCoordCsv() throws IOException {
		FileWriter fileWriter;
		BufferedWriter bufferedWriter;

		// String fileName = "coordinates.csv";
		String filePath = this.folder.getAbsolutePath() + File.separator + "coordinates.csv";
		// String fileName = new SimpleDateFormat("HHddMMyyyy'.txt'").format(new
		// Date());
		System.out.println("Saving coordinates to file: " + filePath);
		
		// Assume default encoding.
		fileWriter = new FileWriter(filePath);

		// Always wrap FileWriter in BufferedWriter.
		bufferedWriter = new BufferedWriter(fileWriter);

		String analysisstring = coordToCsv();
		// Note that write() does not automatically.
		// append a newline character.
		bufferedWriter.write(analysisstring);

		// Always close files.
		bufferedWriter.close();
		return true;
	}

	/**
	 * Saves details to json file
	 * 
	 * @throws IOException
	 */
	public void saveDetailsJson() throws IOException {
		String filePath = this.folder.getAbsolutePath() + File.separator + "details.json";
		System.out.println("Saving details to file: " + filePath);

		// Assume default encoding.
		FileWriter fileWriter = new FileWriter(filePath);

		// Always wrap FileWriter in BufferedWriter.
		BufferedWriter bufferedWriter = new BufferedWriter(fileWriter);

		try {
			String detailsString = detailsToJson();
			// Note that write() does not automatically.
			// append a newline character.
			bufferedWriter.write(detailsString);

		} finally {
			// Always close files.
			bufferedWriter.close();
		}
	}

	/**
	 * Saves json, csv and png files that are associated with this analysis.
	 * 
	 * @throws IOException
	 */
	public void saveToFile() throws IOException {
		saveDetailsJson();
		saveCoordCsv();
		drawChartToFile();
	}

	/**
	 * Writes the coordinatelist to csv String
	 * 
	 * @return csv-String
	 */
	private String coordToCsv() {
		StringBuilder str = new StringBuilder();
		str.append("time,x,y,z");
		str.append(System.getProperty("line.separator"));
		for (int i = 0; i < coordList.size() - 1; i++) {
			Double[] tempf = coordList.get(i);
			str.append(tempf[0]).append(",").append(tempf[1]).append(",").append(tempf[2]).append(",").append(tempf[3]).append(System.getProperty("line.separator"));
		}
		return str.toString();
	}

	/**
	 * Writes the object details to json-string
	 * 
	 * @return json-string
	 */
	@SuppressWarnings("unchecked")
	private String detailsToJson() {
		JSONObject details = new JSONObject();
		if (this.date != null) {
			DateFormat df = new SimpleDateFormat("yyyy-MM-dd");
			details.put("date", df.format(this.date));
		} else {
			details.put("date", "");
		}
		details.put("measurer", measurer);
		details.put("eyes", eyes);
		details.put("stance", this.stance.getStance());
		details.put("comment", this.comments);
		details.put("duration", this.duration);
		// details.put("median_x", this.medianX);
		// details.put("median_y", this.medianY);
		details.put("travel", this.travelStat);
		details.put("frequency_AP", this.frequencyAPavg);
		details.put("frequency_ML", this.frequencyMLavg);
		details.put("amplitude_AP_max", this.amplitudeAPmax);
		details.put("amplitude_AP_avg", this.amplitudeAPavg);
		details.put("amplitude_ML_max", this.amplitudeMLmax);
		details.put("amplitude_ML_avg", this.amplitudeMLavg);

		return details.toString();
	}

	@Override
	public int compareTo(Analysis sortAnalysis) {

		int datecheck = date.compareTo(sortAnalysis.date);

		if (datecheck == 0) {
			int stancecheck = stance.getStance().compareTo(sortAnalysis.stance.getStance());
			if (stancecheck == 0) {
				return eyes.compareTo(sortAnalysis.eyes);
			} else {
				return stancecheck;
			}
		} else {
			return -datecheck;
		}

	}

	// SETTERIT

	public void setDate(Date date) {
		this.date = date;
	}

	public void setStance(Stance stance) {
		this.stance = stance;
	}

	public void setEyes(String eyes) {
		this.eyes = eyes;
	}

	public void setcomments(String comments) {
		this.comments = comments;
	}

	public void setMeasurer(String measurer) {
		this.measurer = measurer;
	}

	public Date getDate() {
		return this.date;
	}

	public Stance getStance() {
		return this.stance;
	}

	public String getEyes() {
		return this.eyes;
	}

	public String getcomments() {
		return comments;
	}

	public ArrayList<Double[]> getCoordList() {
		return coordList;
	}

	public double getAmplitudeAPmax() {
		return amplitudeAPmax;
	}

	public double getAmplitudeAPavg() {
		return amplitudeAPavg;
	}

	public double getAmplitudeMLmax() {
		return amplitudeMLmax;
	}

	public double getAmplitudeMLavg() {
		return amplitudeMLavg;
	}

	public double getFrequencyAPavg() {
		return frequencyAPavg;
	}

	public double getFrequencyMLavg() {
		return frequencyMLavg;
	}

	public double getTravel() {
		return travelStat;
	}

	public double getDuration() {
		return duration;
	}

	public String getMeasurer() {
		return measurer;
	}

	/**
	 * 
	 * @return the index of the current stance in dropdown menu
	 */
	public int getStanceIndex() {
		return this.stance.getIndex();
	}

	/**
	 * 
	 * @return Eyes index in dropdown menu
	 */
	public int getEyesIndex() {
		return getEyesIndex(this.eyes);
	}

	/**
	 * 
	 * @return Eyes index in dropdown menu
	 */
	public static int getEyesIndex(String peyes) {
		if (peyes != null) {
			switch (peyes) {
			case "1_eyes_open":
				return 1;
			case "2_eyes_closed":
				return 2;
			}
		}
		return 0;
	}

	/**
	 * Get eyes-String by index
	 * 
	 * @param peyes
	 * @return eyes
	 */
	public static String getEyesByIndex(int peyes) {
		switch (peyes) {
		case 1:
			return "1_eyes_open";
		case 2:
			return "2_eyes_closed";
		}
		return "0_choose";
	}

	/**
	 * Get File variable for graph.
	 * 
	 * @return
	 */
	public File getImageFile() {
		return new File(this.folder.getAbsolutePath() + File.separatorChar + "graph.png");
	}

	/**
	 * Get File variable for details
	 * 
	 * @return
	 */
	public File getJsonFile() {
		return new File(this.folder.getAbsolutePath() + File.separatorChar + "details.png");
	}

}
