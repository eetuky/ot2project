package main;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.util.LinkedList;
import java.util.Locale;
import java.util.concurrent.atomic.AtomicBoolean;

import javax.imageio.ImageIO;
import javax.swing.ImageIcon;
import javax.swing.UIManager;

import org.json.simple.parser.ParseException;

import gui.ApplicationWindow;
import gui.SplashScreen;
import domain.Customer;
import domain.FileController;

/**
 * 
 * @author Eetu Kyyrö & Maarit Ryynänen School of Computing, University of
 *         Eastern Finland
 */
public class Main implements Runnable {

	private static final AtomicBoolean showGui = new AtomicBoolean(false);

	/**
	 * Entry point of application. Initializes Customer objects and runs graphical
	 * user interface.
	 * 
	 * @param args
	 */
	public static void main(String[] args) {

		Runnable main = new Main();
		Thread syhero = new Thread(main);
		syhero.start();

		SplashScreen splash = new SplashScreen(2000, showGui);
		splash.showSplash();
	}

	public void run() {

		/* SET APPLICATION SETTINGS */

		String language = "fi";
		String country = "FI";
		Locale.setDefault(new Locale(language, country));

		String workDir = System.getProperty("user.home") + File.separatorChar + "syhero-workspace";
		File workingDirectory = new File(workDir);

		if (!workingDirectory.exists()) {
			System.out.println("Created working directory at " + workDir);
			workingDirectory.mkdir();
		}

		// Set expand/collapse handle icons for directory tree
		ImageIcon minusIcon = null;
		ImageIcon plusIcon = null;
		try (InputStream minusStream = Main.class.getResourceAsStream("/images/treeMinus.gif")) {
			minusIcon = new ImageIcon(ImageIO.read(minusStream));
		} catch (IOException e) {
			e.printStackTrace();
		}
		try (InputStream plusStream = Main.class.getResourceAsStream("/images/treePlus.gif")) {
			plusIcon = new ImageIcon(ImageIO.read(plusStream));
		} catch (IOException e) {
			e.printStackTrace();
		}
		if (minusIcon != null && plusIcon != null) {
			UIManager.put("Tree.expandedIcon", minusIcon);
			UIManager.put("Tree.collapsedIcon", plusIcon);
		}

		UIManager.put("Tree.rowHeight", 30);

		/* START APPLICATION */
		System.out.println("Starting application.");

		System.out.println("Creating customer list.");
		LinkedList<Customer> customerList = new LinkedList<>();

		// Read file names in the default working directory into a list of Customers
		FileController dirReader = new FileController(workingDirectory);
		try {
			customerList = dirReader.pullCustomers();
		} catch (IOException e) {
			e.printStackTrace();
		} catch (ParseException e) {
			e.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		}
		System.out.println("Working directory (" + workingDirectory.getAbsolutePath() + ") contains: " + customerList);

		// Run graphical user interface
		ApplicationWindow gui = new ApplicationWindow(workingDirectory);
		gui.runGUI(customerList, showGui);
	}
}
