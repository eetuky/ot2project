package util;

import java.awt.Color;

public class Stance {
	
	public final static Stance COMFORTABLE = new Stance("1_comfortable_stance",1,new Color(0,221,209));
	public final static Stance PARALLEL = new Stance("2_parallel_stance",2,new Color(87,193,0));
	public final static Stance SEMI_TANDEM = new Stance("3_semi-tandem_stance",3,new Color(206,71,211));
	public final static Stance TANDEM = new Stance("4_tandem_stance",4,new Color(224,62,87));
	public final static Stance ONE_LEGGED_LEFT = new Stance("5_one-legged_stance_left",5,new Color(0,115,247));
	public final static Stance ONE_LEGGED_RIGHT = new Stance("6_one-legged_stance_right",6,new Color(255,127,0));
	
	private String stance;
	private int index;
	private Color color;
	
	
	/**
	 * Creates new instance of Stance
	 * @param stance
	 * @param index
	 * @param color
	 */
	private Stance(String stance, int index, Color color) {
		this.setStance(stance);
		this.setIndex(index);
		this.setColor(color);
	}
	
	/**
	 * Creates an instance of Stance based on a stance-String
	 * for example when reading from json.
	 * @param stance
	 */
	public Stance(String stance) {
		this.setStance(stance);
		System.out.println("Got stance"+stance);
		if (stance.equals(Stance.COMFORTABLE.getStance())) {
			this.setIndex(Stance.COMFORTABLE.getIndex());
			this.setColor(Stance.COMFORTABLE.getColor());
		} else if (stance.equals(Stance.PARALLEL.getStance())) {
			this.setIndex(Stance.PARALLEL.getIndex());
			this.setColor(Stance.PARALLEL.getColor());
		} else if (stance.equals(Stance.SEMI_TANDEM.getStance())) {
			this.setIndex(Stance.SEMI_TANDEM.getIndex());
			this.setColor(Stance.SEMI_TANDEM.getColor());
		} else if (stance.equals(Stance.TANDEM.getStance())) {
			this.setIndex(Stance.TANDEM.getIndex());
			this.setColor(Stance.TANDEM.getColor());
		} else if (stance.equals(Stance.ONE_LEGGED_LEFT.getStance())) {
			this.setIndex(Stance.ONE_LEGGED_LEFT.getIndex());
			this.setColor(Stance.ONE_LEGGED_LEFT.getColor());
		} else if (stance.equals(Stance.ONE_LEGGED_RIGHT.getStance())) {
			this.setIndex(Stance.ONE_LEGGED_RIGHT.getIndex());
			this.setColor(Stance.ONE_LEGGED_RIGHT.getColor());
		}
	}
	
	/**
	 * Creates an instance of Stance base on index
	 * for example when chosen on modify or add analysis
	 * @param index
	 */
	public Stance(int index) {
		this.setIndex(index);
		if (index==Stance.COMFORTABLE.getIndex()) {
			this.setStance(Stance.COMFORTABLE.getStance());
			this.setColor(Stance.COMFORTABLE.getColor());
		} else if (index==Stance.PARALLEL.getIndex()) {
			this.setStance(Stance.PARALLEL.getStance());
			this.setColor(Stance.PARALLEL.getColor());
		} else if (index==Stance.SEMI_TANDEM.getIndex()) {
			this.setStance(Stance.SEMI_TANDEM.getStance());
			this.setColor(Stance.SEMI_TANDEM.getColor());
		} else if (index==Stance.TANDEM.getIndex()) {
			this.setStance(Stance.TANDEM.getStance());
			this.setColor(Stance.TANDEM.getColor());
		} else if (index==Stance.ONE_LEGGED_LEFT.getIndex()) {
			this.setStance(Stance.ONE_LEGGED_LEFT.getStance());
			this.setColor(Stance.ONE_LEGGED_LEFT.getColor());
		} else if (index==Stance.ONE_LEGGED_RIGHT.getIndex()) {
			this.setStance(Stance.ONE_LEGGED_RIGHT.getStance());
			this.setColor(Stance.ONE_LEGGED_RIGHT.getColor());
		}
	}
	
	
	public String getStance() {
		return this.stance;
	}
	
	@Override
	public String toString() {
		return this.stance;
	}

	private void setStance(String stance) {
		this.stance = stance;
	}

	public int getIndex() {
		return index;
	}

	private void setIndex(int index) {
		this.index = index;
	}

	public Color getColor() {
		return color;
	}

	private void setColor(Color color) {
		this.color = color;
	}
	
	
}
