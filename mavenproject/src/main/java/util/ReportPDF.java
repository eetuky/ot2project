package util;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.net.MalformedURLException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.*;

import javax.swing.JFileChooser;
import javax.swing.JOptionPane;
import javax.swing.filechooser.FileFilter;

import com.itextpdf.text.BadElementException;
import com.itextpdf.text.BaseColor;
import com.itextpdf.text.Chunk;
import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Element;
import com.itextpdf.text.FontFactory;
import com.itextpdf.text.Image;
import com.itextpdf.text.PageSize;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.Rectangle;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.pdf.PdfPageEventHelper;
import com.itextpdf.text.pdf.PdfWriter;

import gui.AnalysisBox;
import gui.ApplicationWindow;

/**
 * Class for creating pdf from selected reports
 * 
 * @author Juha Korvenaho Partly copied from work of Maarit Ryyn�nen and Heikki
 *         Hiltunen
 */
public class ReportPDF {
	private static ApplicationWindow app;
	private static ResourceBundle messagesBundle;
	private static String topic;
	private static File imagePath;
	private static String customerID;
	private static String stance;
	private static String eyes;
	private static Date dtDate;
	private static String measurer;
	private static String analysisComments;
	private static String statsTopic;
	private static String amplitudeAPmaxTitle;
	private static Double dblAmplitudeAPmax;
	private static Double dblAmplitudeAPavg;
	private static String amplitudeAPavgTitle;
	private static Double dblAmplitudeMLmax;
	private static String amplitudeMLmaxTitle;
	private static Double dblAmplitudeMLavg;
	private static String amplitudeMLavgTitle;
	private static Double dblFrequencyAPavg;
	private static String frequencyAPavgTitle;
	private static Double dblFrequencyMLavg;
	private static String frequencyMLavgTitle;
	private static Double dblTravelStat;
	private static String travelTitle;
	private static Double dblDuration;
	private static String durationTitle;
	private static final DateFormat df = new SimpleDateFormat("dd.MM.yyyy");

	public static String createReport(ApplicationWindow app, LinkedList<AnalysisBox> boxList) {
		int i = 0;
		ReportPDF.app = app;
		ReportPDF.messagesBundle = ResourceBundle.getBundle("Messages", Locale.getDefault());
		ReportPDF.topic = messagesBundle.getString("report_topic");
		ReportPDF.statsTopic = messagesBundle.getString("report_stats_topic");
		ReportPDF.amplitudeAPmaxTitle = messagesBundle.getString("AP_amplitude_max");
		ReportPDF.amplitudeAPavgTitle = messagesBundle.getString("AP_amplitude_avg");
		ReportPDF.amplitudeMLmaxTitle = messagesBundle.getString("ML_amplitude_max");
		ReportPDF.amplitudeMLavgTitle = messagesBundle.getString("ML_amplitude_avg");
		ReportPDF.frequencyAPavgTitle = messagesBundle.getString("AP_frequency_avg");
		ReportPDF.frequencyMLavgTitle = messagesBundle.getString("ML_frequency_avg");
		ReportPDF.travelTitle = messagesBundle.getString("travel");
		ReportPDF.durationTitle = messagesBundle.getString("duration");

		// Header
		class Header extends PdfPageEventHelper {
			public void onStartPage(PdfWriter writer, Document document) {
				try {
					document.add(header(writer.getPageNumber()));
				} catch (DocumentException e) {
					e.printStackTrace();
				}

			}

			private PdfPTable header(int pageNumber) {
				// Logo
				Image logo = null;
				try {
					logo = Image.getInstance(ReportPDF.class.getResource("/images/syhero_pdf.png"));
				} catch (BadElementException e) {
					e.printStackTrace();
				} catch (MalformedURLException e) {
					e.printStackTrace();
				} catch (IOException e) {
					e.printStackTrace();
				}
				Objects.requireNonNull(logo).scaleToFit(100, 100);
				PdfPTable tblLogo = new PdfPTable(new float[] { 1, 3, 1 });
				tblLogo.setWidthPercentage(100);
				PdfPCell rowLogo = new PdfPCell(logo);
				rowLogo.setBorder(Rectangle.NO_BORDER);
				rowLogo.setHorizontalAlignment(Element.ALIGN_LEFT);
				tblLogo.addCell(rowLogo);

				// Mockupwarning
				com.itextpdf.text.Font warningFont = FontFactory.getFont(FontFactory.TIMES_BOLD, 12, BaseColor.RED);
				Chunk warningStyled = new Chunk(messagesBundle.getString("mockup_warning"), warningFont);
				PdfPCell cellWarning = new PdfPCell(new Paragraph(warningStyled));
				cellWarning.setBorder(Rectangle.NO_BORDER);
				cellWarning.setHorizontalAlignment(Element.ALIGN_CENTER);
				tblLogo.addCell(cellWarning);

				// PageNumber
				PdfPCell cellPage = new PdfPCell(new Paragraph(Integer.toString(pageNumber)));
				cellPage.setBorder(Rectangle.NO_BORDER);
				cellPage.setHorizontalAlignment(Element.ALIGN_RIGHT);
				tblLogo.addCell(cellPage);
				return tblLogo;
			}
		}

		// Document
		Document pdf = new Document(PageSize.A4);
		// Margins left, right, top, bottom
		pdf.setMargins(42, 42, 42, 42);
		PdfWriter writer = null;

		JFileChooser saveTo = new JFileChooser();
		saveTo.setFileFilter(new FileFilter() {
			@Override
			public String getDescription() {
				return "PDF Files (*.pdf)";
			}

			@Override
			public boolean accept(File f) {
				if (f.isDirectory()) {
					return true;
				} else {
					String filename = f.getName().toLowerCase();
					return filename.endsWith(".pdf");
				}
			}
		});
		saveTo.setAcceptAllFileFilterUsed(false);
		int userSelection = saveTo.showSaveDialog(app);
		String path = null;

		if (userSelection == JFileChooser.APPROVE_OPTION) {
			path = saveTo.getSelectedFile().getAbsolutePath();
			if (!path.endsWith(".pdf")) {
				path += ".pdf";
			}
			System.out.println("Saving pdf to: " + path);

			try {
				writer = PdfWriter.getInstance(pdf, new FileOutputStream(path));
				writer.setPageEvent(new Header());
				pdf.open();

				// Topic
				PdfPTable tblTopic = new PdfPTable(1);
				tblTopic.setWidthPercentage(100);
				tblTopic.setSpacingAfter(6f);
				com.itextpdf.text.Font font = FontFactory.getFont(FontFactory.TIMES_BOLD, 20, BaseColor.BLACK);
				ReportPDF.customerID = boxList.getFirst().getCustomer().toString();
				Chunk topicStyled = new Chunk(topic + ": " + customerID, font);
				PdfPCell rowTopic = new PdfPCell(new Paragraph(topicStyled));
				rowTopic.setBorder(Rectangle.NO_BORDER);
				rowTopic.setHorizontalAlignment(Element.ALIGN_CENTER);
				tblTopic.addCell(rowTopic);
				pdf.add(tblTopic);

				// Line
				PdfPTable tblLine = new PdfPTable(1);
				tblLine.setWidthPercentage(100);
				PdfPCell rowLine = new PdfPCell();
				rowLine.setBorder(Rectangle.BOTTOM);
				rowLine.setBorderWidthBottom(2);
				tblLine.addCell(rowLine);
				tblLine.setSpacingAfter(11f);
				pdf.add(tblLine);

				// Arrays for ellipses and analysis details
				PdfPCell[] ellipses = new PdfPCell[boxList.size()];
				PdfPCell[] analysisDetails = new PdfPCell[boxList.size()];
				int index = 0;

				// Arrays for stats
				PdfPCell[] arrColumnNumbers = new PdfPCell[boxList.size() + 1];
				PdfPCell[] arrAmplitudeAPmax = new PdfPCell[boxList.size() + 1];
				PdfPCell[] arrAmplitudeAPavg = new PdfPCell[boxList.size() + 1];
				PdfPCell[] arrAmplitudeMLmax = new PdfPCell[boxList.size() + 1];
				PdfPCell[] arrAmplitudeMLavg = new PdfPCell[boxList.size() + 1];
				PdfPCell[] arrFrequencyAPavg = new PdfPCell[boxList.size() + 1];
				PdfPCell[] arrFrequencyMLavg = new PdfPCell[boxList.size() + 1];
				PdfPCell[] arrTravel = new PdfPCell[boxList.size() + 1];
				PdfPCell[] arrDuration = new PdfPCell[boxList.size() + 1];

				// Set topic in start of each array
				com.itextpdf.text.Font statsFont = FontFactory.getFont(FontFactory.TIMES, 10, BaseColor.BLACK);
				arrColumnNumbers[0] = new PdfPCell(new Paragraph(new Chunk(statsTopic, statsFont)));
				arrAmplitudeAPmax[0] = new PdfPCell(new Paragraph(new Chunk(amplitudeAPmaxTitle, statsFont)));
				arrAmplitudeAPavg[0] = new PdfPCell(new Paragraph(new Chunk(amplitudeAPavgTitle, statsFont)));
				arrAmplitudeMLmax[0] = new PdfPCell(new Paragraph(new Chunk(amplitudeMLmaxTitle, statsFont)));
				arrAmplitudeMLavg[0] = new PdfPCell(new Paragraph(new Chunk(amplitudeMLavgTitle, statsFont)));
				arrFrequencyAPavg[0] = new PdfPCell(new Paragraph(new Chunk(frequencyAPavgTitle, statsFont)));
				arrFrequencyMLavg[0] = new PdfPCell(new Paragraph(new Chunk(frequencyMLavgTitle, statsFont)));
				arrTravel[0] = new PdfPCell(new Paragraph(new Chunk(travelTitle, statsFont)));
				arrDuration[0] = new PdfPCell(new Paragraph(new Chunk(durationTitle, statsFont)));

				for (AnalysisBox currentBox : boxList) {
					ReportPDF.imagePath = currentBox.getAnalysis().getImageFile();
					ReportPDF.stance = messagesBundle.getString(currentBox.getAnalysis().getStance().toString());
					ReportPDF.eyes = messagesBundle.getString(currentBox.getAnalysis().getEyes());
					ReportPDF.dtDate = currentBox.getAnalysis().getDate();
					String strDate = df.format(dtDate);

					ReportPDF.measurer = currentBox.getAnalysis().getMeasurer();
					ReportPDF.analysisComments = currentBox.getAnalysis().getcomments();
					ReportPDF.dblAmplitudeAPmax = currentBox.getAnalysis().getAmplitudeAPmax();
					ReportPDF.dblAmplitudeAPavg = currentBox.getAnalysis().getAmplitudeAPavg();
					ReportPDF.dblAmplitudeMLmax = currentBox.getAnalysis().getAmplitudeMLmax();
					ReportPDF.dblAmplitudeMLavg = currentBox.getAnalysis().getAmplitudeMLavg();
					ReportPDF.dblFrequencyAPavg = currentBox.getAnalysis().getFrequencyAPavg();
					ReportPDF.dblFrequencyMLavg = currentBox.getAnalysis().getFrequencyMLavg();
					ReportPDF.dblTravelStat = currentBox.getAnalysis().getTravel();
					ReportPDF.dblDuration = currentBox.getAnalysis().getDuration();
					i++;

					// Ellipsis
					Image ellipsis = Image.getInstance(imagePath.getAbsolutePath());
					// ellipsis.scaleToFit(500, 500);
					PdfPCell ellipsisCell = new PdfPCell(ellipsis);
					ellipsisCell.setBorder(Rectangle.NO_BORDER);
					ellipses[index] = ellipsisCell;

					// Customer
					PdfPTable tblAnalysisDetails = new PdfPTable(1);
					tblAnalysisDetails.setWidthPercentage(100);
					tblAnalysisDetails.setSpacingAfter(11f);
					PdfPCell analysisDetailsCell = new PdfPCell(new Paragraph(new Chunk(((Integer.toString(i) + ".")
							+ "\n" + stance + "\n" + eyes + "\n" + strDate + " - " + measurer + "\n"), statsFont)));
					analysisDetailsCell.setBorder(Rectangle.NO_BORDER);
					analysisDetailsCell.setHorizontalAlignment(Element.ALIGN_LEFT);
					tblAnalysisDetails.addCell(analysisDetailsCell);
					PdfPCell rowCustomerComments = new PdfPCell(new Paragraph(new Chunk("\n\n\n", statsFont)));
					if (analysisComments.length() > 0) {
						rowCustomerComments = new PdfPCell(new Paragraph(new Chunk(analysisComments, statsFont)));
					}
					rowCustomerComments.setBorder(Rectangle.NO_BORDER);
					tblAnalysisDetails.addCell(rowCustomerComments);
					analysisDetails[index] = new PdfPCell(tblAnalysisDetails);

					// Stats
					arrColumnNumbers[index + 1] = new PdfPCell(
							new Paragraph(new Chunk(Integer.toString((index + 1)), statsFont)));
					arrAmplitudeAPmax[index + 1] = new PdfPCell(
							new Paragraph(new Chunk(dblAmplitudeAPmax.toString(), statsFont)));
					arrAmplitudeAPavg[index + 1] = new PdfPCell(
							new Paragraph(new Chunk(dblAmplitudeAPavg.toString(), statsFont)));
					arrAmplitudeMLmax[index + 1] = new PdfPCell(
							new Paragraph(new Chunk(dblAmplitudeMLmax.toString(), statsFont)));
					arrAmplitudeMLavg[index + 1] = new PdfPCell(
							new Paragraph(new Chunk(dblAmplitudeMLavg.toString(), statsFont)));
					arrFrequencyAPavg[index + 1] = new PdfPCell(
							new Paragraph(new Chunk(dblFrequencyAPavg.toString(), statsFont)));
					arrFrequencyMLavg[index + 1] = new PdfPCell(
							new Paragraph(new Chunk(dblFrequencyMLavg.toString(), statsFont)));
					arrTravel[index + 1] = new PdfPCell(new Paragraph(new Chunk(dblTravelStat.toString(), statsFont)));
					arrDuration[index + 1] = new PdfPCell(new Paragraph(new Chunk(dblDuration.toString(), statsFont)));

					index++;
				}

				int analysesAdded = ellipses.length;
				int start = 0;

				int row = 0;
				int detailsIndex = 0;
				int ellipsesIndex = 0;

				while (analysesAdded > 0) {
					PdfPTable tblBody = new PdfPTable(3);
					tblBody.setKeepTogether(true);
					tblBody.setWidthPercentage(100);

					for (int j = 0; j < 2; j++) {
						for (int col = 0; col < 3; col++) {
							if (row % 2 == 0) {
								PdfPCell cell = new PdfPCell();

								if (ellipsesIndex < ellipses.length) {
									cell = ellipses[ellipsesIndex];
								}
								cell.setBorder(Rectangle.NO_BORDER);
								tblBody.addCell(cell);
								ellipsesIndex++;
							} else {
								PdfPCell cell = new PdfPCell();

								if (detailsIndex < analysisDetails.length) {
									cell = analysisDetails[detailsIndex];
								}
								cell.setBorder(Rectangle.NO_BORDER);
								tblBody.addCell(cell);
								detailsIndex++;
							}
						}
						if (row % 2 == 1) {
							analysesAdded = analysesAdded - 3;
						}
						row++;
						start = start + 3;
					}

					pdf.add(tblBody);
				}

				int numberOfStats = arrColumnNumbers.length - 1;
				int colCount = 7;
				int startIndex = 1;
				int endIndex = 7;
				boolean newTopic = true;

				while (numberOfStats > 0) {
					PdfPTable statsTbl = new PdfPTable(colCount);
					statsTbl.setSpacingBefore(15f);
					PdfPCell emptyCell = new PdfPCell();
					emptyCell.setBorder(Rectangle.NO_BORDER);

					for (i = startIndex; i < endIndex; i++) {
						if (newTopic) {
							statsTbl.addCell(arrColumnNumbers[0]);
							newTopic = false;
						}
						if (i < arrColumnNumbers.length) {
							PdfPCell cell = arrColumnNumbers[i];
							cell.setHorizontalAlignment(Rectangle.ALIGN_CENTER);
							statsTbl.addCell(cell);
						} else {
							statsTbl.addCell(emptyCell);
						}
					}
					for (i = startIndex; i < endIndex; i++) {
						if (!newTopic) {
							statsTbl.addCell(arrAmplitudeAPmax[0]);
							newTopic = true;
						}
						if (i < arrColumnNumbers.length) {
							PdfPCell cell = arrAmplitudeAPmax[i];
							cell.setHorizontalAlignment(Rectangle.ALIGN_CENTER);
							statsTbl.addCell(cell);
						} else {
							statsTbl.addCell(emptyCell);
						}
					}
					for (i = startIndex; i < endIndex; i++) {
						if (newTopic) {
							statsTbl.addCell(arrAmplitudeAPavg[0]);
							newTopic = false;
						}
						if (i < arrColumnNumbers.length) {
							PdfPCell cell = arrAmplitudeAPavg[i];
							cell.setHorizontalAlignment(Rectangle.ALIGN_CENTER);
							statsTbl.addCell(cell);
						} else {
							statsTbl.addCell(emptyCell);
						}
					}
					for (i = startIndex; i < endIndex; i++) {
						if (!newTopic) {
							statsTbl.addCell(arrAmplitudeMLmax[0]);
							newTopic = true;
						}
						if (i < arrColumnNumbers.length) {
							PdfPCell cell = arrAmplitudeMLmax[i];
							cell.setHorizontalAlignment(Rectangle.ALIGN_CENTER);
							statsTbl.addCell(cell);
						} else {
							statsTbl.addCell(emptyCell);
						}
					}
					for (i = startIndex; i < endIndex; i++) {
						if (newTopic) {
							statsTbl.addCell(arrAmplitudeMLavg[0]);
							newTopic = false;
						}
						if (i < arrColumnNumbers.length) {
							PdfPCell cell = arrAmplitudeMLavg[i];
							cell.setHorizontalAlignment(Rectangle.ALIGN_CENTER);
							statsTbl.addCell(cell);
						} else {
							statsTbl.addCell(emptyCell);
						}
					}
					for (i = startIndex; i < endIndex; i++) {
						if (!newTopic) {
							statsTbl.addCell(arrFrequencyAPavg[0]);
							newTopic = true;
						}
						if (i < arrColumnNumbers.length) {
							PdfPCell cell = arrFrequencyAPavg[i];
							cell.setHorizontalAlignment(Rectangle.ALIGN_CENTER);
							statsTbl.addCell(cell);
						} else {
							statsTbl.addCell(emptyCell);
						}
					}
					for (i = startIndex; i < endIndex; i++) {
						if (newTopic) {
							statsTbl.addCell(arrFrequencyMLavg[0]);
							newTopic = false;
						}
						if (i < arrColumnNumbers.length) {
							PdfPCell cell = arrFrequencyMLavg[i];
							cell.setHorizontalAlignment(Rectangle.ALIGN_CENTER);
							statsTbl.addCell(cell);
						} else {
							statsTbl.addCell(emptyCell);
						}
					}
					for (i = startIndex; i < endIndex; i++) {
						if (!newTopic) {
							statsTbl.addCell(arrTravel[0]);
							newTopic = true;
						}
						if (i < arrColumnNumbers.length) {
							PdfPCell cell = arrTravel[i];
							cell.setHorizontalAlignment(Rectangle.ALIGN_CENTER);
							statsTbl.addCell(cell);
						} else {
							statsTbl.addCell(emptyCell);
						}
					}
					for (i = startIndex; i < endIndex; i++) {
						if (newTopic) {
							statsTbl.addCell(arrDuration[0]);
							newTopic = false;
						}
						if (i < arrColumnNumbers.length) {
							PdfPCell cell = arrDuration[i];
							cell.setHorizontalAlignment(Rectangle.ALIGN_CENTER);
							statsTbl.addCell(cell);
						} else {
							statsTbl.addCell(emptyCell);
						}
					}
					newTopic = true;
					statsTbl.setWidthPercentage(100);
					statsTbl.setKeepTogether(true);
					pdf.add(statsTbl);
					startIndex += 6;
					endIndex += 6;
					numberOfStats -= 6;
					pdf.add(emptyCell);
				}

				// ToolTip
				com.itextpdf.text.Font tipFont = FontFactory.getFont(FontFactory.TIMES, 8, BaseColor.BLACK);
				PdfPTable tblToolTip = new PdfPTable(1);
				tblToolTip.setSpacingBefore(5f);
				PdfPCell cellTip = new PdfPCell();
				cellTip.setBorder(Rectangle.NO_BORDER);
				tblToolTip.setWidthPercentage(100);
				tblToolTip.setHorizontalAlignment(Rectangle.ALIGN_LEFT);
				cellTip.addElement(new Paragraph(
						new Chunk(amplitudeAPmaxTitle + " = " + messagesBundle.getString("tip_ap_amp_max"), tipFont)));
				tblToolTip.addCell(cellTip);
				cellTip.setPhrase(new Paragraph(
						new Chunk(amplitudeAPavgTitle + " = " + messagesBundle.getString("tip_ap_amp_avg"), tipFont)));
				tblToolTip.addCell(cellTip);
				cellTip.setPhrase(new Paragraph(
						new Chunk(amplitudeMLmaxTitle + " = " + messagesBundle.getString("tip_ml_amp_max"), tipFont)));
				tblToolTip.addCell(cellTip);
				cellTip.setPhrase(new Paragraph(
						new Chunk(amplitudeMLavgTitle + " = " + messagesBundle.getString("tip_ml_amp_avg"), tipFont)));
				tblToolTip.addCell(cellTip);
				cellTip.setPhrase(new Paragraph(
						new Chunk(frequencyAPavgTitle + " = " + messagesBundle.getString("tip_ap_sway_avg"), tipFont)));
				tblToolTip.addCell(cellTip);
				cellTip.setPhrase(new Paragraph(
						new Chunk(frequencyMLavgTitle + " = " + messagesBundle.getString("tip_ml_sway_av"), tipFont)));
				tblToolTip.addCell(cellTip);
				cellTip.setPhrase(new Paragraph(
						new Chunk(travelTitle + " = " + messagesBundle.getString("tip_travel"), tipFont)));
				tblToolTip.addCell(cellTip);
				cellTip.setPhrase(new Paragraph(
						new Chunk(durationTitle + " = " + messagesBundle.getString("tip_duration"), tipFont)));
				tblToolTip.addCell(cellTip);
				pdf.add(tblToolTip);
			} catch (FileNotFoundException nf) {
				popupInformation(messagesBundle.getString("file_already_open"),
						messagesBundle.getString("try_again_title"));
				return null;
			} catch (Exception e) {
				popupInformation(messagesBundle.getString("error_when_saving_file"),
						messagesBundle.getString("try_again_title"));
				return null;
			} finally {
				pdf.close();
				if (writer != null) {
					writer.close();
				}
			}
			return path;
		}
		return null;
	}

	private static void popupInformation(String message, String title) {
		JOptionPane.showMessageDialog(app.getContentPane(), message, title, JOptionPane.INFORMATION_MESSAGE);
	}
}