package gui;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.FlowLayout;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Locale;
import java.util.ResourceBundle;

import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;

import domain.Customer;

/**
 * @author Teemu
 *
 */
class SearchPanel extends JPanel {
	/**
	 * 
	 */
	private static final long serialVersionUID = -4965788936871589795L;
	private ChooseListener chooseListener;
	private final JButton searchBtn = new JButton();
	private final JTextField searchField = new JTextField();
	private final JLabel resultAmount = new JLabel();
	private String currentSearch = "";
	private final List<Customer> results = new ArrayList<>();
	private final LinkedList<Customer> customers = new LinkedList<>();
	private int activeIndex = 0;

	public SearchPanel(List<Customer> givenCustomers) {
		ResourceBundle messages = ResourceBundle.getBundle("Messages", Locale.getDefault());
		customers.addAll(givenCustomers);
		this.setLayout(new BorderLayout());
		resultAmount.setText(0+"/"+0);

		searchField.getDocument().addDocumentListener(new DocumentListener() {

			@Override
			public void changedUpdate(DocumentEvent arg0) {
				//   Auto-generated method stub
				checkEnabled();
			}

			@Override
			public void insertUpdate(DocumentEvent arg0) {
				checkEnabled();
			}

			@Override
			public void removeUpdate(DocumentEvent arg0) {
				checkEnabled();
			}
			
			void checkEnabled() {
				if(searchField.getText().length()>=3) {
					searchBtn.setEnabled(true);
				} else {
					searchBtn.setEnabled(false);
					empty();
					currentSearch = "";
				}
				searchField.addKeyListener(new KeyAdapter() {
					@Override
					public void keyTyped(KeyEvent e) {
						if(searchField.getText().length()>49) {
							e.consume();
							searchField.setText(searchField.getText().substring(0, 49));
							java.awt.Toolkit.getDefaultToolkit().beep();
						}
					}
				});
			}
			
		});
		searchBtn.setText(messages.getString("search"));
		searchBtn.addActionListener(e -> {
			if(searchField.getText().length()>=3)
				search();
		});
		searchField.addActionListener(e -> search());
		searchBtn.setBackground(Color.LIGHT_GRAY);
		JPanel resultCenter = new JPanel(new FlowLayout(FlowLayout.CENTER));
		resultCenter.add(resultAmount);
		JPanel btnAndResults = new JPanel(new BorderLayout());
		btnAndResults.add(resultCenter, BorderLayout.EAST);
		btnAndResults.add(searchBtn, BorderLayout.WEST);
		JPanel searchBar = new JPanel(new BorderLayout());
		searchBar.add(searchField, BorderLayout.CENTER);
		searchBar.add(btnAndResults, BorderLayout.EAST);
		searchBtn.setEnabled(false);
		
		this.add(searchBar, BorderLayout.CENTER);
	}
	
	
	/**
	 * Chooses next customer from results
	 */
	private void chooseNext() {
		if(results.size()>1) {
			if(activeIndex<results.size()-1)
				activeIndex++;
			else
				activeIndex=0;
			choose();
		}
	}
	
	/**
	 * Selects the customer
	 */
	private void choose() {
		chooseListener.callForChooseItem(results.get(activeIndex));
		resultAmount.setText((activeIndex+1)+"/"+results.size());
		System.out.println("Choosing found customer:" + results.get(activeIndex));
	}
	
	/**
	 * Empties the search
	 */
	private void empty() {
		resultAmount.setText(0+"/"+0);
		results.clear();
		activeIndex=0;
	}
	
	/**
	 * Adds a customer from the list
	 * @param givenCustomer is the customer to be added to the list of customers
	 */
	public void addCustomer(Customer givenCustomer) {
		customers.add(givenCustomer);
		empty();
		searchField.setText("");
		currentSearch = null;
	}
	
	/**
	 * Removes a customer from the list
	 * @param givenCustomer is the customer to be removed form the list of customers
	 */
	public void removeCustomer(Customer givenCustomer) {
		customers.remove(givenCustomer);
		empty();
		currentSearch = null;
	}
	
	private void search() {
		int minimumCharAmount = 2;
		if(searchField.getText().length()> minimumCharAmount) {
			if(!searchField.getText().equals(currentSearch)) {
				currentSearch = searchField.getText();
				results.clear();
				for(Customer currentCustomer : customers) {
					if(currentCustomer.toString().toLowerCase().contains(currentSearch.toLowerCase())){
						results.add(currentCustomer);
					}
				}
				activeIndex = 0;
				if(results.size()>0) {
					resultAmount.setText(1+"/"+results.size());
					choose();
				} else {
					//No results found
					resultAmount.setText(0+"/"+0);
				}
			} else {
				chooseNext();
			}
		} else {
			//If under minimum amount of symbols
			resultAmount.setText(0+"/"+0);
		}
	}
	public void setChooseListener(ChooseListener givenListener) {
		this.chooseListener = givenListener;
	};
	
	interface ChooseListener {
		public void callForChooseItem(Customer givenCustomer);
	}
}
