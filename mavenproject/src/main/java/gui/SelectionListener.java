package gui;

interface SelectionListener {
	public void selectionChanged(Object selectedObject);

	public void checkedBoxChanged(Object checkedObject, boolean isChecked);
}