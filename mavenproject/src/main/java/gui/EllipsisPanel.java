package gui;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Component;
import java.util.*;

import javax.swing.*;

import org.jdesktop.swingx.WrapLayout;

import domain.Analysis;
import domain.Customer;

import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.Insets;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ComponentAdapter;
import java.awt.event.ComponentEvent;
import java.awt.Dimension;

/**
 * Panel that contains the reports title bar and ellipses for one or more Analysis objects.
 *
 * @author Maarit Ryynänen, Teemu Savorinen
 */
class EllipsisPanel extends JPanel implements ActionListener {

	/** Attribute required by Serializable interface, which is extended by JPanel */
	private static final long serialVersionUID = 7159012432545955461L;

	/** Strings for labels in user interface */
	private final ResourceBundle messagesBundle;
	
	private final JPanel analysisBoxesPanel;
	
	/** List of AnalysisBoxes */
	private static final LinkedList<AnalysisBox> analysisBoxes = new LinkedList<>();
	private AnalysisBox toBeRemoved = null;

	private final JButton plusZoomBtn = new JButton();
	private final JButton minusZoomBtn = new JButton();
	
	/** Current scale (According to zoom buttons)*/
	private double currentZoom = 0.95;
	
	/** Current scale (According to height of the panel) */
	private double scaleAccordingToHeight = 1;
	
	/** Title bar components */
	private final JPanel titleBarPanel = new JPanel(new BorderLayout());
	private final JLabel titleTxt = new JLabel();
	private final JLabel titleCustomersTxt = new JLabel();

	/** Listeners */
	private SelectionListener listener;
	private EditAnalysisListener editAnalysisListener;
	private SelectAnalysisListener selectAnalysisListener;
	
	private int windowHeight = 0;

	/**
	 * Constructor.
	 */
	public EllipsisPanel() {
		this.setLayout(new BorderLayout());
		this.messagesBundle = ResourceBundle.getBundle("Messages", Locale.getDefault());

		// Base pane where to add ellipses
		analysisBoxesPanel = new JPanel(new WrapLayout(WrapLayout.LEADING));

		JPanel zoomPanel = new JPanel();
		zoomPanel.setLayout(new BorderLayout());
		JPanel zoomPanelInner = new JPanel();
		zoomPanelInner.setLayout(new BorderLayout());
		int windowSize = (int) Math.round(Toolkit.getDefaultToolkit().getScreenSize().width * 0.2);
		reSize(zoomPanel, new Dimension(20, (int) Math.round(windowSize * currentZoom * 2)));
		reSize(zoomPanelInner, new Dimension(20, 40));
		
		// Zoom buttons
		reSize(plusZoomBtn, new Dimension(20 - 2, 20 - 2));
		plusZoomBtn.setText("+");
		plusZoomBtn.setFont(new Font(Font.SERIF, Font.PLAIN, (int) Math.round(18 * 0.8)));
		plusZoomBtn.setMargin(new Insets(1, 1, 1, 1));
		plusZoomBtn.addActionListener(this);
		plusZoomBtn.setBackground(Color.LIGHT_GRAY);
		
		reSize(minusZoomBtn, new Dimension(20 - 2, 20 - 2));
		minusZoomBtn.setText("-");
		minusZoomBtn.setFont(new Font(Font.SERIF, Font.PLAIN, (int) Math.round(18 * 0.8)));
		minusZoomBtn.setMargin(new Insets(1, 1, 1, 1));
		minusZoomBtn.addActionListener(this);
		minusZoomBtn.setBackground(Color.LIGHT_GRAY);

		zoomPanelInner.add(plusZoomBtn, BorderLayout.PAGE_START);
		zoomPanelInner.add(minusZoomBtn, BorderLayout.PAGE_END);

		//Panel for AnalysisBoxes
		JScrollPane analysisBoxesScrollPane = new JScrollPane(analysisBoxesPanel);
		JPanel mainPanel = new JPanel();
		mainPanel.setLayout(new BorderLayout());
		
		//Mockup warning
		JLabel mockupWarning = new JLabel();
		JPanel mockupWarningPanel = new JPanel(new FlowLayout(FlowLayout.CENTER, 0, 0));
		mockupWarning.setText(messagesBundle.getString("mockup_warning"));
		mockupWarning.setForeground(Color.RED);
		mockupWarningPanel.add(mockupWarning);
		titleBarPanel.add(mockupWarningPanel, BorderLayout.PAGE_START);
		mockupWarning.setFont(new Font(Font.SANS_SERIF, Font.BOLD, 12));
		
		Font titleFont = new Font(Font.SANS_SERIF, Font.PLAIN, 20);
		
		//Left side of the title bar
		JPanel titlePanel = new JPanel(new BorderLayout());
		titlePanel.add(titleTxt, BorderLayout.CENTER);
		titlePanel.add(titleCustomersTxt, BorderLayout.PAGE_END);
		titleCustomersTxt.setForeground(Color.BLACK);
		
		titleTxt.setFont(titleFont);
		titleTxt.setText(messagesBundle.getString("report_title"));
		titleCustomersTxt.setText("-");
		titleTxt.setForeground(Color.BLACK);

		JPanel titleBarLeft = new JPanel(new FlowLayout(FlowLayout.LEFT, 10, 10));
		titleBarPanel.add(titleBarLeft, BorderLayout.WEST);
		titleBarLeft.add(zoomPanelInner);
		titleBarLeft.add(titlePanel);
		
		//Right side of the title bar
		JLabel infoLabel = new JLabel();
		infoLabel.setText(" (\u00D8 = 1 cm)");
		infoLabel.setFont(titleFont);
		
		JPanel titleBarRight = new JPanel(new FlowLayout(FlowLayout.RIGHT, 10, 10));
		titleBarPanel.add(titleBarRight, BorderLayout.EAST);
		titleBarRight.add(infoLabel);
		
		//Setting up the main panel
		mainPanel.add(titleBarPanel, BorderLayout.PAGE_START);
		mainPanel.add(analysisBoxesScrollPane, BorderLayout.CENTER);
		zoomPanelInner.setBorder(BorderFactory.createLineBorder(Color.DARK_GRAY));
		analysisBoxesScrollPane.setBorder(BorderFactory.createEmptyBorder());
		add(mainPanel, BorderLayout.CENTER);
		analysisBoxesScrollPane.setHorizontalScrollBarPolicy(JScrollPane.HORIZONTAL_SCROLLBAR_NEVER);
		analysisBoxesScrollPane.getVerticalScrollBar().setUnitIncrement(50);
		
		//Scales AnalysisBoxes according to the panel height
		this.addComponentListener(new ComponentAdapter() {
			@Override
			public void componentResized(ComponentEvent e) {
				if(windowHeight<e.getComponent().getHeight()-10 || windowHeight>e.getComponent().getHeight()+10) {
					windowHeight = e.getComponent().getHeight();
					scaleAccordingToHeight = (float) (e.getComponent().getHeight()-titleBarPanel.getHeight()) / (float) (Toolkit.getDefaultToolkit().getScreenSize().width * 0.4);
					Double currentSize = currentZoom * scaleAccordingToHeight;
					for (AnalysisBox box : analysisBoxes) {
						box.updateSize(currentSize);
					}
					analysisBoxesPanel.updateUI();
				}
			}
		});
	}

	/**
	 * Used for resizing components
	 * 
	 * @param givenComponent
	 * 			Component to be resized
	 * @param newDimension
	 * 			New size for the component
	 */
	private void reSize(Component givenComponent, Dimension newDimension) {
		givenComponent.setPreferredSize(newDimension);
		givenComponent.setMinimumSize(newDimension);
		givenComponent.setMaximumSize(newDimension);
	}

	/**
	 * Unselects all AnalysisBoxes
	 */
	private void unselectAll() {
		for(AnalysisBox currentBox : analysisBoxes) {
			currentBox.unselect();
		}
	}
	
	/**
	 * Adds one Analysis to report.
	 * 
	 * @param analysis
	 *			The Analysis to be added to report
	 */
	public void addAnalysisToReport(Analysis analysis, Customer givenCustomer) {
		if (analysis == null)
			return;

		// Check if report includes analysis yet
		for (AnalysisBox currentBox : analysisBoxes) {
			if (currentBox.getAnalysis() == analysis) {
				System.out.println("Analysis " + analysis + " was not added to report because it was already there");
				return;
			}
		}
		

		// Add Analysis' AnalysisBox to report
		AnalysisBox newBox = new AnalysisBox(analysis, analysisBoxes.size() + 1, givenCustomer);
		analysisBoxes.add(newBox);
		System.out.println("Added analysis " + analysis + " to report");

		analysisBoxesPanel.add(newBox);
		newBox.updateSize(currentZoom * scaleAccordingToHeight);
		newBox.setRemoveListener(givenAnalysis -> {
			removeAnalysisFromReport(givenAnalysis);
			listener.checkedBoxChanged(givenAnalysis, false);
		});
		newBox.setSelectListener((givenAnalysis, givenBox) -> {
			unselectAll();
			givenBox.select();
			selectAnalysisListener.callForSelect(givenAnalysis);
		});
		newBox.setEditListener(givenAnalysis -> editAnalysisListener.callForEditAnalysisItem(givenAnalysis));
		updateTitle();
		analysisBoxesPanel.updateUI();
	}
	
	/**
	 * Main method for removing analysis AnalysisBox
	 */
	public void removeAnalysisFromReport(Analysis analysis) {
		for (AnalysisBox currentBox : analysisBoxes) {
			if (currentBox.getAnalysis() == analysis) {
				toBeRemoved = currentBox;
				break;
			}
		}
		if (toBeRemoved != null) {
			analysisBoxesPanel.remove(toBeRemoved);
			analysisBoxes.remove(toBeRemoved);
			System.out.println("Removed analysis " + analysis + " from report");
			toBeRemoved = null;
			updateBoxNumbers();
		}
		updateTitle();
		analysisBoxesPanel.updateUI();
	}

	/**
	 * Removes a list of analysis from the report and updates view.
	 * 
	 * @param analysisList
	 *            The list of Analysis to be removed from report
	 */
	public void removeAnalysisFromReport(List<Analysis> analysisList) {
		if (analysisList != null) {
			for (Analysis analysis : analysisList) {
				removeAnalysisFromReport(analysis);
			}
		}
	}
	
	/**
	 * Updates the numbers of AnalysisBoxes. Used when removing AnalysisBox.
	 */
	private void updateBoxNumbers() {
		int boxNumbers = 0;
		for (AnalysisBox currentBox : analysisBoxes) {
			boxNumbers++;
			currentBox.setBoxNumber(boxNumbers);
		}
		analysisBoxesPanel.removeAll();

		for (AnalysisBox currentBox : analysisBoxes) {
			analysisBoxesPanel.add(currentBox);
		}
		this.updateUI();
	}

	/**
	 * Sets the box selected that contains the given analysis.
	 * 
	 * @param analysisToSelect
	 *            The Analysis to select
	 */
	public void selectAnalysisBox(Analysis analysisToSelect) {
		if (analysisToSelect == null) {
			unselectAll();
			return;
		}
		unselectAll();
		for(AnalysisBox currentBox : analysisBoxes) {
			if(currentBox.getAnalysis().equals(analysisToSelect)) {
				currentBox.select();
			}
		}
	}

	public void actionPerformed(ActionEvent e) {
		if (e.getSource() == plusZoomBtn && currentZoom < 1.8) {
			currentZoom += 0.1;
			for (AnalysisBox box : analysisBoxes) {
				box.updateSize(currentZoom * scaleAccordingToHeight);
				analysisBoxesPanel.updateUI();
			}
		}
		if (e.getSource() == minusZoomBtn && currentZoom > 0.6) {
			currentZoom -= 0.1;
			for (AnalysisBox box : analysisBoxes) {
				box.updateSize(currentZoom * scaleAccordingToHeight);
				analysisBoxesPanel.updateUI();
			}
		}
	}
	
	/** Interfaces for listeners */
	interface ApplicationWindowEditListener {
		public void callForEdit(Analysis givenAnalysis);
	}
	interface SelectAnalysisListener {
		public void callForSelect(Analysis givenAnalysis);
	}
	interface EditAnalysisListener {
		public void callForEditAnalysisItem(Analysis givenAnalysis);
	}
	
	/** Listeners */
	public void setBoxSelectionListener(SelectionListener listener) {
		this.listener = listener;
	}
	public void setEditAnalysisListener(EditAnalysisListener givenListener) {
		this.editAnalysisListener = givenListener;
	};
	public void setSelectAnalysisListener(SelectAnalysisListener givenListener) {
		this.selectAnalysisListener = givenListener;
	};

	/**
	 * Updates changed analysis after color change.
	 * 
	 * @param analysis
	 * 			The given analysis object
	 */
	public void updateView(Analysis analysis) {
		// Find analysis
		for (AnalysisBox currentBox : analysisBoxes) {
			currentBox.updateView();
			if (currentBox.getAnalysis().compareTo(analysis) == 0) {
				currentBox.updateView();
			}
		}
		updateTitle();
	}
	
	/**
	 * Returns the current list of AnalysisBoxes
	 * @return LinkedList<AnalysisBox> A LinkedList of AnalysisBoxes
	 */
	public static LinkedList<AnalysisBox> getBoxes() {
		return analysisBoxes;
	}
	
	/**
	 * 
	 * @return String The string of customers according to the added AnalysisBoxes
	 */
	private String customersToTitle() {
		StringBuilder mainString = new StringBuilder();
		List<Customer> customers = new ArrayList<>();
		for(AnalysisBox currentBox : analysisBoxes) {
			if(!customers.contains(currentBox.getCustomer())) {
				customers.add(currentBox.getCustomer());
			}
		}
		for(Customer currentCustomer : customers) {
			if(currentCustomer != null)
				mainString.append(currentCustomer.toString()).append(", ");
		}
		if(customers.size()>1) {
			updateShowCustomerTitleOnBoxes(true);
			titleCustomersTxt.setForeground(Color.RED);
		} else {
			updateShowCustomerTitleOnBoxes(false);
			titleCustomersTxt.setForeground(Color.BLACK);
		}
		if(mainString.length()>0)
			return "(" + mainString.substring(0, mainString.length()-2) + ")";
		return "-";
	}
	
	/**
	 * Show or unshow the customers name on the AnalysisBoxes
	 * @param givenBoolean
	 * 			Show or unshow
	 */
	private void updateShowCustomerTitleOnBoxes(boolean givenBoolean) {
		if(givenBoolean) {
			for(AnalysisBox currentBox : analysisBoxes) {
				currentBox.showCustomer(true);
			}
		} else {
			for(AnalysisBox currentBox : analysisBoxes) {
				currentBox.showCustomer(false);
			}
		}
	}
	/**
	 * Updates the current customers into the customers title label
	 */
	private void updateTitle() {
		titleCustomersTxt.setText(customersToTitle());
	}
	
	/**
	 * @return number of Customers whose Analyses are shown on report
	 */
	public int getCustomerCount() {
	    List<Customer> customers = new ArrayList<>();
		for(AnalysisBox currentBox : analysisBoxes) {
			if(!customers.contains(currentBox.getCustomer())) {
				customers.add(currentBox.getCustomer());
			}
		}
		return customers.size();
	}
	
	/**
	 * Updates the shown customer name in AnalysisBoxes (UI)
	 * @param givenCustomer
	 * 			Customer which needs to be checked if needs to be updated in UI
	 */
	public void updateCustomer(Customer givenCustomer) {
		for(AnalysisBox currentBox : analysisBoxes) {
			if(currentBox.getCustomer().equals(givenCustomer)) {
				currentBox.updateIfShownCustomer();
			}
		}
		updateTitle();
	}
}
