package gui;

/*
Java Swing, 2nd Edition
By Marc Loy, Robert Eckstein, Dave Wood, James Elliott, Brian Cole
ISBN: 0-596-00408-7
Publisher: O'Reilly
*/
// SplashScreen.java
//A simple application to show a title screen in the center of the screen
//for the amount of time given in the constructor. This class includes
//a sample main() method to test the splash screen, but it's meant for use
//with other applications.
//Modified by Juha Korvenaho
//

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Toolkit;
import java.util.Locale;
import java.util.ResourceBundle;
import java.util.concurrent.atomic.AtomicBoolean;

import javax.swing.ImageIcon;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JWindow;

public class SplashScreen extends JWindow {
  
	private static final long serialVersionUID = 6635753915694976026L;
	private final int duration;
	private final AtomicBoolean showGui;
	private int run = 0;

	public SplashScreen(int d, AtomicBoolean show) {
		this.showGui = show;
		getContentPane().setLayout(null);
		duration = d;
	}

	// A simple little method to show a title screen in the center
	// of the screen for the amount of time given in the constructor
	public void showSplash() {
		JPanel content = (JPanel) getContentPane();
		content.setBackground(Color.white);

		// Set the window's bounds, centering the window
		int width = 750;
		int height = 400;
		Dimension screen = Toolkit.getDefaultToolkit().getScreenSize();
		int x = (screen.width - width) / 2;
		int y = (screen.height - height) / 2;
		setBounds(x, y, width, height);

	    // Build the splash screen
	    //ohjelman nimen, logon, versiotiedon ja lisenssimaininnan
	    JLabel label = new JLabel(new ImageIcon(getClass().getResource("/images/syhero.png")), JLabel.CENTER);
	    label.setBounds(0, 0, 750, 400);
	    
	    ResourceBundle messages = ResourceBundle.getBundle("Messages", Locale.getDefault());
	    String title = messages.getString("title") + " " + messages.getString("version");
	    JLabel name = new JLabel(title, JLabel.CENTER);
	    name.setBounds(0, 350, 1075, 25);
	    
	    JLabel license = new JLabel(messages.getString("license"), JLabel.CENTER);
	    license.setBounds(0, 370, 1075, 25);
	    
	    content.add(label);
	    content.add(name);
	    content.add(license);
	
	    // Display it
	    setVisible(true);
	    //setAlwaysOnTop(true);
	
	    // Wait a little while, maybe while loading resources
	    try {
	    	while (!showGui.get()) {
	    		if (run == 10) {
	    			break;
	    		}
	    		Thread.sleep(duration);
	    		run++;
	    	}
	    }
	    catch (Exception e) {
	    }
	    setVisible(false);
	}

	public void showSplashAndExit() {
		showSplash();
		System.exit(0);
	}
}