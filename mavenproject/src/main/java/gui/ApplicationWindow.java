package gui;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.Desktop;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.GridLayout;
import java.awt.event.*;
import java.io.*;
import java.text.DateFormat;

import java.text.ParseException;
import java.util.*;
import java.util.concurrent.atomic.AtomicBoolean;

import javax.imageio.ImageIO;
import javax.swing.*;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;
import javax.swing.filechooser.FileNameExtensionFilter;

import org.apache.commons.lang3.mutable.MutableInt;

import domain.Analysis;
import domain.Customer;
import domain.RawDataAnalyzer;
import tree.MutableObjectNode;
import util.ReportPDF;
import util.Stance;

/**
 * Main window to graphical user interface. Includes toolbar for buttons,
 * directory panel and report panel.
 * 
 * @author Maarit Ryynänen
 */
public class ApplicationWindow extends JFrame {

	/** Attribute required by Serializable interface, which is extended by JFrame */
	private static final long serialVersionUID = 7210805112708284109L;

	/** Components to show */
	private SearchPanel searchPanel;
	private DirectoryPanel dirPanel;
	private EllipsisPanel ellipsisPanel;
	private CommentPanel commentPanel;
	private StatsPanel statsPanel;
	private final ToolTipPanel toolTipPanel = new ToolTipPanel();

	/** Menu buttons */
	private JButton btnNewCustomer;
	private JButton btnNewAnalysis;
	private JButton btnEdit;
	private JButton btnDelete;
	private JButton btnPrintPdf;
	private JButton btnUserManual;

	/** Localized strings for labels in user interface */
	private final ResourceBundle messagesBundle;

	/** Variables for file handling */
	private final JFileChooser fileChooser;
	private final File workingDir; // Where customer files are located

	// Variables for analysis popup
	private Date date = null;
	private Stance stance = null;
	private String eyes = null;
	private String analysisComment = null;
	private String measurer = null;
	private JTextField dateField = new JTextField();
	private ApplicationWindow app;
	private String path;

	// Variables for customer popup
	private String folderName = null;
	private String customerComment = null;

	/**
	 * @param workingDir
	 *            The working directory
	 */
	public ApplicationWindow(File workingDir) {
		addWindowFocusListener(new WindowFocusListener() {
			public void windowGainedFocus(WindowEvent arg0) {
			}

			public void windowLostFocus(WindowEvent arg0) {
			}
		});
		this.workingDir = workingDir;
		this.messagesBundle = ResourceBundle.getBundle("Messages", Locale.getDefault());

		// Set starting point for searching raw data files
		this.fileChooser = new JFileChooser();
		fileChooser.setFileSelectionMode(JFileChooser.FILES_ONLY);
		FileNameExtensionFilter filter = new FileNameExtensionFilter("TXT files", "txt");
		fileChooser.setFileFilter(filter);

		File userHome = this.fileChooser.getFileSystemView().getHomeDirectory();
		this.fileChooser.setCurrentDirectory(userHome);
	}

	/**
	 * Initializes and shows the user interface window.
	 * 
	 * @param customerList
	 *            List of Customers in the working directory (must not be null)
	 */
	public void runGUI(List<Customer> customerList, AtomicBoolean show) throws NullPointerException {
		if (customerList == null)
			throw new NullPointerException("Customer list should be initialized");

		setTitle(messagesBundle.getString("title") + " " + messagesBundle.getString("version")); // window title
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 1000, 600); // window size
		setResizable(true); // user may resize window

		// Add icon image to window title
		try (InputStream stream = MutableObjectNode.class.getResourceAsStream("/images/icon.png")) {
			this.setIconImage(ImageIO.read(stream));
		} catch (IOException e) {
			System.out.println("Icon not found");
		}

		// CREATE ALL COMPONENTS
		dirPanel = new DirectoryPanel(this.workingDir, customerList);
		dirPanel.setName("directoryPanel");
		dirPanel.setPreferredSize(new Dimension(240, 23));
		ellipsisPanel = new EllipsisPanel();
		ellipsisPanel.setName("ellipsesPanel");
		commentPanel = new CommentPanel();
		commentPanel.setName("commentPanel");
		commentPanel.setPreferredSize(new Dimension(50, 90));
		statsPanel = new StatsPanel();
		statsPanel.setName("statsPanel");
		searchPanel = new SearchPanel(customerList);
		searchPanel.setPreferredSize(new Dimension(25, 25));
		searchPanel.setChooseListener(givenCustomer -> dirPanel.selectCustomer(givenCustomer));

		// SET LISTENERS TO COMPONENTS

		// ToolTips for the statsPanel
		MouseMotionAdapter mouseAdapter = new MouseMotionAdapter() {
			public void mouseMoved(MouseEvent e) {
				int rowHeight = 15;
				int movePointing = 4;
				if (e.getY() < rowHeight + movePointing) {
					toolTipPanel.setTip(messagesBundle.getString("tip_ap_amp_max"));
				} else if (e.getY() < rowHeight * 2 + movePointing) {
					toolTipPanel.setTip(messagesBundle.getString("tip_ap_amp_avg"));
				} else if (e.getY() < rowHeight * 3 + movePointing) {
					toolTipPanel.setTip(messagesBundle.getString("tip_ml_amp_max"));
				} else if (e.getY() < rowHeight * 4 + movePointing) {
					toolTipPanel.setTip(messagesBundle.getString("tip_ml_amp_avg"));
				} else if (e.getY() < rowHeight * 5 + movePointing) {
					toolTipPanel.setTip(messagesBundle.getString("tip_ap_sway_avg"));
				} else if (e.getY() < rowHeight * 6 + movePointing) {
					toolTipPanel.setTip(messagesBundle.getString("tip_ml_sway_av"));
				} else if (e.getY() < rowHeight * 7 + movePointing) {
					toolTipPanel.setTip(messagesBundle.getString("tip_travel"));
				} else if (e.getY() < rowHeight * 8 + movePointing) {
					toolTipPanel.setTip(messagesBundle.getString("tip_duration"));
				}
			}
		};
		MouseAdapter emptyTipStats = new MouseAdapter() {
			@Override
			public void mouseExited(MouseEvent e) {
				toolTipPanel.setTip("");
			}
		};
		statsPanel.getTable().addMouseMotionListener(mouseAdapter);
		statsPanel.getRowHeaderTable().addMouseMotionListener(mouseAdapter);
		statsPanel.getTable().addMouseListener(emptyTipStats);
		statsPanel.getRowHeaderTable().addMouseListener(emptyTipStats);

		// Add tree selection listener to directory panel
		this.dirPanel.setSelectionListener(new SelectionListener() {
			@Override
			public void selectionChanged(Object selectedObject) {
				System.out.println("Selected " + selectedObject + " on directory panel");

				Class<?> selectedClass = selectedObject == null ? null : selectedObject.getClass();
				toggleButtonDisable(selectedClass);

				// Set the clicked analysis selected on stats panel and on ellipsis panel
				Analysis selectedAnalysis = selectedClass == Analysis.class ? (Analysis) selectedObject : null;
				statsPanel.selectColumn(selectedAnalysis);
				ellipsisPanel.selectAnalysisBox(selectedAnalysis);

				try {
					commentPanel.updateComment(dirPanel.getActiveCustomer());
				} catch (IOException | org.json.simple.parser.ParseException e) {
					popupInformation(messagesBundle.getString("error_when_reading_file"), "error_title");
					e.printStackTrace();
				}
			}

			@Override
			public void checkedBoxChanged(Object checkedObject, boolean isChecked) {
				if (checkedObject.getClass() == Analysis.class)
					handleCheckBoxChange((Analysis) checkedObject, isChecked, null);
				else if (checkedObject.getClass() == Customer.class)
					handleCheckBoxChange((Customer) checkedObject, isChecked);
			}
		});
		// Add popup menu listener to directory panel
		this.dirPanel.setRightClickMenuListener(e -> {
			String actionCommand = e.getActionCommand();
			System.out.println("Popup menu item " + actionCommand + " was chosen");

			if (actionCommand.equals(messagesBundle.getString("add_customer"))) {
				btnNewCustomer.doClick();
			} else if (actionCommand.equals(messagesBundle.getString("add_analysis"))) {
				btnNewAnalysis.doClick();
			} else if (actionCommand.equals(messagesBundle.getString("edit"))) {
				btnEdit.doClick();
			} else if (actionCommand.equals(messagesBundle.getString("delete"))) {
				btnDelete.doClick();
			}
		});
		// Add box selection listener to ellipsis panel
		this.ellipsisPanel.setBoxSelectionListener(new SelectionListener() {
			@Override
			public void selectionChanged(Object selectedObject) {
				System.out.println("Clicked " + selectedObject + " on ellipsis panel");

				// Set the clicked analysis selected on directory tree and on stats table
				Analysis selectedAnalysis = selectedObject.getClass() == Analysis.class ? (Analysis) selectedObject
						: null;
				dirPanel.selectCheckedAnalysis(selectedAnalysis);
				statsPanel.selectColumn(selectedAnalysis);
			}

			@Override
			public void checkedBoxChanged(Object uncheckedObject, boolean isChecked) {
				// (EllipsesPanel can only uncheck analyses)
				Analysis uncheckedAnalysis = (Analysis) uncheckedObject;
				dirPanel.uncheckAnalysis(uncheckedAnalysis);
				statsPanel.removeStatsFromTable(uncheckedAnalysis);

				// Set print pdf button enabled if there are some analyses on the report
				if (statsPanel.isEmpty()||ellipsisPanel.getCustomerCount()>1) {
					btnPrintPdf.setEnabled(false);
				} else {
					btnPrintPdf.setEnabled(true);
				}
			}
		});
		// Add column selection listener to stats panel
		this.statsPanel.setColumnClickListener(new SelectionListener() {
			@Override
			public void selectionChanged(Object selectedObject) {
				System.out.println("Clicked " + selectedObject + " on stats table");

				// Set the clicked analysis selected on directory tree and on ellipsis panel
				Analysis selectedAnalysis = selectedObject.getClass() == Analysis.class ? (Analysis) selectedObject
						: null;
				dirPanel.selectCheckedAnalysis(selectedAnalysis);
				ellipsisPanel.selectAnalysisBox(selectedAnalysis);
			}

			@Override
			public void checkedBoxChanged(Object checkedObject, boolean isChecked) {
				// Not used in stats panel
			}

		});

		JPanel leftSidePanel = new JPanel(new BorderLayout());
		leftSidePanel.add(searchPanel, BorderLayout.PAGE_START);
		leftSidePanel.add(dirPanel, BorderLayout.CENTER);
		leftSidePanel.add(commentPanel, BorderLayout.PAGE_END);
		leftSidePanel.setMinimumSize(new Dimension(216, 400));

		JPanel rightSidePanel = new JPanel(new BorderLayout());
		rightSidePanel.add(ellipsisPanel, BorderLayout.CENTER);
		rightSidePanel.add(statsPanel, BorderLayout.SOUTH);
		rightSidePanel.setMinimumSize(new Dimension(400, 400));

		// Listener for calling edit from AnalysisBox
		ellipsisPanel.setEditAnalysisListener(givenAnalysis -> {
			dirPanel.selectCheckedAnalysis(givenAnalysis);
			btnEdit.doClick();
		});

		// Listener for forwarding EllipsesPanels selections
		ellipsisPanel.setSelectAnalysisListener(givenAnalysis -> {
			dirPanel.selectCheckedAnalysis(givenAnalysis);
			statsPanel.selectColumn(givenAnalysis);
		});

		// Split window in two parts so that user can resize them
		UIManager.put("SplitPane.dividerSize", 3);
		JSplitPane splitPane = new JSplitPane(JSplitPane.HORIZONTAL_SPLIT, leftSidePanel, rightSidePanel);
		splitPane.setDividerLocation(420);

		// Create toolbar
		JPanel toolbar = getToolBar();
		toolbar.setName("toolbar");
		setButtonNames();
		toggleButtonDisable(null); // Disable buttons that require selecting customer and/or analysis first
		btnPrintPdf.setEnabled(false);

		// Add components to window layout
		getContentPane().setLayout(new BorderLayout());
		getContentPane().add(toolbar, BorderLayout.NORTH);
		getContentPane().add(splitPane, BorderLayout.CENTER);

		// Set minimum size
		double minimumWidth = toolbar.getMinimumSize().getWidth();
		double minimumHeight = toolbar.getMinimumSize().getHeight() + statsPanel.getPreferredSize().getHeight() * 3;
		setMinimumSize(new Dimension((int) minimumWidth, (int) minimumHeight));

		this.setExtendedState(JFrame.MAXIMIZED_BOTH); // Fill full screen
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		show.set(true);
		setVisible(true);
		//setAlwaysOnTop(true);
		this.app = this;
	}

	/**
	 * Names components that will be unit tested.
	 */
	private void setButtonNames() {
		this.btnNewCustomer.setName("btnNewCustomer");
		this.btnNewAnalysis.setName("btnNewAnalysis");
		this.btnEdit.setName("btnEdit");
		this.btnDelete.setName("btnDelete");
		this.btnPrintPdf.setName("btnPrintPdf");
	}

	private List<Object> turnToArray(Component givenComponent, String givenString) {
		List<Object> newArray = new ArrayList<>();
		newArray.add(givenComponent);
		newArray.add(givenString);
		return newArray;
	}

	private List<List<Object>> getComponentsForToolTip() {
		List<List<Object>> componentList = new ArrayList<>();
		componentList.add(turnToArray(this.btnNewCustomer, messagesBundle.getString("tip_new_customer")));
		componentList.add(turnToArray(this.btnNewAnalysis, messagesBundle.getString("tip_add_raw_data")));
		componentList.add(turnToArray(this.btnEdit, messagesBundle.getString("tip_edit")));
		componentList.add(turnToArray(this.btnDelete, messagesBundle.getString("tip_remove")));
		componentList.add(turnToArray(this.btnPrintPdf, messagesBundle.getString("tip_create_pdf")));
		componentList.add(turnToArray(this.btnUserManual, messagesBundle.getString("tip_manual")));
		return componentList;
	}

	/**
	 * Create menu buttons and a tool bar.
	 * 
	 * @return JToolBar Toolbar containing menu buttons
	 */
	private JPanel getToolBar() {
		// Get button icons
		Icon newCustomerIcon = null;
		Icon newAnalysisIcon = null;
		Icon editIcon = null;
		Icon printPdfIcon = null;
		Icon userManualIcon = null;
		Icon deleteIcon = null;
		InputStream newCustomerStream = null;
		InputStream newAnalysisStream = null;
		InputStream editStream = null;
		InputStream printPdfStream = null;
		InputStream userManualStream = null;
		InputStream deleteStream = null;
		try {
			newCustomerStream = ApplicationWindow.class.getResourceAsStream("/images/btnNewCustomer.png");
			if (newCustomerStream != null) {
				newCustomerIcon = new ImageIcon(ImageIO.read(newCustomerStream));
			}
			newAnalysisStream = ApplicationWindow.class.getResourceAsStream("/images/btnNewAnalysis.png");
			if (newAnalysisStream != null) {
				newAnalysisIcon = new ImageIcon(ImageIO.read(newAnalysisStream));
			}
			editStream = ApplicationWindow.class.getResourceAsStream("/images/btnEdit.png");
			if (editStream != null) {
				editIcon = new ImageIcon(ImageIO.read(editStream));
			}
			printPdfStream = ApplicationWindow.class.getResourceAsStream("/images/btnPrintPdf.png");
			if (editStream != null) {
				printPdfIcon = new ImageIcon(ImageIO.read(printPdfStream));
			}
			userManualStream = ApplicationWindow.class.getResourceAsStream("/images/btnUserManual.png");
			if (editStream != null) {
				userManualIcon = new ImageIcon(ImageIO.read(userManualStream));
			}
			deleteStream = ApplicationWindow.class.getResourceAsStream("/images/btnDelete.png");
			if (editStream != null) {
				deleteIcon = new ImageIcon(ImageIO.read(deleteStream));
			}
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			try {
				if (newCustomerStream != null)
					newCustomerStream.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
			try {
				if (newAnalysisStream != null)
					newAnalysisStream.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
			try {
				if (editStream != null)
					editStream.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
			try {
				if (printPdfStream != null)
					printPdfStream.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
			try {
				if (userManualStream != null)
					userManualStream.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
			try {
				if (deleteStream != null)
					deleteStream.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}

		double btnWidth = 0;

		// Create all buttons
		this.btnNewCustomer = new JButton(messagesBundle.getString("add_customer"), newCustomerIcon);
		this.btnNewCustomer.setHorizontalTextPosition(SwingConstants.CENTER);
		this.btnNewCustomer.setVerticalTextPosition(SwingConstants.BOTTOM);
		btnWidth = btnWidth < this.btnNewCustomer.getPreferredSize().getWidth()
				? this.btnNewCustomer.getPreferredSize().getWidth()
				: btnWidth;

		this.btnNewAnalysis = new JButton(messagesBundle.getString("add_analysis"), newAnalysisIcon);
		this.btnNewAnalysis.setHorizontalTextPosition(SwingConstants.CENTER);
		this.btnNewAnalysis.setVerticalTextPosition(SwingConstants.BOTTOM);
		btnWidth = btnWidth < this.btnNewAnalysis.getWidth() ? this.btnNewAnalysis.getWidth() : btnWidth;

		this.btnEdit = new JButton(messagesBundle.getString("edit"), editIcon);
		this.btnEdit.setHorizontalTextPosition(SwingConstants.CENTER);
		this.btnEdit.setVerticalTextPosition(SwingConstants.BOTTOM);
		btnWidth = btnWidth < this.btnEdit.getWidth() ? this.btnEdit.getWidth() : btnWidth;

		this.btnPrintPdf = new JButton(messagesBundle.getString("print_pdf"), printPdfIcon);
		this.btnPrintPdf.setHorizontalTextPosition(SwingConstants.CENTER);
		this.btnPrintPdf.setVerticalTextPosition(SwingConstants.BOTTOM);
		btnWidth = btnWidth < this.btnPrintPdf.getWidth() ? this.btnPrintPdf.getWidth() : btnWidth;

		this.btnUserManual = new JButton(messagesBundle.getString("user_manual"), userManualIcon);
		btnUserManual.setHorizontalTextPosition(SwingConstants.CENTER);
		btnUserManual.setVerticalTextPosition(SwingConstants.BOTTOM);
		btnWidth = btnWidth < btnUserManual.getWidth() ? btnUserManual.getWidth() : btnWidth;
		
		//Check if manual exists
		File initmanual_file = new File(System.getProperty("user.dir") + File.separatorChar + messagesBundle.getString("user_manual_filename"));
		
		if (Desktop.isDesktopSupported()) {
			if (!initmanual_file.exists()) {
				btnUserManual.setEnabled(false);
			}
		}
		
		Dimension btnSize = new Dimension((int) btnWidth, (int) btnPrintPdf.getPreferredSize().getHeight());

		this.btnDelete = new JButton(messagesBundle.getString("delete"), deleteIcon);
		this.btnDelete.setHorizontalTextPosition(SwingConstants.CENTER);
		this.btnDelete.setVerticalTextPosition(SwingConstants.BOTTOM);
		this.btnDelete.setPreferredSize(btnSize);
		this.btnDelete.setMaximumSize(btnSize);
		this.btnDelete.setMinimumSize(btnSize);

		// Listener to btnNewCustomer button
		btnNewCustomer.addActionListener(e -> createNewCustomer());
		// Listener to btnNewAnalysis button
		btnNewAnalysis.addActionListener(e -> {
			Customer selectedCustomer = dirPanel.getActiveCustomer(); // Which customer is currently selected
			createNewAnalysis(selectedCustomer);
		});
		// Listener to btnEdit button
		btnEdit.addActionListener(e -> {
			Analysis selectedAnalysis = dirPanel.getActiveAnalysis(); // Which analysis is currently selected
			Customer selectedCustomer = dirPanel.getActiveCustomer(); // Which customer is currently selected
			try {
				if (selectedAnalysis != null)
					editAnalysis(selectedAnalysis, selectedCustomer);
				else if (selectedCustomer != null)
					editCustomer(selectedCustomer);
			} catch (IOException ioe) {
				ioe.printStackTrace();
				popupInformation(messagesBundle.getString("error_when_saving_file"),
						messagesBundle.getString("try_again_title"));
			}
		});
		// Listener to btnDelete button
		btnDelete.addActionListener(e -> {
			Analysis selectedAnalysis = dirPanel.getActiveAnalysis(); // Which analysis is currently selected
			Customer selectedCustomer = dirPanel.getActiveCustomer(); // Which customer is currently selected

			try {
				if (selectedAnalysis != null) {
					deleteAnalysis(selectedCustomer, selectedAnalysis);
				} else if (selectedCustomer != null) {
					deleteCustomer(selectedCustomer);
				}
			} catch (FileNotFoundException | UnsupportedEncodingException ue) { // Could not read customers Analysis
																				// files
				ue.printStackTrace();
				popupInformation(messagesBundle.getString("error_when_reading_file")
						+ selectedCustomer.getFolder().getName(), messagesBundle.getString("error_title"));
			} catch (IOException ioe) {
				ioe.printStackTrace();
				popupInformation(messagesBundle.getString("error_when_deleting_file"),
						messagesBundle.getString("error_title"));
			}
		});
		// Listener to btnPrintPdf button
		btnPrintPdf.addActionListener(e -> {
			path = ReportPDF.createReport(app, EllipsisPanel.getBoxes());
			if (Desktop.isDesktopSupported() && path != null) {
				try {
					File myFile = new File(path);
					Desktop.getDesktop().open(myFile);
				} catch (IOException ex) {
					ex.printStackTrace();
				}
			}
		});
		// Listener to btnUser button
		btnUserManual.addActionListener(e -> {
			String workDir = System.getProperty("user.dir");
			String fileName = messagesBundle.getString("user_manual_filename");
			File manual_file = new File(workDir + File.separatorChar + fileName);

			if (Desktop.isDesktopSupported()) {
				if (!manual_file.exists()) {

					System.out.println("User manual file " + manual_file.getAbsolutePath() + " does not exist");
					popupInformation(messagesBundle.getString("manual_file_does_not_exist"),
							messagesBundle.getString("error_title"));
				}
				try {
					Desktop.getDesktop().open(manual_file);

				} catch (IOException ioe) {
					popupInformation(messagesBundle.getString("could_not_open_manual_file"),
							messagesBundle.getString("try_again_title"));
					System.out.println("Could not read user_manual");
				}
			}

		});

		// Create left side panel and add buttons
		JToolBar leftBar = new JToolBar();
		leftBar.setFloatable(false);
		leftBar.setLayout(new GridLayout());
		leftBar.add(btnNewCustomer);
		leftBar.add(btnNewAnalysis);
		leftBar.add(btnEdit);
		leftBar.add(btnPrintPdf);
		leftBar.add(btnUserManual);

		// Create right side panel and add content
		JToolBar rightBar = new JToolBar();
		rightBar.setFloatable(false);
		rightBar.setLayout(new GridBagLayout());

		GridBagConstraints constraints = new GridBagConstraints(0, 0, 1, 1, 1.0, 1.0, GridBagConstraints.CENTER,
				GridBagConstraints.BOTH, btnPrintPdf.getMargin(), 0, 0);

		rightBar.add(toolTipPanel, constraints);
		toolTipPanel.addComponents(getComponentsForToolTip());

		constraints.gridx = 1;
		constraints.weightx = 0.0;
		rightBar.add(btnDelete, constraints);

		JPanel bar = new JPanel(new BorderLayout());
		bar.add(leftBar, BorderLayout.WEST);
		bar.add(rightBar, BorderLayout.CENTER);

		bar.setMinimumSize(
				new Dimension((int) (leftBar.getPreferredSize().getWidth() + rightBar.getPreferredSize().getWidth()),
						(int) leftBar.getPreferredSize().getHeight()));
		return bar;
	}

	/**
	 * Determines which buttons need to be enabled or disabled.
	 * 
	 * @param selectedClass
	 *            Class of the object that was selected in the tree
	 */
	private void toggleButtonDisable(Class<?> selectedClass) {
		// Enable buttons when customer or analysis is selected
		if (selectedClass == Customer.class || selectedClass == Analysis.class) {
			btnNewAnalysis.setEnabled(true);
			btnEdit.setEnabled(true);
			btnDelete.setEnabled(true);
		}
		// Disable buttons when neither customer nor analysis is selected
		else {
			btnNewAnalysis.setEnabled(false);
			btnEdit.setEnabled(false);
			btnDelete.setEnabled(false);
		}
	}

	/**
	 * Handles checking and unchecking of Analysis. Adds and removes Analysis from
	 * report panel, accordingly.
	 * 
	 * @param analysis
	 *            Checked or unchecked Analysis
	 * @param isChecked
	 *            Whether it is currently checked
	 */
	private void handleCheckBoxChange(Analysis analysis, boolean isChecked, Customer customer) {
		if (isChecked) {
			// System.out.println(dirPanel.getActiveCustomer());
			if (customer != null)
				ellipsisPanel.addAnalysisToReport(analysis, customer);
			else
				ellipsisPanel.addAnalysisToReport(analysis, dirPanel.getActiveCustomer());
			statsPanel.addAnalysisToTable(analysis);
			toolTipPanel.updateAnalysisBoxes(EllipsisPanel.getBoxes());
		} else {
			ellipsisPanel.removeAnalysisFromReport(analysis);
			statsPanel.removeStatsFromTable(analysis);
		}

		// Set print pdf button enabled if there are some analyses on the report
		if (statsPanel.isEmpty()||ellipsisPanel.getCustomerCount()>1) {
			btnPrintPdf.setEnabled(false);
		} else {
			btnPrintPdf.setEnabled(true);
		}
	}

	/**
	 * Handles checking and unchecking of Customer.
	 * 
	 * @param customer
	 *            Checked or unchecked Customer
	 * @param isChecked
	 *            Whether it is currently checked
	 */
	private void handleCheckBoxChange(Customer customer, boolean isChecked) {
		List<Analysis> analysisList = new LinkedList<>();
		analysisList = customer.getAnalysisList();

		for (Analysis analysis : analysisList) {
			handleCheckBoxChange(analysis, isChecked, customer);
		}
	}

	/**
	 * Handles the creation of a new customer.
	 * 
	 * @author Ville Salo
	 */
	private void createNewCustomer() {
		boolean create = customerPopUp(null);

		if (create) {
			File newCustomerFolder = new File(workingDir.getAbsolutePath() + File.separatorChar + this.folderName);
			Customer createdCustomer = new Customer(newCustomerFolder, this.customerComment);
			try {
				createdCustomer.saveToFile(); // throws IOException if customer folder or json file cannot be created
			} catch (IOException e) {
				System.out.println(e.getMessage());
				popupInformation(messagesBundle.getString("error_when_saving_file"),
						messagesBundle.getString("try_again_title"));
				return;
			}

			dirPanel.addCustomerToTree(createdCustomer);
			searchPanel.addCustomer(createdCustomer);
			System.out.println("Created customer " + createdCustomer);

			// Ask if user wants to add new analysis & let him add one
			String question = messagesBundle.getString("confirm_add_analysis_to_customer") + " "
					+ createdCustomer.getFolder().getName() + "?";
			Object[] options = { "OK", messagesBundle.getString("cancel") };
			int addAnalysis = JOptionPane.showOptionDialog(this, question, messagesBundle.getString("confirm_title"),
					JOptionPane.OK_CANCEL_OPTION, JOptionPane.QUESTION_MESSAGE, null, options,
					JOptionPane.OK_CANCEL_OPTION);
			if (addAnalysis == JOptionPane.OK_OPTION) {
				createNewAnalysis(createdCustomer);
			}
		}
	}

	/**
	 * Handles the creation of a new analysis, including dialogue with user to get
	 * attributes. Adds the new analysis to the given customer.
	 * 
	 * @param customer
	 *            The customer which the new analysis is added to
	 * @return Analysis The Analysis object that was created
	 */
	private Analysis createNewAnalysis(Customer customer) {
		// Check that customer is not null
		if (customer == null)
			return null;

		Analysis newAnalysis = null;

		// Pop up a file chooser for the raw data file
		int returnVal = fileChooser.showOpenDialog(this);

		if (returnVal == JFileChooser.APPROVE_OPTION) { // If user clicked OK
			File selectedFile = fileChooser.getSelectedFile();
			System.out.println("Chosen raw data file: " + selectedFile.getAbsolutePath());

			if (selectedFile != null) {
				ArrayList<Double[]> coordinates = null;

				try {
					// Calculate coordinates from the chosen raw data file
					coordinates = new RawDataAnalyzer(fileChooser.getSelectedFile().getAbsolutePath()).getCoordList();
				} catch (IOException e) {
					System.out.println(e.getMessage());
					popupInformation(messagesBundle.getString("error_when_reading_raw_data"),
							messagesBundle.getString("try_again_title"));
					return null;
				}

				// Generate (hopefully) unique filename
				String fileName = "" + System.currentTimeMillis();
				File newAnalysisFolder = new File(customer.getFolder().getAbsolutePath() + File.separator + fileName);

				// Pop up a frame to ask Analysis details from user
				boolean userAddedAnalysis;
				userAddedAnalysis = addAnalysisPopUp(customer, selectedFile.getName());
				System.out.println(
						"Pop up exited with: " + stance + ", " + eyes + ", " + date + ", " + analysisComment + ", " + measurer);
				
				if (userAddedAnalysis) {
					// Generate path for this new Analysis file in Customer's directory
					boolean folderCreated = newAnalysisFolder.mkdir();

					if (!folderCreated) {
						System.out.println("Could not create analysis folder " + newAnalysisFolder.getAbsolutePath());
						popupInformation(messagesBundle.getString("error_when_saving_file"),
								messagesBundle.getString("try_again_title"));
					} else {
						System.out.println("Created folder for analysis: " + newAnalysisFolder.getAbsolutePath());
						try {
							// Create Analysis object with chosen attributes, coordinates and stats
							newAnalysis = new Analysis(newAnalysisFolder, date, stance, eyes, coordinates,
									analysisComment, measurer);
							newAnalysis.saveToFile(); // throws IOException
							System.out.println("Created analysis " + newAnalysis.toString());

							customer.addAnalysis(newAnalysis);
							dirPanel.addAnalysisToTree(customer, newAnalysis);
						} catch (IOException e) {
							System.out.println("Could not save Analysis files to " + newAnalysisFolder.getAbsolutePath());
							popupInformation(messagesBundle.getString("error_when_saving_file"),
									messagesBundle.getString("try_again_title"));
						}
					}
				}
			}
		}
		return newAnalysis;
	}

	/**
	 * Handles the updating of customer details.
	 * @param customer
	 *            The Customer to be updated
	 */
	private void editCustomer(Customer customer) {
		if (customer == null)
			return;

		if (customerPopUp(customer)) {
			// Check what changed
			boolean commentModified = false;
			try {
				commentModified = this.customerComment.compareTo(customer.getComment()) != 0;
			} catch (IOException | org.json.simple.parser.ParseException e) {
				System.out.println("Couldn't read comment from customer " + customer);
				e.printStackTrace();
			}
			System.out.println("comment modified: " + commentModified);
			boolean folderModified = this.folderName.compareTo(customer.getFolder().getName()) != 0;
			System.out.println("folder modified: " + folderModified);

			if (commentModified) {
				try {
					customer.setComment(this.customerComment); // throws IOException
					commentPanel.updateComment(customer); // throws ParseException
				} catch (IOException e) {
					e.printStackTrace();
					popupInformation(messagesBundle.getString("error_when_saving_file"),
							messagesBundle.getString("try_again_title"));
				} catch (org.json.simple.parser.ParseException e) {
					System.out.println("Couldn't read comment from customer " + customer);
					e.printStackTrace();
				}
			}
			if (folderModified) {
				String newFolderPath = customer.getFolder().getParentFile().getAbsolutePath() + File.separator
						+ this.folderName;
				File newFolder = new File(newFolderPath);
				try {
					customer.setFolder(newFolder); // throws IOException
					dirPanel.updateCustomer(customer);
					ellipsisPanel.updateCustomer(customer);

				} catch (IOException e) {
					e.printStackTrace();
					popupInformation(messagesBundle.getString("error_when_saving_file"),
							messagesBundle.getString("try_again_title"));
				}
			}
		}

		this.customerComment = null;
		this.folderName = null;
	}

	/**
	 * Handles the updating of analysis details.
	 * 
	 * @param analysis
	 *            The analysis to be updated
	 */
	private void editAnalysis(Analysis analysis, Customer customer) throws IOException {
		if (analysis == null || customer == null)
			return;

		if (editAnalysisPopUp(analysis, customer)) {

			boolean commentsModified = this.analysisComment.compareTo(analysis.getcomments()) != 0;
			System.out.println("comments Modified: " + commentsModified);
			boolean eyesModified = this.eyes.compareTo(analysis.getEyes()) != 0;
			System.out.println("eyes Modified: " + eyesModified);
			boolean stanceModified = this.stance.getStance().compareTo(analysis.getStance().getStance()) != 0;
			System.out.println("Stance Modified: " + stanceModified);
			boolean dateModified = this.date.compareTo(analysis.getDate()) != 0;
			System.out.println("date Modified: " + dateModified);
			boolean measurerModified = this.measurer.compareTo(analysis.getMeasurer()) != 0;
			System.out.println("measurer modifoed: " + measurerModified);

			if (commentsModified) {
				analysis.setcomments(this.analysisComment);
			}
			if (eyesModified) {
				analysis.setEyes(this.eyes);
			}
			if (dateModified) {
				analysis.setDate(this.date);
			}
			if (measurerModified) {
				analysis.setMeasurer(this.measurer); // needs method in Analysis
			}
			if (stanceModified) {
				System.out.println("Testing WHAT CAME OUT" + this.stance.getStance());
				analysis.setStance(this.stance);
				analysis.drawChartToFile();
			}
			if (eyesModified || stanceModified || dateModified || measurerModified) {
				this.dirPanel.updateAnalysis(customer, analysis);
			}
			if (commentsModified || eyesModified || stanceModified || dateModified || measurerModified) {
				analysis.saveDetailsJson();
				this.ellipsisPanel.updateView(analysis);
			}

		}
	}

	/**
	 * Handles the deletion of customer's folder from computer and removing customer
	 * from tree.
	 * 
	 * @param customer
	 *            The Customer to delete
	 */
	private void deleteCustomer(Customer customer) {
		if (customer == null) {
			System.out.println("Cannot delete null customer");
			return;
		}
		String messagetxt = messagesBundle.getString("confirm_delete_customer") + " " + customer;
		String messagetxt2 = "";
		String messagetxt3 = "";
		JCheckBox deleteAllChk = new JCheckBox(messagesBundle.getString("checkbox_delete_customer"));

		// Check if customer has analyses
		if (customer.getAnalysisList().size() > 0) {
			messagetxt = messagesBundle.getString("customer_has_analyses") + "\n";
			messagetxt2 = messagesBundle.getString("confirm_delete_customer_with_analyses") + " " + customer + "\n";
			messagetxt3 = messagesBundle.getString("and_delete_all_analyses");
			deleteAllChk = new JCheckBox(messagesBundle.getString("checkbox_delete_customer_with_analyses") + " ("
					+ customer.getAnalysisList().size() + ")");
			// msgContent = new Object[] { message, deleteAllChk };
		}

		// Variable to pass the option that user selected
		final MutableInt option = new MutableInt();
		final JLabel message = new JLabel(messagetxt);
		final JLabel message2 = new JLabel(messagetxt2);
		final JLabel message3 = new JLabel(messagetxt3);
		// Button for saving the data with its listener
		final JButton okay = new JButton(messagesBundle.getString("delete"));
		okay.addActionListener(e -> {
			JOptionPane pane = getOptionPane((JComponent) e.getSource());
			// here we tell popup buttons listener that ok was clicked
			pane.setValue(JOptionPane.OK_OPTION);
			// and here we set ok selected
			option.setValue(JOptionPane.OK_OPTION);
		});
		// Here we set it disabled (gray) first
		okay.setEnabled(false);

		// Cancel button with listener
		final JButton cancel = new JButton(messagesBundle.getString("cancel"));
		cancel.addActionListener(e -> {
			JOptionPane pane = getOptionPane((JComponent) e.getSource());
			pane.setValue(JOptionPane.CANCEL_OPTION);
			option.setValue(JOptionPane.CANCEL_OPTION);
		});

		// Jpanel which holds the elements of the pop up together
		JPanel pane = new JPanel();

		// We set layout to null to be able to set individual elements as we wish
		pane.setLayout(new BoxLayout(pane, BoxLayout.PAGE_AXIS));

		// Size of the pop up
		pane.setPreferredSize(new Dimension(450, 100));

		// also listener for checkbox
		deleteAllChk.addActionListener(e -> {

			if (!okay.isEnabled()) {

				// we can enable ok button
				okay.setEnabled(true);
			} else
				okay.setEnabled(false);
		});

		pane.add(message);
		pane.add(message2);
		pane.add(message3);
		pane.add(deleteAllChk);
		// pop up is prepared so we show it to user
		JOptionPane.showOptionDialog(this, pane, customer.toString(), JOptionPane.YES_NO_OPTION,
				JOptionPane.WARNING_MESSAGE, null, new Object[] { okay, cancel }, okay);

		// Object[] msgContent = { message, deleteAllChk };

		// Pop up a confirmation so that user can check that the current selected
		// customer is the right one
		// int confirmed = popupYesNo(msgContent,
		// messagesBundle.getString("cannot_be_undone_title"));
		boolean deleteAllAnalyses = deleteAllChk.isSelected();
		if (option.getValue() == JOptionPane.OK_OPTION && deleteAllAnalyses) { // If yes
			try {
				customer.deleteFiles(); // throws IOException
				System.out.println("Deleted customer " + customer);

				List<Analysis> analysisToRemove = customer.getAnalysisList();
				ellipsisPanel.removeAnalysisFromReport(analysisToRemove);
				statsPanel.removeStatsFromTable(analysisToRemove);
				dirPanel.removeCustomerFromTree(customer);
				searchPanel.removeCustomer(customer);
			} catch (IOException e) {
				System.out.println("Failed to delete customer " + customer);
				e.printStackTrace();
				popupInformation(messagesBundle.getString("error_when_deleting_file"),
						messagesBundle.getString("error_title"));
			}
		}
	}

	/**
	 * Handles the deletion of analysis files from computer.
	 * 
	 * @param customer
	 *            The Customer whose analysis is to be deleted
	 * @param analysis
	 *            The Analysis to be deleted
	 * @throws FileNotFoundException,
	 *             UnsupportedEncodingException, IOException
	 */
	private void deleteAnalysis(Customer customer, Analysis analysis)
			throws IOException {
		if (analysis == null || customer == null) {
			System.out.println("Cannot delete analysis " + analysis + " from customer " + customer);
			return;
		}
		LinkedList<Analysis> allAnalysisOfCustomer = null;
		allAnalysisOfCustomer = customer.getAnalysisList(); // throws FileNotFoundException or
															// UnsupportedEncodingException
		if (!allAnalysisOfCustomer.contains(analysis)) {
			System.out.println("Customer " + customer + " doesn't have analysis " + analysis + " to delete");
			return;
		}

		// Pop up a confirmation so that user can check that the current selected
		// analysis is the right one
		DateFormat dateFormat = DateFormat.getDateInstance(DateFormat.FULL, getLocale());
		String date = dateFormat.format(analysis.getDate());

		String messagetxt = messagesBundle.getString("confirm_delete_analysis") + "\n";
		String strBuilder = date + " " + messagesBundle.getString(analysis.getStance().getStance()) +
				" / " + messagesBundle.getString(analysis.getEyes());
		String messagetxt2 = strBuilder + "\n";
		String messagetxt3 = messagesBundle.getString("from_customer") + " " + customer + "?";

		JCheckBox deleteAllChk = new JCheckBox(messagesBundle.getString("checkbox_delete_analysis"));

		// Variable to pass the option that user selected
		final MutableInt option = new MutableInt();
		final JLabel message = new JLabel(messagetxt);
		final JLabel message2 = new JLabel(messagetxt2);
		final JLabel message3 = new JLabel(messagetxt3);

		// Button for saving the data with its listener
		final JButton okay = new JButton(messagesBundle.getString("delete"));
		okay.addActionListener(e -> {
			JOptionPane pane = getOptionPane((JComponent) e.getSource());
			// here we tell popup buttons listener that ok was clicked
			pane.setValue(JOptionPane.OK_OPTION);
			// and here we set ok selected
			option.setValue(JOptionPane.OK_OPTION);
		});
		// Here we set it disabled (gray) first
		okay.setEnabled(false);

		// Cancel button with listener
		final JButton cancel = new JButton(messagesBundle.getString("cancel"));
		cancel.addActionListener(e -> {
			JOptionPane pane = getOptionPane((JComponent) e.getSource());
			pane.setValue(JOptionPane.CANCEL_OPTION);
			option.setValue(JOptionPane.CANCEL_OPTION);
		});

		// Jpanel which holds the elements of the pop up together
		JPanel pane = new JPanel();

		// We set layout to null to be able to set individual elements as we wish
		pane.setLayout(new BoxLayout(pane, BoxLayout.PAGE_AXIS));

		// Size of the pop up
		pane.setPreferredSize(new Dimension(450, 100));

		// also listener for checkbox
		deleteAllChk.addActionListener(e -> {

			if (!okay.isEnabled()) {
				// we can enable ok button
				okay.setEnabled(true);
			} else
				okay.setEnabled(false);
		});

		pane.add(message);
		pane.add(message2);
		pane.add(message3);
		pane.add(deleteAllChk);
		// pop up is prepared so we show it to user
		JOptionPane.showOptionDialog(this, pane, customer.toString(), JOptionPane.YES_NO_OPTION,
				JOptionPane.WARNING_MESSAGE, null, new Object[] { okay, cancel }, okay);

		boolean deleteAnalysis = deleteAllChk.isSelected();
		if (option.getValue() == JOptionPane.OK_OPTION && deleteAnalysis) { // If yes
			try {
				customer.removeSingleAnalysis(analysis); // throws IOException
				System.out.println("Deleted analysis " + analysis + " from customer " + customer);
				ellipsisPanel.removeAnalysisFromReport(analysis);
				statsPanel.removeStatsFromTable(analysis);
				dirPanel.removeAnalysisFromTree(customer, analysis);
			} catch (IOException e) {
				System.out.println("Failed to delete analysis " + analysis + " from customer " + customer);
				e.printStackTrace();
				popupInformation(messagesBundle.getString("error_when_deleting_file"),
						messagesBundle.getString("error_title"));
			}
		}		
	}

	/**
	 * Pop up an information window.
	 * 
	 * @param message
	 *            The message to user
	 * @param title
	 *            Title for the pop up window
	 */
	private void popupInformation(String message, String title) {
		JOptionPane.showMessageDialog(getContentPane(), message, title, JOptionPane.INFORMATION_MESSAGE);
	}

	/**
	 * Pop up for adding required information when editing analysis
	 * 
	 * @param analysis
	 *            this is the analysis which is being modified
	 * @return whether user changed some value
	 * @author Eetu Kyyro
	 */
	private boolean editAnalysisPopUp(Analysis analysis, Customer customer) {
		return analysisPopUp(analysis.getcomments(), analysis.getEyes(), analysis.getStance(), analysis.getDate(),
				customer, analysis.getJsonFile().getAbsolutePath(), analysis.getMeasurer());
	}

	/**
	 * Pop up for adding required information when adding new analysis for customer
	 * 
	 * @param customer
	 *            this is the customer for whom the analysis is being added
	 * @return whether user changed some value
	 * @author Juha Korvenaho, Eetu Kyyrö
	 */
	private boolean addAnalysisPopUp(Customer customer, String pathToFile) {
		return analysisPopUp(null, null, null, null, customer, pathToFile, null);
	}

	private JOptionPane getOptionPane(JComponent parent) {
		JOptionPane pane = null;
		if (!(parent instanceof JOptionPane)) {
			pane = getOptionPane((JComponent) parent.getParent());
		} else {
			pane = (JOptionPane) parent;
		}
		return pane;
	}

	/**
	 * Generic jpanel for editing Analysis information
	 * 
	 * @return whether user changed some value
	 * @author Juha Korvenaho, Eetu Kyyro
	 */
	private boolean analysisPopUp(String pcomment, String peyes, Stance pstance, Date pdate, Customer customer,
			String pathToFile, String pmeasurer) {

		// Variable to pass the option that user selected
		final MutableInt option = new MutableInt(JOptionPane.CANCEL_OPTION);
		// Variables needed for listeners to check if they should enable ok button or not
		// final AtomicBoolean eyesSelected = new AtomicBoolean(false);
		// final AtomicBoolean stanceSelected = new AtomicBoolean(false);
		final AtomicBoolean measurerAdded = new AtomicBoolean(false);
		final AtomicBoolean dayAcceptable = new AtomicBoolean(true);

		// BUTTONS
		
		// Button for saving the data with its listener
		final JButton okay = new JButton(messagesBundle.getString("save"));
		okay.addActionListener(e -> {
			JOptionPane pane = getOptionPane((JComponent) e.getSource());
			// here we tell popup buttons listener that ok was clicked
			pane.setValue(JOptionPane.OK_OPTION);
			// and here we set ok selected
			option.setValue(JOptionPane.OK_OPTION);
		});
		// Here we set it disabled (gray) first
		okay.setEnabled(false);

		// Cancel button with listener
		final JButton cancel = new JButton(messagesBundle.getString("cancel"));
		cancel.addActionListener(e -> {
			JOptionPane pane = getOptionPane((JComponent) e.getSource());
			pane.setValue(JOptionPane.CANCEL_OPTION);
			option.setValue(JOptionPane.CANCEL_OPTION);
		});

		// Format of the dateString
		DateFormat df = DateFormat.getDateInstance();

		// LABELS AND TEXTFIELDS
		
		// Jpanel which holds the elements of the pop up together
		JPanel pane = new JPanel();
		// We set layout to null to be able to set individual elements as we wish
		pane.setLayout(null);
		pane.setPreferredSize(new Dimension(500, 280)); // Size of the pop up

		// Comments section
		// These are labels which are shown in black text
		JLabel chosenFile;
		JLabel chosenCustomer;
		JLabel lblPathToFile;

		// If we didn't get parameter for eyes we know we are creating new customer
		if (peyes == null) {
			// This is label for user to know he is adding new analysis
			chosenFile = new JLabel(messagesBundle.getString("add_analysis_label"));
			chosenFile.setBounds(0, 0, 160, 25); // Location of the label
			// Filename of the analysis
			lblPathToFile = new JLabel(pathToFile);
			lblPathToFile.setBounds(170, 0, 300, 25); // Location of the filename
			pane.add(lblPathToFile);
			// Here we got eyes as parameter and set different label to announce we are
			// editing something
		} else {
			// label to announce we are editing something
			chosenFile = new JLabel(messagesBundle.getString("edit_customer_label"));
			chosenFile.setBounds(0, 0, 250, 25); // location for label
			// name of the customer whose analysis we are editing
			chosenCustomer = new JLabel(customer.toString());
			chosenCustomer.setBounds(260, 0, 300, 25); // location of the customer name
			pane.add(chosenCustomer);
		}
		// filename is added to panel
		pane.add(chosenFile);

		// label for comment field
		JLabel lblComment = new JLabel(messagesBundle.getString("analysis_comment_label"));
		lblComment.setBounds(0, 30, 160, 25); // location of the label
		pane.add(lblComment);

		// this is the actual commentfield
		JTextArea commentField = new JTextArea();
		commentField.addKeyListener(new KeyAdapter() {
			public void keyTyped(KeyEvent e) {
				if (commentField.getText().length() >= 100) {
					e.consume();
					commentField.setText(commentField.getText().substring(0, 100));
					java.awt.Toolkit.getDefaultToolkit().beep();
				}
			}
		});
		commentField.addKeyListener(new KeyListener() {
			@Override
			public void keyTyped(KeyEvent e) {
			}
			@Override
			public void keyReleased(KeyEvent e) {
			}
			@Override
			public void keyPressed(KeyEvent e) {
 
				if (e.getKeyCode() == KeyEvent.VK_ENTER || e.getKeyCode() == KeyEvent.VK_TAB) {
					e.consume();
					java.awt.Toolkit.getDefaultToolkit().beep();
				}
			}
		});
		commentField.setLineWrap(true);
		commentField.setWrapStyleWord(true);
		JScrollPane scrollField = new JScrollPane(commentField);
		scrollField.setBounds(170, 35, 300, 35); // its location
		pane.add(scrollField);

		// Eyes label
		JLabel lblEyes = new JLabel(messagesBundle.getString("eyes_label"));
		lblEyes.setBounds(0, 80, 160, 25); // eyes label location
		pane.add(lblEyes);

		// combobox for setting the eyes
		JComboBox<String> comboEyes = new JComboBox<>();
		comboEyes.setBackground(Color.WHITE);
		comboEyes.setBounds(170, 80, 300, 25); // location of combobox
		// contents of combobox taken from messages.properties file and added to
		// specific location
		comboEyes.setModel(
				new DefaultComboBoxModel<>(new String[]{messagesBundle.getString(Analysis.getEyesByIndex(0)),
						messagesBundle.getString(Analysis.getEyesByIndex(1)),
						messagesBundle.getString(Analysis.getEyesByIndex(2))}));

		// Combobox for setting stance needed to be declared here because it is used in
		// listener for eyes
		JComboBox<String> comboStance = new JComboBox<>();

		// also listener for eyes
		comboEyes.addActionListener(e -> {
			// first we set boolean to true to know later that eyes are modified
			// eyesSelected.set(true);
			// the if stance is also selected
			if (comboStance.getSelectedIndex() != 0 && measurerAdded.get() && dayAcceptable.get()) {
				// we can enable ok button
				okay.setEnabled(true);
			}
			// if user selects zero from eyes or stance
			if (comboEyes.getSelectedIndex() == 0 || comboStance.getSelectedIndex() == 0
					|| !dayAcceptable.get()) {
				// we disable ok button again
				okay.setEnabled(false);
			}
		});
		// eyes selecter is added to popup
		pane.add(comboEyes);

		// Stance has pretty much same things as eyes
		JLabel lblStance = new JLabel(messagesBundle.getString("stance_label"));
		lblStance.setBounds(0, 115, 160, 25); // label location
		pane.add(lblStance);

		// combobox being created and names of stances fetch from message properties
		comboStance.setBackground(Color.WHITE);
		comboStance.setBounds(170, 115, 300, 25);
		comboStance.setModel(new DefaultComboBoxModel<>(new String[]{messagesBundle.getString("0_choose"),
				messagesBundle.getString(new Stance(1).getStance()),
				messagesBundle.getString(new Stance(2).getStance()),
				messagesBundle.getString(new Stance(3).getStance()),
				messagesBundle.getString(new Stance(4).getStance()),
				messagesBundle.getString(new Stance(5).getStance()),
				messagesBundle.getString(new Stance(6).getStance())}));

		// adding listener to stance selector too
		comboStance.addActionListener(e -> {
			// stanceSelected.set(true);
			if (comboEyes.getSelectedIndex() != 0 && measurerAdded.get() && dayAcceptable.get()) {
				okay.setEnabled(true);
			}
			if (comboStance.getSelectedIndex() == 0 || comboEyes.getSelectedIndex() == 0
					|| !dayAcceptable.get()) {
				okay.setEnabled(false);
			}
		});
		pane.add(comboStance);

		// date label and its location being added to popup
		JLabel lblDate = new JLabel(messagesBundle.getString("date_label"));
		lblDate.setBounds(0, 150, 160, 25);
		pane.add(lblDate);

		// textfield for calendar and its location

		dateField = new CalendarObject();
		dateField.setBounds(170, 150, 300, 25);
		dateField.setBackground(Color.WHITE);
		dateField.getDocument().addDocumentListener(new DocumentListener() {
			public void changedUpdate(DocumentEvent e) {
				// warn();
			}

			public void insertUpdate(DocumentEvent e) {
				warn();
			}

			void warn() {
				Date userSelected = ((CalendarObject) dateField).getDate();

				if (Objects.requireNonNull(userSelected).after(new Date())) {
					JOptionPane.showMessageDialog(getRootPane(), messagesBundle.getString("future_date_warning"),
							messagesBundle.getString("future_date_warning_header"), JOptionPane.INFORMATION_MESSAGE);

					okay.setEnabled(false);
					dayAcceptable.set(false);
				} else {
					dayAcceptable.set(true);
					if (comboStance.getSelectedIndex() != 0 && comboEyes.getSelectedIndex() != 0
							&& measurerAdded.get()) {
						okay.setEnabled(true);
					}
				}
			}

			@Override
			public void removeUpdate(DocumentEvent arg0) {
 
			}
		});
		pane.add(dateField);

		// label for measurer field
		JLabel lblMeasurer = new JLabel(messagesBundle.getString("analysis_measurer_label"));
		lblMeasurer.setBounds(0, 195, 160, 25);	// location of the label
		pane.add(lblMeasurer);

		// this is the actual measurer field
		JTextField measurerField = new JTextField();
		measurerField.getDocument().addDocumentListener(new DocumentListener() {
			public void changedUpdate(DocumentEvent e) {
				measure();
			}
			public void insertUpdate(DocumentEvent e) {
				measure();
			}
			public void removeUpdate(DocumentEvent e) {
				measure();
			}
			private void measure() {
				if (measurerField.getText().trim().length() >= 2) {
					measurerAdded.set(true);
					if (comboEyes.getSelectedIndex() != 0 && comboStance.getSelectedIndex() != 0
							&& dayAcceptable.get()) {
						okay.setEnabled(true);
					}
				}
				if (measurerField.getText().trim().length() < 2) {
					measurerAdded.set(false);
					okay.setEnabled(false);
				}
				if (comboStance.getSelectedIndex() == 0 || comboEyes.getSelectedIndex() == 0
						|| !dayAcceptable.get()) {
					okay.setEnabled(false);
				}
			}
		});
		measurerField.addKeyListener(new KeyAdapter() {
			public void keyTyped(KeyEvent e) {
				if (measurerField.getText().length() >= 50) {
					e.consume();
					measurerField.setText(measurerField.getText().substring(0, 50));
					java.awt.Toolkit.getDefaultToolkit().beep();
				}
			}
		});
		measurerField.setBounds(170, 195, 300, 25);
		pane.add(measurerField);

		// SET VALUES
		
		if (pcomment != null && pcomment.length() > 0) {
			commentField.setText(pcomment);
		}
		
		// if eyes is not null (received as parameter) we set correct index selected
		if (peyes != null) {
			okay.setEnabled(true);
			switch (peyes) {
			case "1_eyes_open":
				comboEyes.setSelectedIndex(1);
				break;
			case "2_eyes_closed":
				comboEyes.setSelectedIndex(2);
				break;
			}
		}
		// here we set selected those eyes we got as parameter
		comboEyes.setSelectedIndex(Analysis.getEyesIndex(peyes));

		// if stance got as parameter was null we set first index selected
		if (pstance == null) {
			comboStance.setSelectedIndex(0);
		} else {
			// else we set selected the item which was gotten as parameter
			comboStance.setSelectedIndex(pstance.getIndex());
		}
		
		// if we received some date we set it to datefield
		if (pdate != null) {
			dateField.setText(df.format(pdate));
		} else {
			// else we use default value
			dateField.setText(df.format(new Date()));
		}

		if (pmeasurer != null && pmeasurer.length() > 0) {
			measurerField.setText(pmeasurer);
		}

		// TEXTPROMPTS
		
		TextPrompt commentPrompt = new TextPrompt(messagesBundle.getString("comment_textprompt"), commentField);
		commentPrompt.setForeground(Color.DARK_GRAY);
		// cant recall what this does but tied to comment hint :-D
		commentPrompt.changeAlpha(0.5f);
		commentPrompt.changeStyle(Font.BOLD + Font.ITALIC);
		
		TextPrompt measurerPrompt = new TextPrompt(messagesBundle.getString("measurer_textprompt"), measurerField);
		measurerPrompt.setForeground(Color.DARK_GRAY);
		// cant recall what this does but tied to measurer hint :-D
		measurerPrompt.changeAlpha(0.5f);
		measurerPrompt.changeStyle(Font.BOLD + Font.ITALIC);
		
		
		// pop up is prepared so we show it to user
		JOptionPane.showOptionDialog(this, pane, customer.toString(), JOptionPane.YES_NO_OPTION,
				JOptionPane.PLAIN_MESSAGE, null, new Object[] { okay, cancel }, okay);

		// here we use set option to save changes if there are any
		if (option.getValue() == JOptionPane.OK_OPTION) {

			// if comment field text is longer than 100 characters
			if (commentField.getText().trim().length() > 100) {
				// we extract only 100 characters from it
				analysisComment = commentField.getText().trim().substring(0, 99);
			} else {
				// else we use whole comment
				analysisComment = commentField.getText().trim();
			}

			// here get chosen index from stance selector
			int chosenStance = comboStance.getSelectedIndex();
			// and set it to class variable
			this.stance = new Stance(chosenStance);

			// here we get chose index for eyes
			int chosedEyes = comboEyes.getSelectedIndex();
			// and set it to class variable
			this.eyes = Analysis.getEyesByIndex(chosedEyes);

			// here we get value of datefield
			String strDate = dateField.getText();
			try {
				date = df.parse(strDate);
			} catch (ParseException e) {
				e.printStackTrace();
			}

			if (measurerField.getText().trim().length() > 100) {
				// we extract only 100 characters from it
				this.measurer = measurerField.getText().trim().substring(0, 99);
			} else {
				// else we use whole measurer
				this.measurer = measurerField.getText().trim();
			}

			// here we create that later popup which shows what will be saved
			pane = new JPanel();
			// layout for popup
			pane.setLayout(new BoxLayout(pane, BoxLayout.PAGE_AXIS));
			// label for comment and comment itself
			pane.add(new JLabel(messagesBundle.getString("analysis_comment_label") + ": " + analysisComment));
			// label for stance and selected stance
			pane.add(new JLabel(
					messagesBundle.getString("stance_label") + ": " + comboStance.getSelectedItem().toString()));
			// label for eyes and selected eyes
			pane.add(
					new JLabel(messagesBundle.getString("eyes_label") + ": " + comboEyes.getSelectedItem().toString()));
			// and label for date and chosen date
			pane.add(new JLabel(messagesBundle.getString("date_label") + ": " + dateField.getText()));

			pane.add(new JLabel(messagesBundle.getString("analysis_measurer_label") + ": " + this.measurer));

			// and when all is added to popup we can show it to user
			JOptionPane.showMessageDialog(this, pane, messagesBundle.getString("show_input_to_be_saved"),
					JOptionPane.PLAIN_MESSAGE);

			// return true value so other methods know that we changed something
			return true;

		} else {
			// if we didn't change anything we return false
			return false;
		}
	}

	/**
	 * Generic jpanel for editing Customer information
	 * 
	 * @param customer
	 * @return whether user changed some value
	 */
	private boolean customerPopUp(Customer customer) {
		// Variable to pass the option that user selected
		final MutableInt option = new MutableInt(JOptionPane.CANCEL_OPTION);

		// BUTTONS
		
		// Button for saving the data with its listener
		final JButton okay = new JButton(messagesBundle.getString("save"));
		okay.addActionListener(e -> {
			JOptionPane pane = getOptionPane((JComponent) e.getSource());
			// here we tell popup buttons listener that ok was clicked
			pane.setValue(JOptionPane.OK_OPTION);
			option.setValue(JOptionPane.OK_OPTION);
		});
		okay.setEnabled(false);

		// Cancel button with listener
		final JButton cancel = new JButton(messagesBundle.getString("cancel"));
		cancel.addActionListener(e -> {
			JOptionPane pane = getOptionPane((JComponent) e.getSource());
			pane.setValue(JOptionPane.CANCEL_OPTION);
			option.setValue(JOptionPane.CANCEL_OPTION);
		});


		// Jpanel which holds the elements of the pop up together
		JPanel pane = new JPanel();
		// We set layout to null to be able to set individual elements as we wish
		pane.setLayout(null);
		pane.setPreferredSize(new Dimension(500, 85));

		// LABELS AND TEXTFIELDS

		// Label for customer ID
		JLabel input = new JLabel(messagesBundle.getString("customer_id_label"));
		input.setBounds(0, 0, 160, 25); // Location of label
		pane.add(input);

		// Field for customer ID
		JTextField idField = new JTextField();
		idField.getDocument().addDocumentListener(new DocumentListener() {
			public void changedUpdate(DocumentEvent e) {
				measure();
			}
			public void insertUpdate(DocumentEvent e) {
				measure();
			}
			public void removeUpdate(DocumentEvent e) {
				measure();
			}
			private void measure() {
				if (idField.getText().trim().length() >= 3) {
					okay.setEnabled(true);
				}
				if (idField.getText().trim().length() < 3) {
					okay.setEnabled(false);
				}
			}
		});
		idField.addKeyListener(new KeyAdapter() {
			public void keyTyped(KeyEvent e) {
				if (idField.getText().length() >= 40) {
					e.consume();
					idField.setText(idField.getText().substring(0, 40));
					java.awt.Toolkit.getDefaultToolkit().beep();
				}
			}
		});
		idField.setBounds(170, 0, 300, 25);
		idField.requestFocus();
		pane.add(idField);

		// Label for comment
		JLabel lblComment = new JLabel(messagesBundle.getString("customer_comment_label"));
		lblComment.setBounds(0, 45, 160, 25); // location of the label
		pane.add(lblComment);

		// Field for comment
		JTextArea commentField = new JTextArea();
		JScrollPane scrollField = new JScrollPane(commentField);
		// Limit characters to 100
		commentField.addKeyListener(new KeyAdapter() {
			public void keyTyped(KeyEvent e) {
				if (commentField.getText().length() >= 100) {
					e.consume();
					commentField.setText(commentField.getText().substring(0, 100));
					java.awt.Toolkit.getDefaultToolkit().beep();
				}
			}
		});
		commentField.addKeyListener(new KeyListener() {
			@Override
			public void keyTyped(KeyEvent e) {
			}
			@Override
			public void keyReleased(KeyEvent e) {
			}
			@Override
			public void keyPressed(KeyEvent e) {
 
				if (e.getKeyCode() == KeyEvent.VK_ENTER || e.getKeyCode() == KeyEvent.VK_TAB) {
					e.consume();
					java.awt.Toolkit.getDefaultToolkit().beep();
				}
			}
		});
		scrollField.setBounds(170, 40, 300, 35);
		commentField.setLineWrap(true);
		commentField.setWrapStyleWord(true);
		pane.add(scrollField);
		
		// TEXTPROMPTS
		TextPrompt idPrompt = new TextPrompt(messagesBundle.getString("customer_id_textprompt"), idField);
		idPrompt.setForeground(Color.DARK_GRAY);
		idPrompt.changeAlpha(0.5f);
		idPrompt.changeStyle(Font.BOLD + Font.ITALIC);
		
		TextPrompt commentPrompt = new TextPrompt(messagesBundle.getString("comment_textprompt"), commentField);
		commentPrompt.setForeground(Color.DARK_GRAY);
		commentPrompt.changeAlpha(0.5f);
		commentPrompt.changeStyle(Font.BOLD + Font.ITALIC);

		// SET VALUES TO FIELDS
		String strTitle = messagesBundle.getString("add_customer");
		String pFolderName = "";
		String pComment = "";

		// If we're editing an existing customer
		if (customer != null) {
			pFolderName = customer.getFolder().getName();
			strTitle = messagesBundle.getString("modify_customer");
			try {
				pComment = customer.getComment();
			} catch (IOException | org.json.simple.parser.ParseException e) {
				e.printStackTrace();
				System.out.println("Couldn't read customer " + customer);
			}
		}
		if (pFolderName != null && pFolderName.length() > 0) {
			idField.setText(pFolderName);
		}
		if (pComment != null && pComment.length() > 0) {
			commentField.setText(pComment);
		}

		// Pop up is prepared so we show it to user
		Object[] customButtons = { okay, messagesBundle.getString("cancel") };
		JOptionPane.showOptionDialog(this, pane, strTitle, JOptionPane.OK_CANCEL_OPTION,
				JOptionPane.PLAIN_MESSAGE, null, customButtons, okay);

		if (option.getValue() == JOptionPane.OK_OPTION) {
			this.folderName = idField.getText().trim();
			this.customerComment = commentField.getText().trim();
			return true;
		} else {
			return false;
		}
	}
}
