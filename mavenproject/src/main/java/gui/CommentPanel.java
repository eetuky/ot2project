package gui;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.io.IOException;
import java.util.Locale;
import java.util.ResourceBundle;

import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;

import org.json.simple.parser.ParseException;

import domain.Customer;

class CommentPanel extends JPanel {

	/**
	 * Panel that displays the customer comment.
	 * 
	 * @author Teemu Savorinen
	 */
	private static final long serialVersionUID = -3374192551504353248L;
	private final ResourceBundle messages;
	private static final JButton saveAndEditBtn = new JButton();
	private static final JTextArea textField = new JTextArea();
	private static final JLabel title = new JLabel();
	private static JScrollPane scrollComment;
	private Customer customer;
	private String currentComment;
	private String savedComment;

	/**
	 * Constructor.
	 */
	public CommentPanel() {
		this.messages = ResourceBundle.getBundle("Messages", Locale.getDefault());
		saveAndEditBtn.setBackground(Color.LIGHT_GRAY);
		textField.setEnabled(false);
		textField.setBackground(Color.LIGHT_GRAY);
		scrollComment = new JScrollPane(textField, JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED,
				JScrollPane.HORIZONTAL_SCROLLBAR_NEVER);
		saveAndEditBtn.setText(messages.getString("save"));
		title.setText(messages.getString("customer_comment_label"));
		this.setLayout(new BorderLayout());
		textField.setBorder(BorderFactory.createLineBorder(Color.DARK_GRAY));
		textField.setLineWrap(true);
		textField.setWrapStyleWord(true);
		this.add(title, BorderLayout.PAGE_START);
		this.add(scrollComment, BorderLayout.CENTER);
		this.add(saveAndEditBtn, BorderLayout.PAGE_END);
		saveAndEditBtn.setEnabled(false);
		title.setEnabled(false);
		scrollComment.setEnabled(false);
		saveAndEditBtn.addActionListener(e -> saveComment());
		textField.addKeyListener(new KeyAdapter() {
			public void keyTyped(KeyEvent e) {
				updateSaveBtn();
				if (textField.getText().length() >= 100) {
					e.consume();
					textField.setText(textField.getText().substring(0, 100));
					java.awt.Toolkit.getDefaultToolkit().beep();
				}
			}
		});
		textField.addKeyListener(new KeyListener() {			
			@Override
			public void keyTyped(KeyEvent e) {
			}
			@Override
			public void keyReleased(KeyEvent e) {
			}
			@Override
			public void keyPressed(KeyEvent e) {
				if (e.getKeyCode() == KeyEvent.VK_ENTER || e.getKeyCode() == KeyEvent.VK_TAB) {
					e.consume();
					java.awt.Toolkit.getDefaultToolkit().beep();
				}				
			}
		});
	}

	/**
	 * Used to set current customer and comment.
	 * 
	 * @param givenComment
	 *            Comment to be shown
	 * @param selectedCustomer
	 *            The current customer object
	 */
	private void setComment(String givenComment, Customer selectedCustomer) {
		textField.setEnabled(true);
		if (givenComment == null) {
			currentComment = "";
			savedComment = "";
		} else {
			currentComment = givenComment;
			savedComment = givenComment;
		}
		textField.setText(givenComment);
		saveAndEditBtn.setEnabled(false);
		saveAndEditBtn.setBackground(Color.LIGHT_GRAY);
		if (selectedCustomer == null) {
			title.setText(messages.getString("customer_comment_label"));
			setAvailable(false);
		} else {
			String customerId = selectedCustomer.getFolder().getName();
			title.setText(messages.getString("customer_comment_label") + " (" + customerId + ")");
			setAvailable(true);
		}
	}

	/**
	 * Enables and disables the save button.
	 */
	private void updateSaveBtn() {
		currentComment = textField.getText();
		if (savedComment != null && currentComment != null && !savedComment.equals(currentComment)) {
			saveAndEditBtn.setEnabled(true);
			saveAndEditBtn.setBackground(Color.GREEN);
		} else {
			saveAndEditBtn.setEnabled(false);
			saveAndEditBtn.setBackground(Color.LIGHT_GRAY);
		}
	}

	/**
	 * Changes the customer object and loads the comment.
	 * 
	 * @param selectedCustomer
	 *            The given Customer Object
	 */
	public void updateComment(Customer selectedCustomer) throws IOException, ParseException {
		this.customer = selectedCustomer;
		String comment = "";
		if (!(selectedCustomer == null)) {
			comment = selectedCustomer.getComment();
		}
		setComment(comment, selectedCustomer);
	}

	/**
	 * Used to enable and disable the comment panel.
	 * 
	 * @param available
	 *            Enabled or disabled
	 */
	private void setAvailable(boolean available) {
		if (available) {
			title.setEnabled(true);
			scrollComment.setEnabled(true);
			textField.setBackground(Color.WHITE);
		} else {
			saveAndEditBtn.setEnabled(false);
			title.setEnabled(false);
			scrollComment.setEnabled(false);
			textField.setBackground(Color.LIGHT_GRAY);
			textField.setEnabled(false);
		}
	}

	/**
	 * Saves the comment.
	 */
	private void saveComment() {
		saveAndEditBtn.setBackground(Color.LIGHT_GRAY);
		saveAndEditBtn.setEnabled(false);

		currentComment = textField.getText();
		if (savedComment != null && currentComment != null && !savedComment.equals(currentComment)) {
			try {
				customer.setComment(textField.getText());
			} catch (IOException e) {
				System.out.println("Couln't save comment to customer " + customer);
				e.printStackTrace();
			}
			savedComment = currentComment;
		}
	}
}
