package gui;

import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.LinkedList;
import java.util.List;

import javax.swing.JPanel;

import org.jdesktop.swingx.JXLabel;

/**
 * ToolTipPanel shows used relevant information when hovering over UI.
 * @author Teemu
 *
 */
public class ToolTipPanel extends JPanel {

	private static final long serialVersionUID = -7892594746452662282L;
	private final JXLabel mainText = new JXLabel();
	private final LinkedList<AnalysisBox> oldBoxes = new LinkedList<>();
	private Font defaultFont = new Font(Font.SERIF, Font.BOLD, 15);
	private Font smallFont = new Font(Font.SERIF, Font.BOLD, 10);
	public ToolTipPanel() {
		this.setLayout(new BorderLayout());
		this.add(mainText, BorderLayout.CENTER);
		this.setOpaque(false);
		mainText.setOpaque(false);
		mainText.setFont(new Font(Font.SERIF, Font.BOLD, 15));
		mainText.setLineWrap(true);
		Dimension currentDimension = new Dimension(mainText.getWidth(), 45);
		this.setPreferredSize(currentDimension);
		this.setMaximumSize(currentDimension);
		this.setMinimumSize(currentDimension);
	}
	
	public void addComponents(List<List<Object>> givenComponents) {
		for(List<Object> currentList : givenComponents) {
			addNameListener((Component) currentList.get(0), (String) currentList.get(1));
		}
	}
	public void setTip(String givenText) {
		mainText.setText(givenText);
		if(mainText.getPreferredSize().height>45)
			mainText.setFont(smallFont);
		else if(mainText.getFont().equals(smallFont))
			mainText.setFont(defaultFont);
	}
	
	public void emptyTip() {
		mainText.setText("");
		mainText.setFont(defaultFont);
	}
	

	public void updateAnalysisBoxes(LinkedList<AnalysisBox> givenBoxes) {
		if(oldBoxes!=null) {
			for(AnalysisBox currentBox : givenBoxes) {
				if(!oldBoxes.contains(currentBox)) {
					addListenersToAnalysisBoxes(currentBox);
				}
			}
		}
		oldBoxes.clear();
		oldBoxes.addAll(givenBoxes);
	}
	
	private void addListenersToAnalysisBoxes(AnalysisBox givenAnalysisBox) {

		for(List<Object> currentList : givenAnalysisBox.getComponentsForToolTip()) {
			addNameListener((Component) currentList.get(0), (String) currentList.get(1));
		}
	}
	private void addNameListener(Component givenComponent, String givenString) {
		givenComponent.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseEntered(MouseEvent arg0) {
				setTip(givenString);
			}
			@Override
			public void mouseExited(MouseEvent arg0) {
				setTip("");
			}
		});
	}
}
