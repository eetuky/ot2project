package gui;

import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.*;

import javax.swing.*;
import javax.swing.table.*;

import domain.Analysis;

/**
 * Panel that shows a table of statistics for one or more Analysis objects.
 * 
 * @author Maarit Ryynänen
 */
public class StatsPanel extends JPanel {

	/** Attribute required by Serializable interface, which is extended by JPanel */
	private static final long serialVersionUID = 2751683495556030983L;

	/** Stats table. */
	private final AutoSizeJTable statsTable;
	private JTable rowHeaderTable;
	private final StatsTableModel model;

	/** Strings for labels in user interface */
	private final ResourceBundle messagesBundle;

	private SelectionListener listener;

	public StatsPanel() {
		this.messagesBundle = ResourceBundle.getBundle("Messages", Locale.getDefault());
		this.model = new StatsTableModel();
		this.statsTable = new AutoSizeJTable(model);
		this.statsTable.setName("statsTable");

		// Set selection mode to single column selection
		this.statsTable.setColumnSelectionAllowed(true);
		this.statsTable.getColumnModel().getSelectionModel().setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
		setTableLooks(this.statsTable);

		// Set listener to clicks on columns
		this.statsTable.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				// Find out which column was clicked
				int col = statsTable.columnAtPoint(e.getPoint());

				if (col < 0) // User clicked somewhere outside columns
					return;

				// Find the analysis
				Analysis objClicked = model.getAnalysisByColumnIndex(col);
				Analysis aClicked = null;
				if (objClicked.getClass() == Analysis.class)
					aClicked = objClicked;

				if (listener != null)
					listener.selectionChanged(aClicked);
			}
		});

		// Set center alignment for Analysis columns
		this.statsTable.setDefaultRenderer(Analysis.class, new DefaultTableCellRenderer() {
			/**
			 * Attribute required by Serializable interface, which is extended by
			 * DefaultTableCellRenderer
			 */
			private static final long serialVersionUID = 8897850120783296397L;

			@Override
			public int getHorizontalAlignment() {
				return SwingConstants.CENTER;
			}
		});

		// Panes for showing row headers and stats table.
		JScrollPane rowHeaderPane = createRowHeaderTable();
		JScrollPane statsPane = new JScrollPane(statsTable);

		setLayout(new BorderLayout());
		setPreferredSize(new Dimension(23, this.getHeight()));

		add(rowHeaderPane, BorderLayout.WEST);
		add(statsPane, BorderLayout.CENTER);
	}

	public void setColumnClickListener(SelectionListener listener) {
		this.listener = listener;
	}

	@Override
	public int getHeight() {
		int rowCount = this.rowHeaderTable.getRowCount();
		int rowHeight = this.rowHeaderTable.getRowHeight();
		int heightOfHeaderRow = 23;
		int heightOfScrollbar = 15;
		return rowCount * rowHeight + heightOfHeaderRow + heightOfScrollbar;
	}

	private JScrollPane createRowHeaderTable() {
		this.rowHeaderTable = new JTable(new DefaultTableModel(8, 1) {
			/**
			 * Attribute required by Serializable interface, which is extended by
			 * DefaultTableModel
			 */
			private static final long serialVersionUID = 7449633434620701890L;

			private final String[] rowNames = new String[] { "AP_amplitude_max", "AP_amplitude_avg", "ML_amplitude_max",
					"ML_amplitude_avg", "AP_frequency_avg", "ML_frequency_avg", "travel", "duration" };

			@Override
			public String getColumnName(int col) {
				return messagesBundle.getString("stats");
			}

			@Override
			public Object getValueAt(int row, int col) {
				return messagesBundle.getString(this.rowNames[row]);
			}

			@Override
			public boolean isCellEditable(int row, int col) {
				return false;
			}
		});
		this.rowHeaderTable.setName("rowHeaderTable");
		this.rowHeaderTable.setColumnSelectionAllowed(false);
		setTableLooks(this.rowHeaderTable);

		this.rowHeaderTable.setBackground(UIManager.getLookAndFeelDefaults().getColor("TableHeader.background"));
		this.rowHeaderTable.setAutoResizeMode(JTable.AUTO_RESIZE_ALL_COLUMNS);

		// JTable has to be placed inside scroll pane in order to show column headers
		JScrollPane rowHeaderPane = new JScrollPane(this.rowHeaderTable);
		rowHeaderPane.setHorizontalScrollBarPolicy(JScrollPane.HORIZONTAL_SCROLLBAR_NEVER);
		rowHeaderPane.setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_NEVER);
		rowHeaderPane.setPreferredSize(new Dimension(250, this.rowHeaderTable.getHeight()));

		return rowHeaderPane;
	}

	private void setTableLooks(JTable table) {
		table.setAutoResizeMode(JTable.AUTO_RESIZE_OFF);
		table.setRowSelectionAllowed(false);
		table.getTableHeader().setReorderingAllowed(false);

		// Bold headers
		Font defaultFont = UIManager.getFont("Table.font");
		table.getTableHeader().setFont(new Font(defaultFont.getFontName(), Font.BOLD, defaultFont.getSize()));

		// Set row margins
		int fontSize = defaultFont.getSize();
		int marginTopBottom = 2;
		table.setRowHeight(fontSize + 2 * marginTopBottom);
		table.setIntercellSpacing(new Dimension(17, marginTopBottom));
	}

	/**
	 * @return Whether there are stats columns in the stats table
	 */
	public boolean isEmpty() {
		return this.model.getColumnCount() < 1;
	}

	/**
	 * Adds one analysis to stats table.
	 * 
	 * @param analysis
	 *            The Analysis to add
	 */
	public boolean addAnalysisToTable(Analysis analysis) {
		if (this.model.contains(analysis)) {
			System.out.println("Analysis was not added to report because stats table already contains it");
			return false;
		}
		this.model.addColumn(analysis);
		return true;
	}

	/**
	 * Removes one analysis from the stats table.
	 * 
	 * @param analysis
	 *            The Analysis to remove
	 */
	public boolean removeStatsFromTable(Analysis analysis) {
		if (!this.model.contains(analysis)) {
			System.out.println("Analysis was not removed from report because stats table doesn't contain it");
			return false;
		}
		this.model.removeColumn(analysis);
		return true;
	}

	/**
	 * Removes a list of analyses from the stats table.
	 * 
	 * @param analyses
	 *            The list of Analysis to remove
	 */
	public void removeStatsFromTable(List<Analysis> analyses) {
		for (Analysis analysis : analyses) {
			removeStatsFromTable(analysis);
		}
	}

	/**
	 * Sets the column selected that contains the given analysis.
	 * 
	 * @param analysisToSelect
	 *            The Analysis to select
	 */
	public void selectColumn(Analysis analysisToSelect) {
		int i = this.model.getColumnIndex(analysisToSelect);
		if (i >= 0)
			this.statsTable.setColumnSelectionInterval(i, i);
		else
			this.statsTable.getColumnModel().getSelectionModel().clearSelection();
	}

	/**
	 * Customized JTable that adjusts the width of table columns to fit content.
	 */
	class AutoSizeJTable extends JTable {
		/** Attribute required by Serializable interface, which is extended by JTable */
		private static final long serialVersionUID = 4862552803383776501L;

		public AutoSizeJTable(DefaultTableModel tableModel) {
			super(tableModel);
		}

		/** Auto adjusts column width to fit content. */
		@Override
		public Component prepareRenderer(TableCellRenderer renderer, int row, int col) {
			Component component = super.prepareRenderer(renderer, row, col);
			int rendererWidth = component.getPreferredSize().width;
			TableColumn tableColumn = getColumnModel().getColumn(col);
			tableColumn.setPreferredWidth(
					Math.max(rendererWidth + getIntercellSpacing().width, tableColumn.getPreferredWidth()));
			return component;
		}
	}

	/**
	 * Customized TableModel that shows Analysis stats in table cells and allows
	 * adding and removing Analysis to/from table.
	 */
	class StatsTableModel extends DefaultTableModel {
		/** Attribute required by Serializable interface */
		private static final long serialVersionUID = -4529364512006397378L;

		private final List<Analysis> analysisList;

		public StatsTableModel() {
			this.analysisList = new ArrayList<>();
		}

		@Override
		public String getColumnName(int col) {
			return "" + (col + 1);
		}

		@Override
		public int getRowCount() {
			return 8;
		}

		@Override
		public int getColumnCount() {
			if (this.analysisList == null)
				return 0;
			return this.analysisList.size();
		}

		@Override
		public Class<?> getColumnClass(int col) {
			return Analysis.class;
		}

		public Analysis getAnalysisByColumnIndex(int index) {
			if (index >= 0 && index < analysisList.size())
				return this.analysisList.get(index);
			else
				return null;
		}

		public boolean isCellEditable(int row, int col) {
			return false;
		}

		public void addColumn(Analysis analysis) {
			this.analysisList.add(analysis);
			fireTableStructureChanged();
		}

		public void removeColumn(Analysis analysis) {
			this.analysisList.remove(analysis);
			fireTableStructureChanged();
		}

		public boolean contains(Analysis analysis) {
			return this.analysisList.contains(analysis);
		}

		public int getColumnIndex(Analysis analysis) {
			return this.analysisList.indexOf(analysis);
		}

		@Override
		public Object getValueAt(int row, int col) {
			Double value = -1.0;
			Analysis analysis = analysisList.get(col);
			switch (row) {
			case 0:
				value = analysis.getAmplitudeAPmax();
				break;
			case 1:
				value = analysis.getAmplitudeAPavg();
				break;
			case 2:
				value = analysis.getAmplitudeMLmax();
				break;
			case 3:
				value = analysis.getAmplitudeMLavg();
				break;
			case 4:
				value = analysis.getFrequencyAPavg();
				break;
			case 5:
				value = analysis.getFrequencyMLavg();
				break;
			case 6:
				value = analysis.getTravel();
				break;
			case 7:
				value = analysis.getDuration();
				break;
			}
			return value;
		}
	};
	public JTable getTable() {
		return statsTable;
	}
	public JTable getRowHeaderTable() {
		return rowHeaderTable;
	}
}