package gui;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Rectangle;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.io.File;
import java.util.*;

import javax.swing.*;
import javax.swing.event.*;
import javax.swing.table.TableColumn;
import javax.swing.tree.*;

import domain.Analysis;
import domain.Customer;
import name.lecaroz.java.swing.jocheckboxtree.*;
import tree.*;

/**
 * Panel that shows Customer and Analysis objects as a tree. Listens to mouse
 * clicking and collapses/expands each Customer folder. Notifys main window when
 * user chooses an Analysis in order to show/hide its report.
 *
 * @author Maarit Ryynänen
 */
class DirectoryPanel extends JPanel {

	/** Attribute required by Serializable interface, which is extended by JPanel */
	private static final long serialVersionUID = 3959716045664864318L;

	/** Tree table of all the customers and analyses in the working directory. */
	private final CustomJOCheckboxTreeTable<Object> checkboxTreeTable;

	private SelectionListener selectionListener;
	private ActionListener rightClickListener;
	private final ResourceBundle messages;

	private final File workingDir;

	/**
	 * @param workingDir
	 *            The File object of working directory
	 * @param customerList
	 *            The Customer objects from working directory
	 */
	public DirectoryPanel(File workingDir, List<Customer> customerList) {
		super(new BorderLayout());
		this.workingDir = workingDir;
		this.messages = ResourceBundle.getBundle("Messages", Locale.getDefault());

		// Format customer count
		int numberOfCust = customerList.size();
		String numberStr;
		if (numberOfCust == 1) {
			numberStr = numberOfCust + " " + messages.getString("customer_count");
		} else {
			numberStr = numberOfCust + " " + messages.getString("customers_count");
		}

		// Create tree structure for directory view
		String rootName = workingDir.getName() + " (" + numberStr + ")";
		MutableObjectNode root = new MutableObjectNode(rootName, false, true);
		root.addChildren(customerList, true);

		CustomizedTreeModel treeModel = new CustomizedTreeModel(root);
		this.checkboxTreeTable = new CustomJOCheckboxTreeTable<>(treeModel);
		this.checkboxTreeTable.setName("treetable");
		this.checkboxTreeTable.setAutoResizeMode(JTable.AUTO_RESIZE_LAST_COLUMN);

		CheckboxTree tree = (CheckboxTree) this.checkboxTreeTable.getTree();
		tree.setCellRenderer(new CustomizedTreeCellRenderer());

		// Set how the tree table behaves
		this.checkboxTreeTable.setFillsViewportHeight(true);
		this.checkboxTreeTable.getTableHeader().setReorderingAllowed(false);
		tree.setExpandsSelectedPaths(true);
		tree.setSelectsByChecking(true);
		tree.setRootVisible(true); // Show tree with root node

		// Set width of second column
		TableColumn col2 = this.checkboxTreeTable.getColumnModel().getColumn(1);
		col2.setMinWidth(70);
		col2.setMaxWidth(70);
		col2.setResizable(false);

		// Zero dependencies
		DependenciesModel<Object> dependencies = new NodeDependencies();

		// Set the checking mode, must be done after having set data in the
		// JOCheckboxTreeTable constructor or thru
		// JOCheckboxTreeTable.setTreeTableModel()
		tree.getCheckingModel()
				.setCheckingMode(new PropagatePreservingCheckDependenciesTreeCheckingMode<>(
						(DefaultTreeCheckingModel) ((CheckboxTree) this.checkboxTreeTable.getTree()).getCheckingModel(),
						dependencies));

		// Allow only one row to be selected at a time (doesn't affect checking boxes)
		tree.getSelectionModel().setSelectionMode(TreeSelectionModel.SINGLE_TREE_SELECTION);
		checkboxTreeTable.getSelectionModel().setSelectionMode(ListSelectionModel.SINGLE_SELECTION);

		// Add box checking listener

		// If customer was (un)checked, do the same to its analyses, too
		// Customer was checked
		// Add child nodes if the Customer node doesn't yet have them
		// Check all child nodes
		// Expand to show children nodes if there are any
		// Customer was unchecked
		// Uncheck all child nodes
		/**
		 * Local variable that defines a custom tree checking listener. When user checks
		 * some item in the treetable, this says what will happen.
		 */
		TreeCheckingListener customTreeCheckingListener = e -> {
			MutableObjectNode checkedNode = (MutableObjectNode) e.getPath().getLastPathComponent();
			TreePath path = new TreePath(checkedNode.getPath());
			checkboxTreeTable.getTree().setLeadSelectionPath(path);
			checkboxTreeTable.getTree().setSelectionPath(path);

			Object checkedObj = checkedNode.getObject();
			DefaultTreeCheckingModel model = (DefaultTreeCheckingModel) e.getSource();

			if (e.isCheckedPath()) {
				System.out.println("** Checked " + checkedNode.getObject());
			} else {
				System.out.println("** Unchecked " + checkedNode.getObject());
			}

			// If customer was (un)checked, do the same to its analyses, too
			if (checkedObj.getClass() == Customer.class) {
				if (e.isCheckedPath()) { // Customer was checked
					System.out.println("** Checked " + checkedNode.getObject());

					// Add child nodes if the Customer node doesn't yet have them
					if (checkedNode.getChildCount() == 0 && checkedNode.getAllowsChildren()) {
						createChildNodesForCustomer(checkedNode);
					}

					// Check all child nodes
					model.checkSubTree(path);
					// Expand to show children nodes if there are any
					checkboxTreeTable.getTree().expandPath(path);

				} else { // Customer was unchecked
					System.out.println("** Unchecked " + checkedNode.getObject());

					// Uncheck all child nodes
					model.uncheckSubTree(path);
				}
			}
			if (selectionListener != null)
				selectionListener.checkedBoxChanged(checkedObj, e.isCheckedPath());
		};
		tree.getCheckingModel().addTreeCheckingListener(customTreeCheckingListener);
		// Add tree expanding listener
		/**
		 * Local variable that defines a custom tree expansion listener. When user
		 * expands some item in the treetable, this says what will happen.
		 */
		TreeExpansionListener customTreeExpansionListener = new TreeExpansionListener() {
			@Override
			public void treeExpanded(TreeExpansionEvent e) {
				MutableObjectNode expandedNode = (MutableObjectNode) e.getPath().getLastPathComponent();
				System.out.println("** Expanded " + expandedNode.getObject());
				checkboxTreeTable.getTree().setLeadSelectionPath(new TreePath(expandedNode.getPath()));
			}

			@Override
			public void treeCollapsed(TreeExpansionEvent e) {
				MutableObjectNode collapsedNode = (MutableObjectNode) e.getPath().getLastPathComponent();
				System.out.println("** Collapsed " + collapsedNode.getObject());
			}
		};
		tree.addTreeExpansionListener(customTreeExpansionListener);
		// Add click selection listener
		// The row that was clicked on
		// User clicked outside treetable
		// Keep last selected path active
		// User clicked somewhere in treetable
		// A Customer was selected
		// Add child nodes if the Customer node didn't yet have them
		// Refresh table cell to show it can be expanded
		// an Analysis was selected
		// Scroll to selected item
		/**
		 * Local variable that defines a custom selection listener. When user clicks on
		 * some item in the treetable, this says what will happen.
		 */
		ListSelectionListener customListSelectionListener = e -> {
			if (e.getValueIsAdjusting())
				return;

			MutableObjectNode selectedNode = null;
			Object selectedObj = null;

			// The row that was clicked on
			int row = ((DefaultListSelectionModel) e.getSource()).getMaxSelectionIndex();
			if (row == -1) { // User clicked outside treetable
				// Keep last selected path active
				TreePath lastPath = checkboxTreeTable.getTree().getLeadSelectionPath();
				if (lastPath != null) {
					checkboxTreeTable.getTree().setSelectionPath(lastPath);
					selectedObj = ((MutableObjectNode) lastPath.getLastPathComponent()).getObject();
				}

			} else { // User clicked somewhere in treetable
				TreePath path = checkboxTreeTable.getTree().getPathForRow(row);
				checkboxTreeTable.getTree().setLeadSelectionPath(path);
				selectedNode = (MutableObjectNode) path.getLastPathComponent();
				selectedObj = selectedNode.getObject();

				Customer selectedCustomer = null;
				if (selectedObj.getClass() == Customer.class) { // A Customer was selected
					selectedCustomer = (Customer) selectedObj;

					// Add child nodes if the Customer node didn't yet have them
					if (selectedNode.getChildCount() == 0) {
						System.out.println("Getting analyses from " + selectedCustomer);
						createChildNodesForCustomer(selectedNode);

					}
					// Refresh table cell to show it can be expanded
					((CustomizedTreeTableModelAdapter<?>) checkboxTreeTable.getModel()).fireTableCellUpdated(row, 0);

				} else if (selectedObj.getClass() == Analysis.class) { // an Analysis was selected
					selectedCustomer = (Customer) ((MutableObjectNode) selectedNode.getParent()).getObject();
				}

				// Scroll to selected item
				Rectangle rect = checkboxTreeTable.getTree().getPathBounds(path);
				checkboxTreeTable.scrollRectToVisible(rect);
			}
			fireSelectionChanged(selectedObj);
		};
		this.checkboxTreeTable.getSelectionModel().addListSelectionListener(customListSelectionListener);
		// Add mouse (right) click listener

		// Handles clicks on the tree table
		// Right button click
		// Find table row that was clicked and set it selected
		// Find corresponding index in tree and set it selected
		/* Pop up menu is defined in CustomizedTreeTableModel */
		// Handles clicks outside the tree table
		// Right button click
		/**
		 * Local variable that defines a custom mouse listener. When user clicks on or
		 * outside the treetable, this says what will happen.
		 */
		MouseListener customMouseListener = new MouseListener() {
			// Handles clicks on the tree table
			@Override
			public void mouseReleased(MouseEvent e) {
				if (!e.isConsumed())
					return;

				if (e.isPopupTrigger()) { // Right button click
					// Find table row that was clicked and set it selected
					int row = checkboxTreeTable.rowAtPoint(e.getPoint());
					checkboxTreeTable.setRowSelectionInterval(row, row);

					// Find corresponding index in tree and set it selected
					int selectedIndex = checkboxTreeTable.getSelectionModel().getMaxSelectionIndex();
					CheckboxTree tree = (CheckboxTree) checkboxTreeTable.getTree();
					TreePath path = tree.getPathForRow(selectedIndex);
					tree.setLeadSelectionPath(path);
					tree.setSelectionPath(path);
				}

				/* Pop up menu is defined in CustomizedTreeTableModel */
			}

			@Override
			public void mousePressed(MouseEvent e) {

			}

			@Override
			public void mouseExited(MouseEvent e) {

			}

			@Override
			public void mouseEntered(MouseEvent e) {

			}

			// Handles clicks outside the tree table
			@Override
			public void mouseClicked(MouseEvent e) {
				if (SwingUtilities.isRightMouseButton(e)) { // Right button click
					JPopupMenu menu = new JPopupMenu("Basic menu");
					JMenuItem item = new JMenuItem(messages.getString("add_customer"));
					item.addActionListener(rightClickListener);
					menu.add(item);
					menu.show(e.getComponent(), e.getX(), e.getY());
				}
			}
		};
		this.checkboxTreeTable.addMouseListener(customMouseListener);

		tree.setAnchorSelectionPath(new TreePath(root.getPath()));
		tree.setLeadSelectionPath(new TreePath(root.getPath()));

		JScrollPane scrollDirectory = new JScrollPane(this.checkboxTreeTable, JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED,
				JScrollPane.HORIZONTAL_SCROLLBAR_NEVER);
		scrollDirectory.getViewport().setBackground(Color.WHITE);
		scrollDirectory.setAutoscrolls(true);

		add(scrollDirectory, BorderLayout.CENTER);
	}

	/**
	 * Notifies the listener registered to listen tree selection changes.
	 * 
	 * @param selectedObject
	 *            The currently chosen node object
	 */
	void fireSelectionChanged(Object selectedObject) {
		if (selectionListener == null)
			return;
		selectionListener.selectionChanged(selectedObject);
	}

	/**
	 * Adds customer's analyses as child nodes to the customer's tree node.
	 * 
	 * @param customerNode
	 *            The node to update
	 */
	private void createChildNodesForCustomer(MutableObjectNode customerNode) {
		// Add customer's analyses to the tree if they are not there yet
		Customer customer = (Customer) customerNode.getObject();
		LinkedList<Analysis> analysesList = customer.getAnalysisList();
		customerNode.addChildren(analysesList, false);
	}

	/**
	 * Add a new customer to directory tree.
	 * 
	 * @param newCustomer
	 *            The new customer to be added
	 */
	public void addCustomerToTree(Customer newCustomer) {
		CheckboxTree tree = (CheckboxTree) checkboxTreeTable.getTree();
		MutableObjectNode root = (MutableObjectNode) tree.getModel().getRoot();

		MutableObjectNode newNode = root.addChild(newCustomer, true);
		System.out.println("Added customer " + newNode.getObject() + " to tree");
		checkboxTreeTable.updateUI();

		// Select the added customer node
		TreePath path = new TreePath(newNode.getPath());
		checkboxTreeTable.getTree().expandPath(new TreePath(root.getPath()));
		checkboxTreeTable.getTree().setLeadSelectionPath(path);
		checkboxTreeTable.getTree().setSelectionPath(path);

        updateCustomerCount();
	}

	/**
	 * Remove customer node and its child analysis nodes from the directory tree.
	 * 
	 * @param customer
	 *            The customer to be removed
	 */
	public void removeCustomerFromTree(Customer customer) {
		CheckboxTree tree = (CheckboxTree) checkboxTreeTable.getTree();
		MutableObjectNode root = (MutableObjectNode) tree.getModel().getRoot();
		MutableObjectNode nodeToRemove = root.getChildNode(customer);
		TreePath pathToRemove = new TreePath(nodeToRemove.getPath());

		// Clear selection
		tree.removeSelectionPath(pathToRemove);
		tree.setLeadSelectionPath(null);
		fireSelectionChanged(null);
		
		// Uncheck
		boolean isChecked = tree.isPathChecked(pathToRemove);
		if (isChecked) {
			tree.removeCheckingPath(pathToRemove);
		}

		root.removeChild(customer);
		System.out.println("Removed customer " + customer + " from tree");
		checkboxTreeTable.updateUI();

        updateCustomerCount();
	}
	
	private void updateCustomerCount() {
	    CheckboxTree tree = (CheckboxTree) checkboxTreeTable.getTree();
		MutableObjectNode root = (MutableObjectNode) tree.getModel().getRoot();
		
	    // Update customer count to tree root
		int numberOfCust = root.getChildCount();
		String numberStr;
		if (numberOfCust == 1) {
			numberStr = numberOfCust + " " + messages.getString("customer_count");
		} else {
			numberStr = numberOfCust + " " + messages.getString("customers_count");
		}
		
		String rootName = workingDir.getName() + " (" + numberStr+ ")";
		root.setUserObject(rootName);
	}

	/**
	 * Add a new analysis to an existing customer in directory tree. Sets the added
	 * analysis selected in the tree.
	 * 
	 * @param customer
	 *            The customer which the new analysis is added to
	 * @param newAnalysis
	 *            The new analysis to be added
	 */
	public void addAnalysisToTree(Customer customer, Analysis newAnalysis) {
		CheckboxTree tree = (CheckboxTree) checkboxTreeTable.getTree();
		MutableObjectNode root = (MutableObjectNode) tree.getModel().getRoot();
		MutableObjectNode customerNode = root.getChildNode(customer);
		TreePath customerPath = new TreePath(customerNode.getPath());

		MutableObjectNode newNode = customerNode.addChild(newAnalysis, false);
		System.out.println("Added analysis " + newAnalysis + " to customer " + customerNode.getObject());
		checkboxTreeTable.updateUI();

		// Grey customer checkbox if it was checked
		DefaultTreeCheckingModel checkingModel = ((DefaultTreeCheckingModel) tree.getCheckingModel());
		boolean customerIsChecked = tree.isPathChecked(customerPath);
		boolean hasCheckedChildren = checkingModel.pathHasCheckedChildren(customerPath);
		if (customerIsChecked && hasCheckedChildren) {
			((DefaultTreeCheckingModel) tree.getCheckingModel()).removeFromCheckedPathsSet(customerPath);
			((DefaultTreeCheckingModel) tree.getCheckingModel()).addToGreyedPathsSet(customerPath);
		} else {
			((DefaultTreeCheckingModel) tree.getCheckingModel()).removeFromCheckedPathsSet(customerPath);
		}

		// Expand the customer node and select the added analysis
		TreePath pathAdded = new TreePath(newNode.getPath());
		tree.expandPath(new TreePath(customerNode.getPath()));
		tree.setLeadSelectionPath(pathAdded);
		tree.setSelectionPath(pathAdded);
	}

	/**
	 * Remove analysis node from the directory tree.
	 * 
	 * @param customer
	 *            The customer whose analysis is to be removed
	 * @param analysis
	 *            The analysis to be removed
	 */
	public void removeAnalysisFromTree(Customer customer, Analysis analysis) {
		// Find out which tree node to update
		CheckboxTree tree = ((CheckboxTree) checkboxTreeTable.getTree());
		MutableObjectNode root = (MutableObjectNode) tree.getModel().getRoot();
		MutableObjectNode customerNode = root.getChildNode(customer);
		TreePath customerPath = new TreePath(customerNode.getPath());
		MutableObjectNode nodeToRemove = customerNode.getChildNode(analysis);
		TreePath pathToRemove = new TreePath(nodeToRemove.getPath());

		// Select customer of the removed analysis
		checkboxTreeTable.getTree().setLeadSelectionPath(customerPath);
		checkboxTreeTable.getTree().setSelectionPath(customerPath);

		// Uncheck
		boolean isChecked = tree.isPathChecked(pathToRemove);
		if (isChecked) {
			tree.removeCheckingPath(pathToRemove);
		}

		customerNode.removeChild(analysis);
		System.out.println("Removed analysis " + analysis + " from customer " + customer + " from tree");
		checkboxTreeTable.updateUI();

		// Find out if customer still has checked or unchecked analyses
		DefaultTreeCheckingModel checkingModel = ((DefaultTreeCheckingModel) tree.getCheckingModel());
		boolean hasCheckedChildren = checkingModel.pathHasCheckedChildren(customerPath);
		boolean hasUncheckedChildren = checkingModel.pathHasUncheckedChildren(customerPath);

		// Update customer checkbox accordingly
		boolean parentIsChecked = checkingModel.isPathChecked(customerPath);
		boolean parentIsGreyed = checkingModel.isPathGreyed(customerPath);

		if (parentIsChecked) {
			if (!hasCheckedChildren) { // All children are unchecked
				checkingModel.removeFromCheckedPathsSet(customerPath);
			}
		} else if (parentIsGreyed) {
			if (!hasCheckedChildren) { // All children are unchecked
				checkingModel.removeFromGreyedPathsSet(customerPath);
			} else if (!hasUncheckedChildren) { // All children are checked
				checkingModel.removeFromGreyedPathsSet(customerPath);
				checkingModel.addCheckingPath(customerPath);
			}
		} else { // Customer is not checked nor greyed
			if (!hasUncheckedChildren) { // All children are checked
				checkingModel.addCheckingPath(customerPath);
			} else if (hasCheckedChildren) { // Some children are checked
				checkingModel.addToGreyedPathsSet(customerPath);
			}
		}
	}

	/**
	 * @return The Customer object from currently selected treetable node. If
	 *         Analysis is selected, returns the Customer it belongs to. Null if no
	 *         Customer nor Analysis is selected.
	 */
	public Customer getActiveCustomer() {
		if (checkboxTreeTable.getSelectionModel().isSelectionEmpty())
			return null;

		// Currently selected row
		int row = checkboxTreeTable.getSelectionModel().getMaxSelectionIndex();

		TreePath path = checkboxTreeTable.getTree().getPathForRow(row);
		MutableObjectNode activeNode = (MutableObjectNode) path.getLastPathComponent();

		Object obj = activeNode.getObject();
		if (obj.getClass() == Customer.class)
			return (Customer) obj;
		else if (obj.getClass() == Analysis.class)
			return (Customer) ((MutableObjectNode) activeNode.getParent()).getObject();
		else
			return null;
	}

	/**
	 * Sets the given customer selected in the tree table.
	 * 
	 * @param customer
	 *            The customer to select
	 */
	public void selectCustomer(Customer customer) {
		MutableObjectNode root = (MutableObjectNode) checkboxTreeTable.getTree().getModel().getRoot();
		MutableObjectNode nodeToSelect = root.getChildNode(customer);
		checkboxTreeTable.getTree().setLeadSelectionPath(new TreePath(nodeToSelect.getPath()));
		checkboxTreeTable.getTree().setSelectionPath(new TreePath(nodeToSelect.getPath()));
	}

	/**
	 * @return The Analysis object from currently selected treetable node. Null if
	 *         no Analysis is selected.
	 */
	public Analysis getActiveAnalysis() {
		if (checkboxTreeTable.getSelectionModel().isSelectionEmpty())
			return null;

		// Currently selected row
		int row = checkboxTreeTable.getSelectionModel().getMaxSelectionIndex();

		TreePath path = checkboxTreeTable.getTree().getPathForRow(row);
		MutableObjectNode activeNode = (MutableObjectNode) path.getLastPathComponent();

		Object obj = activeNode.getObject();
		if (obj.getClass() == Analysis.class)
			return (Analysis) obj;
		else
			return null;
	}

	/**
	 * Updates an analysis node in the tree.
	 * 
	 * @param customer
	 *            The customer whose analysis needs updating
	 * @param analysis
	 *            The analysis to update
	 */
	public void updateAnalysis(Customer customer, Analysis analysis) {
		// Find out which tree node to update
		CheckboxTree tree = ((CheckboxTree) checkboxTreeTable.getTree());
		MutableObjectNode root = (MutableObjectNode) tree.getModel().getRoot();
		MutableObjectNode customerNode = root.getChildNode(customer);
		MutableObjectNode analysisNode = customerNode.getChildNode(analysis);

		// Reorder customer's analyses
		customerNode.moveChild(analysis);
		analysisNode.updateIcon();
		checkboxTreeTable.updateUI();
	}

	/**
	 * Updates a customer node in the tree.
	 * 
	 * @param customer
	 */
	public void updateCustomer(Customer customer) {
		// Find out which tree node to update
		JTree tree = checkboxTreeTable.getTree();
		MutableObjectNode root = (MutableObjectNode) tree.getModel().getRoot();
		MutableObjectNode customerNode = root.getChildNode(customer);
		TreePath customerPath = new TreePath(customerNode.getPath());

		// Reorder customers
		root.moveChild(customer);
		System.out.println("Updated customer " + customer);

		// Notify table about changed row (updates row label)
		int row = tree.getRowForPath(customerPath);
		((CustomizedTreeTableModelAdapter<?>) checkboxTreeTable.getModel()).fireTableRowsUpdated(row, row);

		// Notify tree about changed node (resizes and repaints label)
		TreeModelEvent event = new TreeModelEvent(this, customerPath);
		TreeModelListener[] listeners = ((CustomizedTreeModel) tree.getModel()).getTreeModelListeners();
		for (TreeModelListener listener : listeners) {
			listener.treeNodesChanged(event);
		}
		
		checkboxTreeTable.updateUI();
	}

	/**
	 * Unchecks a single analysis on directory tree.
	 * 
	 * @param analysisToUncheck
	 *            The analysis to uncheck
	 */
	public void uncheckAnalysis(Analysis analysisToUncheck) {
		CheckboxTree tree = (CheckboxTree) checkboxTreeTable.getTree();

		// Go through all checked paths and find the right one to uncheck
		TreePath[] checkedPaths = tree.getCheckingPaths();
		TreePath pathToUncheck = null;
		TreePath path;
		Object obj;
		for (TreePath checkedPath : checkedPaths) {
			path = checkedPath;
			obj = ((MutableObjectNode) path.getLastPathComponent()).getObject();
			if (obj.getClass() == Analysis.class)
				if (analysisToUncheck == obj) {
					pathToUncheck = path;
					break;
				}
		}

		if (pathToUncheck != null) {
			TreePath parentPath = pathToUncheck.getParentPath();

			DefaultTreeCheckingModel checkingModel = ((DefaultTreeCheckingModel) tree.getCheckingModel());
			checkingModel.removeFromCheckedPathsSet(pathToUncheck);

			// Notify table about unchecked row (updates table cell)
			int row = tree.getRowForPath(pathToUncheck);
			((CustomizedTreeTableModelAdapter<?>) checkboxTreeTable.getModel()).fireTableRowsUpdated(row, row);

			// If customer is checked, uncheck and grey its checkbox
			boolean parentIsChecked = checkingModel.isPathChecked(parentPath);
			if (parentIsChecked) {
				checkingModel.removeFromCheckedPathsSet(parentPath);
				checkingModel.addToGreyedPathsSet(parentPath);
			}

			// If customer has no checked analyses left, ungrey its checkbox
			boolean hasCheckedChildren = checkingModel.pathHasCheckedChildren(parentPath);
			if (!hasCheckedChildren) {
				checkingModel.removeFromGreyedPathsSet(parentPath);
			}
		}
	}

	/**
	 * Selects one of the checked analysis on directory tree.
	 * 
	 * @param analysisToSelect
	 */
	public void selectCheckedAnalysis(Analysis analysisToSelect) {
		CheckboxTree tree = ((CheckboxTree) checkboxTreeTable.getTree());

		// Go through all checked paths and find the right one to select
		TreePath[] checkedPaths = tree.getCheckingPaths();
		TreePath pathToSelect = null;
		Object obj;
		MutableObjectNode node;
		for (TreePath checkedPath : checkedPaths) {
			node = (MutableObjectNode) checkedPath.getLastPathComponent();
			obj = node.getObject();
			if (obj.getClass() == Analysis.class)
				if (analysisToSelect == obj) {
					pathToSelect = checkedPath;
					break;
				}
		}
		if (pathToSelect != null) {
			checkboxTreeTable.getTree().setLeadSelectionPath(pathToSelect);
			checkboxTreeTable.getTree().setSelectionPath(pathToSelect);
		}
	}

	public void setSelectionListener(SelectionListener listener) {
		this.selectionListener = listener;
	}

	public void setRightClickMenuListener(ActionListener listener) {
		this.rightClickListener = listener;
		((CustomizedTreeModel) checkboxTreeTable.getTree().getModel()).setPopupMenuListener(listener);
	}

	class NodeDependencies implements DependenciesModel<Object> {
		final AbstractMap<Object, AbstractList<Object>> dependenciesMap;
		final AbstractMap<Object, AbstractList<Object>> parentsMap;

		NodeDependencies() {
			this.dependenciesMap = new HashMap<>();
			this.parentsMap = new HashMap<>();
		}

		private void add(final AbstractMap<Object, AbstractList<Object>> map, Object key, Object dependency) {
			Vector<Object> elements = (Vector<Object>) map.get(key);
			if (elements == null) {
				elements = new Vector<>();
				map.put(key, elements);
			}
			if (!elements.contains(dependency))
				elements.add(dependency);

		}

		public DependenciesModel<Object> addDependency(Object entry, Object dependency) {
			this.add(this.dependenciesMap, entry, dependency);
			this.add(this.parentsMap, dependency, entry);
			return this;
		}

		public boolean isDependency(Object node, Object dependency) {
			return this.dependenciesMap.containsKey(node) && this.dependenciesMap.get(node).contains(dependency);
		}

		public Object[] parents(Object node) {
			final AbstractList<Object> parents = this.parentsMap.get(node);
			return (parents != null ? parents.toArray(new Object[0]) : null);
		}

		public Object[] dependencies(Object node) {
			final AbstractList<Object> dependencies = this.dependenciesMap.get(node);
			return (dependencies != null ? dependencies.toArray(new Object[0]) : null);
		}
	}
}
