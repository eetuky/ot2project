package gui;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.Toolkit;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.text.SimpleDateFormat;
import java.util.*;

import javax.imageio.ImageIO;
import javax.swing.BorderFactory;
import javax.swing.BoxLayout;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.SwingConstants;
import javax.swing.border.Border;

import domain.Analysis;
import domain.Customer;

/**
 * Panel that displays the information of one analysis object.
 * 
 * @author Teemu Savorinen
 */
public class AnalysisBox extends JPanel{

	private static final long serialVersionUID = 2687040713728885887L;

	private final ResourceBundle messages;

	private int imgBoxHeightAndWidth;
	private int givenName;
	
	private final Customer customer;

	/** Components for the buttons */
	private final JButton editBtn = new JButton();
	private final JPanel closeBtn = new JPanel(new BorderLayout());
	private JLabel closeBtnLabel = new JLabel();
	
	private final JPanel ellipsisBox = new JPanel();
	private final JPanel titleBar = new JPanel();
	private final JPanel imageAndTitleBarBox = new JPanel();
	private final JPanel infoBoxAndComment = new JPanel();
	
	private int thisBoxsNumber;
	
	/** Labels for shown Analysis info */
	private final JLabel boxNumberLabel = new JLabel();
	private final JLabel stanceLabel = new JLabel();
	private final JLabel eyesLabel = new JLabel();
	private final JLabel dateLabel = new JLabel();
	private final JLabel measurerLabel = new JLabel();
	
	private final JLabel lineBetweenDateAndMeasurer = new JLabel();
	
	/** JPanels for shown Analysis info */
	private final JPanel boxNumberPanel = new JPanel();
	private final JPanel stancePanel = new JPanel();
	private final JPanel eyesPanel = new JPanel();
	private final JPanel dateAndMeasurerPanel = new JPanel();
	
	private final JPanel infoBox = new JPanel();
	private final JPanel imageBox = new JPanel();

	private final Analysis givenAnalysis;
	private int titleLabelHeight;
	
	private RemoveListener removeListener;
	private SelectListener selectListener;
	private EditListener editListener;
	
	private final JPanel infoBoxMarginPanel = new JPanel();
	private Font italicFont;
	private BufferedImage ellipse;
	private final ImageIcon ellipseAsIcon = new ImageIcon();
	private final JLabel showEllipse = new JLabel();
	private final JTextArea analysisComment = new JTextArea();
	private final JPanel analysisCommentMarginPanel = new JPanel();
	private final JPanel centeringBlockForTitleBar = new JPanel(new BorderLayout());
	private JLabel titleBarText = new JLabel();
	private final AnalysisBox thisBox = this;
	
	private final ImageIcon editIcon;
	private final ImageIcon editIconScaled = new ImageIcon();

	private ImageIcon eyesIcon;
	private final ImageIcon eyesIconScaled = new ImageIcon();
	
	private ImageIcon stanceIcon;
	private final ImageIcon stanceIconScaled = new ImageIcon();
	
	private final JScrollPane scrollComment;


	/**
	 * Constructor.
	 * 
	 * @param currentAnalysis
	 * 			Given Analysis object
	 * @param givenBoxNumber
	 * 			Shown number which is used to identify the connection between stats and ellipsis
	 * @param givenCustomer
	 * 			Customer object, which analysis is to be shown
	 */
	public AnalysisBox(Analysis currentAnalysis, int givenBoxNumber, Customer givenCustomer) {
		this.thisBoxsNumber = givenBoxNumber;
		this.givenAnalysis = currentAnalysis;
		this.customer = givenCustomer;
		this.messages = ResourceBundle.getBundle("Messages", Locale.getDefault());
		this.setLayout(new BorderLayout());

		titleBar.setLayout(new BorderLayout());

		titleBar.setBorder(BorderFactory.createLineBorder(Color.DARK_GRAY));

		closeBtnLabel.setText("x");
		
		//Custom button
		closeBtn.addMouseListener(new MouseListener() {
			@Override
			public void mouseClicked(MouseEvent e) {
				removeListener.callForRemoveItem(givenAnalysis);
			}
			
			@Override
			public void mousePressed(MouseEvent e) {
				closeBtn.setBackground(Color.WHITE);
			}
			
			@Override
			public void mouseReleased(MouseEvent e) {
				closeBtn.setBackground(Color.LIGHT_GRAY);
			}
			
			@Override
			public void mouseExited(MouseEvent e) {
				closeBtn.setBackground(Color.LIGHT_GRAY);
			}
			
			@Override
			public void mouseEntered(MouseEvent arg0) {
				closeBtn.setBackground(Color.GRAY);
			}
		});
		
		closeBtn.add(closeBtnLabel, BorderLayout.CENTER);
		closeBtnLabel.setHorizontalAlignment(SwingConstants.CENTER);
		editBtn.addActionListener(e -> editListener.callForEditItem(givenAnalysis));
		closeBtn.setBackground(Color.LIGHT_GRAY);
		closeBtn.setBorder(BorderFactory.createLineBorder(Color.DARK_GRAY));
		titleBar.add(closeBtn, BorderLayout.EAST);
		centeringBlockForTitleBar.setOpaque(false);
		centeringBlockForTitleBar.setBorder(BorderFactory.createEmptyBorder());
		titleBarText.setText(givenCustomer.toString());
		
		titleBar.add(centeringBlockForTitleBar, BorderLayout.WEST);
		titleBar.add(titleBarText, BorderLayout.CENTER);
		titleBarText.setHorizontalAlignment(SwingConstants.CENTER);
		MouseAdapter selectAdapter = new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				selectListener.callForSelectItem(givenAnalysis, thisBox);
			}
		};
		titleBarText.addMouseListener(selectAdapter);
		centeringBlockForTitleBar.addMouseListener(selectAdapter);
		
		imageBox.setLayout(new BorderLayout());

		imageAndTitleBarBox.setLayout(new BorderLayout());
		imageAndTitleBarBox.setLayout(new BoxLayout(imageAndTitleBarBox, BoxLayout.Y_AXIS));
		imageAndTitleBarBox.add(titleBar);
		imageAndTitleBarBox.add(imageBox);

		ellipsisBox.setLayout(new BoxLayout(ellipsisBox, BoxLayout.Y_AXIS));
		
		showEllipse.addMouseListener(selectAdapter);
		
		File imagePath = givenAnalysis.getImageFile();
		System.out.println("imagePath: " + imagePath);
		try {
			ellipse = ImageIO.read(imagePath);
			ellipseAsIcon.setImage(ellipse);
			showEllipse.setIcon(ellipseAsIcon);
			imageBox.add(showEllipse, BorderLayout.CENTER);
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		imageBox.setBorder(BorderFactory.createLineBorder(Color.DARK_GRAY));
		this.add(ellipsisBox, BorderLayout.CENTER);
		ellipsisBox.add(imageAndTitleBarBox);
		
		infoBoxMarginPanel.setLayout(new BoxLayout(infoBoxMarginPanel, BoxLayout.Y_AXIS));
		boxNumberPanel.setLayout(new BorderLayout());
		stancePanel.setLayout(new BorderLayout());
		eyesPanel.setLayout(new BorderLayout());
		dateAndMeasurerPanel.setLayout(new FlowLayout(FlowLayout.LEFT));
		
		boxNumberPanel.add(boxNumberLabel, BorderLayout.WEST);
		stancePanel.add(stanceLabel, BorderLayout.WEST);
		eyesPanel.add(eyesLabel, BorderLayout.WEST);
		dateAndMeasurerPanel.add(dateLabel);
		dateAndMeasurerPanel.add(lineBetweenDateAndMeasurer);
		dateAndMeasurerPanel.add(measurerLabel);
		
		infoBoxMarginPanel.add(boxNumberPanel);
		infoBoxMarginPanel.add(stancePanel);
		infoBoxMarginPanel.add(eyesPanel);
		
		infoBox.setLayout(new FlowLayout(FlowLayout.CENTER, 0, 0));
		infoBox.add(infoBoxMarginPanel);
		
		this.setBackground(Color.LIGHT_GRAY);
		editBtn.setBackground(Color.LIGHT_GRAY);
		editIcon = loadIcon("/images/btnEdit.png");
		editBtn.setIcon(editIconScaled);
		analysisComment.setText(givenAnalysis.getcomments());
		analysisComment.setEditable(false);
		scrollComment = new JScrollPane(analysisComment, JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED,
				JScrollPane.HORIZONTAL_SCROLLBAR_NEVER);
		scrollComment.setBorder(BorderFactory.createEmptyBorder());
		analysisCommentMarginPanel.setLayout(new BorderLayout());
		
		JPanel analysisCommentPanel = new JPanel(new FlowLayout(FlowLayout.CENTER, 0, 0));
		analysisCommentPanel.add(analysisCommentMarginPanel);
		analysisComment.setFont(italicFont);
		analysisComment.setWrapStyleWord(true);
		analysisComment.setLineWrap(true);
		analysisComment.setBackground(new JPanel().getBackground());
		JPanel commentBox = new JPanel();
		commentBox.setLayout(new BorderLayout());
		

		commentBox.add(analysisCommentPanel, BorderLayout.CENTER);
		JPanel editBtnPanel = new JPanel(new BorderLayout());
		editBtnPanel.add(editBtn, BorderLayout.EAST);
		editBtnPanel.add(dateAndMeasurerPanel, BorderLayout.WEST);
		commentBox.add(editBtnPanel, BorderLayout.PAGE_END);

		infoBoxAndComment.setLayout(new BoxLayout(infoBoxAndComment, BoxLayout.Y_AXIS));

		infoBoxAndComment.setBorder(BorderFactory.createLineBorder(Color.DARK_GRAY));

		infoBoxAndComment.add(infoBox);
		infoBoxAndComment.add(commentBox);

		ellipsisBox.add(infoBoxAndComment, BorderLayout.CENTER);

		GridBagConstraints currentXY = new GridBagConstraints();
		currentXY.gridx = 0;
		currentXY.gridy = 0;
		
		loadInfo();
		updateSize(0.8);
	}

	/**
	 * Updates the size of AnalysisBox according to the scale-value given.
	 * 
	 * @param givenScale
	 * 			multiplier used to scale the components
	 */
	public void updateSize(double givenScale) {
		// Height and width settings for scaling
		int mainWidth = (int) Math.round(Toolkit.getDefaultToolkit().getScreenSize().width * 0.2 * givenScale);
		int mainHeight = (int) Math.round(mainWidth * 1.74);
		int titleBarHeight = (int) Math.round(mainHeight * 0.05);
		int textSize = (int) Math.round(mainHeight * 0.03);
		int commentAndTitleHeight = (int) Math.round(mainWidth * 0.65);
		imgBoxHeightAndWidth = Math.round(mainWidth);
		showEllipse.setSize(imgBoxHeightAndWidth, imgBoxHeightAndWidth);
		titleLabelHeight = (int) Math.round(mainHeight * 0.03);

		reSize(this, new Dimension(mainWidth, mainHeight));
		reSize(titleBar, new Dimension(mainWidth, titleBarHeight));
		
		reSize(closeBtn, new Dimension(titleBarHeight, titleBarHeight));
		reSize(infoBox, new Dimension(mainWidth, Math.round(titleLabelHeight * 5)));
		
		//Analysis info labels
		Dimension labelDimension = new Dimension(mainWidth, titleLabelHeight);
		reSize(boxNumberPanel, new Dimension(mainWidth, titleLabelHeight*2));
		reSize(stancePanel, labelDimension);
		reSize(eyesPanel, labelDimension);
		reSize(dateAndMeasurerPanel, new Dimension(mainWidth, titleLabelHeight*2));
		
		//Font sizes
		Font boldFont = new Font(Font.SANS_SERIF, Font.BOLD, textSize);
		Font defaultFont = new Font(Font.SANS_SERIF, Font.PLAIN, textSize);
		italicFont = new Font(Font.SANS_SERIF, Font.ITALIC, textSize);
		
		closeBtnLabel.setFont(boldFont);
		editBtn.setFont(boldFont);
		analysisComment.setFont(defaultFont);
		boxNumberLabel.setFont(boldFont);
		stanceLabel.setFont(defaultFont);
		eyesLabel.setFont(defaultFont);
		dateLabel.setFont(defaultFont);
		measurerLabel.setFont(defaultFont);
		analysisComment.setFont(italicFont);
		lineBetweenDateAndMeasurer.setFont(defaultFont);
		titleBarText.setFont(defaultFont);
		
		centeringBlockForTitleBar.setFont(new Font(Font.SANS_SERIF, Font.BOLD, (int) (textSize *0.8)));
		reSize(centeringBlockForTitleBar, new Dimension(titleBarHeight, titleBarHeight));
		
		boxNumberLabel.setFont(defaultFont);
		
		Dimension commentDimension = new Dimension(mainWidth -(int) Math.round(mainWidth * 0.06), Math.round(titleLabelHeight*5));
		reSize(scrollComment, new Dimension(mainWidth -(int) Math.round(mainWidth * 0.06), Math.round(titleLabelHeight*4)));
		reSize(infoBoxMarginPanel, commentDimension);
		reSize(analysisCommentMarginPanel, new Dimension(mainWidth -(int) Math.round(mainWidth * 0.06), Math.round(titleLabelHeight*4)));
		reSize(infoBox, new Dimension(mainWidth, Math.round(titleLabelHeight*5)));
		reSize(imageBox, new Dimension(mainWidth, mainWidth));
		
		//If scaling makes image too small, renders as smooth
		if(givenScale<0.9)
			ellipseAsIcon.setImage(ellipse.getScaledInstance(imgBoxHeightAndWidth, imgBoxHeightAndWidth, java.awt.Image.SCALE_SMOOTH));
		else
			ellipseAsIcon.setImage(ellipse.getScaledInstance(imgBoxHeightAndWidth, imgBoxHeightAndWidth, java.awt.Image.SCALE_DEFAULT));

		if (editIcon != null)
			editIconScaled.setImage(editIcon.getImage().getScaledInstance(titleLabelHeight*2, titleLabelHeight*2, java.awt.Image.SCALE_SMOOTH));
		updateIcons();
		reSize(editBtn, new Dimension(titleLabelHeight*2, titleLabelHeight*2));
		reSize(infoBoxAndComment, new Dimension(mainWidth, commentAndTitleHeight));
		reSize(imageAndTitleBarBox, new Dimension(mainWidth, mainWidth + titleBarHeight));
		reSize(ellipsisBox, new Dimension(mainWidth, imgBoxHeightAndWidth + titleBarHeight));
	}
	
	/**
	 * Used to set the dimension of the given component (sets the preferredSize, MinimumSize 
	 * and maximumSize).
	 * 
	 * @param givenComponent
	 * 			Component to be resized
	 * @param newDimension
	 * 			Dimension to be set on the component
	 */
	private void reSize(Component givenComponent, Dimension newDimension) {
		givenComponent.setPreferredSize(newDimension);
		givenComponent.setMinimumSize(newDimension);
		givenComponent.setMaximumSize(newDimension);
	}
	
	/**
	 * Updates the eyes and stance icons.
	 */
	private void updateIcons() {
		if (eyesIcon != null)
			eyesIconScaled.setImage(eyesIcon.getImage().getScaledInstance(titleLabelHeight, titleLabelHeight, java.awt.Image.SCALE_SMOOTH));
		if(stanceIcon != null)
			stanceIconScaled.setImage(stanceIcon.getImage().getScaledInstance(titleLabelHeight, titleLabelHeight, java.awt.Image.SCALE_SMOOTH));
	}

	/**
	 * Loads icon according to given path.
	 * 
	 * @param givenPath
	 * 			Path of the icon
	 * @return ImageIcon Loaded icon as ImageIcon
	 */
	private ImageIcon loadIcon(String givenPath) {
				ImageIcon loadedIcon = null;
				InputStream editStream = AnalysisBox.class.getResourceAsStream(givenPath);
				try {
					if (editStream != null) {
						loadedIcon = new ImageIcon(ImageIO.read(editStream));
					}
				} catch (IOException e) {
					e.printStackTrace();
				} finally {
					try {
						if (editStream != null)
							editStream.close();
					} catch (IOException e1) {
						e1.printStackTrace();
					}
				}
		return loadedIcon;
	}
	
	/** Getters */
	public Customer getCustomer() {
		return this.customer;
	}

	public Analysis getAnalysis() {
		return givenAnalysis;
	}

	public int getRemoveIndex() {
		return givenName;
	}
	
	/**
	 * Setter for the box number.
	 * 
	 * @param givenNumber
	 * 			Number used to recognise the connection between ellipsis and statistics
	 */
	public void setBoxNumber(int givenNumber) {
		this.thisBoxsNumber = givenNumber;
		boxNumberLabel.setText(this.thisBoxsNumber + ".");
	}
	
	/**
	 * Sets the listener for removing the AnalysisBox from the EllipsesPanel.
	 */
	public void setRemoveListener(RemoveListener givenListener) {
		this.removeListener = givenListener;
	};
	
	/**
	 * Sets the listener for selecting the Analysis from report.
	 */
	public void setSelectListener(SelectListener givenListener) {
		this.selectListener = givenListener;
	};
	
	/**
	 * Sets the listener for clicking of the editBtn.
	 */
	public void setEditListener(EditListener givenListener) {
		this.editListener = givenListener;
	};
	
	/** Interfaces for the listeners */
	interface RemoveListener {
		void callForRemoveItem(Analysis givenAnalysis);
	}
	
	interface SelectListener {
		void callForSelectItem(Analysis givenAnalysis, AnalysisBox givenBox);
	}
	
	interface EditListener {
		void callForEditItem(Analysis givenAnalysis);
	}
	
	/**
	 * Can be used to toggle customer name visible on top of the AnalysisBox (shown only 
	 * if more than customer is visible on report).
	 */
	public void showCustomer(boolean givenBoolean) {
		if(givenBoolean)
			titleBarText.setText(this.customer.toString());
		else
			titleBarText.setText("");
	}
	
	/**
	 * Changes the UI of AnalysisBox according to the selection.
	 */
	public void select() {
		titleBar.setBackground(Color.BLUE);
		titleBarText.setForeground(Color.WHITE);
		Border selectedBorder = BorderFactory.createLineBorder(Color.BLUE);
		imageBox.setBorder(selectedBorder);
		titleBar.setBorder(selectedBorder);
		infoBoxAndComment.setBorder(selectedBorder);
	}
	
	/**
	 * Changes the UI of AnalysisBox according to the selection.
	 */
	public void unselect() {
		titleBar.setBackground(Color.LIGHT_GRAY);
		titleBarText.setForeground(Color.RED);
		Border unSelectedBorder = BorderFactory.createLineBorder(Color.DARK_GRAY);
		imageBox.setBorder(unSelectedBorder);
		imageBox.setBorder(unSelectedBorder);
		titleBar.setBorder(unSelectedBorder);
		infoBoxAndComment.setBorder(unSelectedBorder);
	}
	
	/**
	 * A method for reloading the ellipse-image.
	 */
	private void reloadImage() {
		File imagePath = this.givenAnalysis.getImageFile();
		System.out.println("Reading graph image: "+imagePath.getAbsolutePath());
		try {
			BufferedImage graphImage = ImageIO.read(new File(imagePath.getAbsolutePath()));
			if(ellipseAsIcon!=null&&graphImage!=null) {
				ellipse = graphImage;
				ellipseAsIcon.setImage(ellipse.getScaledInstance(imgBoxHeightAndWidth, imgBoxHeightAndWidth, java.awt.Image.SCALE_SMOOTH));
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	/**
	 * Loads in the info from Analysis object.
	 */
	private void loadInfo() {
		boxNumberLabel.setText(thisBoxsNumber + ".");
		System.out.println(givenAnalysis.getStance().getIndex());
		stanceIcon = null;
		
		//Loads the icon according to the stance
		switch(givenAnalysis.getStance().getIndex()) {
			case 1:
				stanceIcon = loadIcon("/images/1_comfortable_stance.jpg");
				break;
			case 2:
				stanceIcon = loadIcon("/images/2_parallel_stance.jpg");
				break;
			case 3:
				stanceIcon = loadIcon("/images/3_semi-tandem_stance.jpg");
				break;
			case 4:
				stanceIcon = loadIcon("/images/4_tandem_stance.jpg");
				break;
			case 5:
				stanceIcon = loadIcon("/images/5_one-legged_stance_left.png");
				break;
			case 6:
				stanceIcon = loadIcon("/images/6_one-legged_stance_right.png");
				break;
		}
		if(stanceIcon!=null)
			stanceLabel.setIcon(stanceIconScaled);
		stanceLabel.setText(messages.getString(givenAnalysis.getStance().getStance()));
		
		if(givenAnalysis.getEyesIndex()==1) {
			eyesIcon = loadIcon("/images/1_eyes_open.png");
		} else if(givenAnalysis.getEyesIndex()==2) {
			eyesIcon = loadIcon("/images/2_eyes_closed.png");
		}
		
		eyesLabel.setIcon(eyesIconScaled);
		eyesLabel.setText(messages.getString(givenAnalysis.getEyes()));

		SimpleDateFormat formatter = new SimpleDateFormat("dd.MM.yyyy");
		try {
			dateLabel.setText(formatter.format(givenAnalysis.getDate()));
		} catch (NullPointerException e) {
			dateLabel.setText("");
		}		
		lineBetweenDateAndMeasurer.setText(" - ");
		measurerLabel.setText(givenAnalysis.getMeasurer());
		
		analysisComment.setText(givenAnalysis.getcomments());
		
		//removes analysisComment (so comments scrollPanes wouldn't interfere with scrolling the main scrollPane as much) if text's empty
		analysisCommentMarginPanel.removeAll();
		if(!analysisComment.getText().equals("")) {
			analysisCommentMarginPanel.add(scrollComment, BorderLayout.CENTER);
		}

	}

	
	/**
	 * Updates/reloads the information in the AnalysisBox.
	 */
	public void updateView() {
		reloadImage();
		loadInfo();
		updateIcons();
		updateUI();
	}
	
	/**
	 * turnToArray is used to create array of the component and its ToolTip-text for the ToolTipPanel.
	 * 
	 * @param givenComponent
	 * 			Component that gets the listener for the ToolTip-panel
	 * @param givenString
	 * 			String that'll be shown inside the ToolTip-panel
	 * @return List<Object> Created array
	 */
	private List<Object> turnToArray(Component givenComponent, String givenString){
		List<Object> newArray = new ArrayList<>();
		newArray.add(givenComponent);
		newArray.add(givenString);
		return newArray;
	}
	
	/**
	 * getComponentsForToolTip is used to create array of arrays witch include the component and its ToolTip-text for the ToolTipPanel.
	 * 
	 * @return List<Object> Created array
	 */
	public List<List<Object>> getComponentsForToolTip() {
		List<List<Object>> componentList = new ArrayList<>();
		componentList.add(turnToArray(stancePanel, messages.getString("tip_stance")));
		componentList.add(turnToArray(eyesPanel, messages.getString("tip_eyes")));
		componentList.add(turnToArray(dateLabel, messages.getString("tip_date")));
		componentList.add(turnToArray(measurerLabel, messages.getString("tip_measurer")));
		componentList.add(turnToArray(analysisCommentMarginPanel, messages.getString("tip_analysis_comment")));
		componentList.add(turnToArray(analysisComment, messages.getString("tip_analysis_comment")));
		componentList.add(turnToArray(closeBtn, messages.getString("tip_close_btn")));
		componentList.add(turnToArray(showEllipse, messages.getString("tip_ellipse_image")));
		componentList.add(turnToArray(editBtn, messages.getString("tip_edit_analysis")));
		return componentList;
	}

	/**
	 * Updates/reloads the customer in the UI of the AnalysisBox (in case the customer-id is being changed).
	 */
	public void updateIfShownCustomer() {
		if(!titleBarText.equals("")) {
			showCustomer(true);
		}
	}
}
