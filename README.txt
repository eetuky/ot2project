Syhero 0.5.1
2019

GENERAL INFORMATION
============
A prototype application for analysing postural sway during standing. The application takes a raw data file from an inertial measurement unit (IMU), interprets it into sway coordinates, and shows visualization of Center of Mass sway area, along with statistics derived from the coordinates data.

N.B.
**THIS VERSION 0.5.1 DOES NOT CALCULATE COORDINATES FROM THE IMU RAW DATA FOR REAL!**
The mockup coordinates are generated randomly. Proper calculator module is yet to be added to the application.

SYSTEM REQUIREMENTS & INSTALLATION
============
The prototype was developed for Windows 10 using Java version 8.

The application is delivered as Syhero_0.5.1.jar file, accompanied by user manual in PDF format, and two text files: README.txt (LUEMINUT.txt in Finnish) and LICENSE.txt. No installation is required. You can just copy the whole program folder to your computer, and run the JAR file.

AUTHORS
============
Application spesific code is written by a group of Computer Science students in the University of Eastern Finland:
- Juha Korvenaho
- Eetu Kyyr�
- Kimi Oksanen
- Maarit Ryyn�nen
- Ville Salo
- Teemu Savorinen

The application uses name.lecaroz.java.swing.sun package. Most part of codes under src/main/java/name/lecaroz/java/swing/jocheckboxtree was initially written by:
- Enrico Boldrini
- Lorenzo Bigagli

Warning:
JTreeTable.java (renamed and modified into JOCheckboxTreeTable), TreeTableModelAdapter.java stored in the name.lecaroz.java.swing.sun package, are under Sun Microsystems Copyright (1997, 1998) and were modified for supporting new features like contextual tooltips. They were initially written by: 
- Philip Milne
- Scott Violet


LICENSE
=======
See the LICENSE.txt file for details.

This project uses JOCheckboxTreeTable: https://github.com/llecaroz/JOCheckboxTreeTable, which is a fork of the eu.floraresearch.lablib.gui.checkboxtree (lablib-checkboxtree artifactId) CheckboxTree version 4.0-beta-1: http://essi-lab.eu/nexus/content/repositories/open.repos/eu/floraresearch/lablib-checkboxtree/4.0-beta-1/

Because the eu.floraresearch.lablib.gui.checkboxtree original project was initially under a GNU GENERAL PUBLIC LICENSE Version 2, this project inherits of their license.

This project uses iText PDF toolkit for creating PDF files. iText PDF module is licensed under GNU AFFERO GENERAL PUBLIC LICENSE Version 3.

Icons by https://icons8.com/ are licensed under Creative Commons Attribution-NoDerivs 3.0 Unported.

Application logo was designed by Ville Salo.